//
//  AudioMessageCell.swift
//  chatscreen
//
//  Created by ethisham bader on 04/11/2020.
//

import UIKit
import AVFoundation

class AudioCell: UITableViewCell, AVAudioPlayerDelegate {

    @IBOutlet weak var userNameeel: UILabel!
    @IBOutlet weak var audioProgressBar: UIProgressView!
    @IBOutlet weak var audioDuration: UILabel!
    @IBOutlet weak var playStopBtn: UIButton!
    @IBOutlet weak var pauseBtn: UIButton!
    @IBOutlet weak var audioView: UIView!
    @IBOutlet weak var timeStampAudio: UILabel!
    
    var parentViewController:UIViewController!
    var indexRow: String!
    var audioPlayer:AVAudioPlayer!
    var player: AVPlayer!
    var playerItem: AVPlayerItem!
    var timeObserverToken: Any?
    var currentlyInProgress: Bool!
    var dur:Float!
    var duration: Float!
    var userno:String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        pauseBtn.isHidden = true
        playStopBtn.isHidden = false
        currentlyInProgress = false
        duration = 0
        dur = 0
        timeStampAudio.text = DateFormatter.localizedString(from: Date(), dateStyle: .none, timeStyle: .short)
        timeStampAudio.textColor = UIColor.lightGray
        audioView.layer.borderColor = UIColor.lightGray.cgColor
        audioView.layer.borderWidth = 1.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func pauseBtn(_ sender: Any) {
        if(player.currentTime() < player.currentItem!.duration){
            player.pause()
            currentlyInProgress = true
        }else{
            currentlyInProgress = false
        }
        pauseBtn.isHidden = true
        playStopBtn.isHidden = false
    }
    @IBAction func playBtn(_ sender: Any) {
        if(currentlyInProgress){
            player.seek(to: player.currentTime())
            player.play()
            
            playStopBtn.isHidden = true
            pauseBtn.isHidden = false
        }else{
            playStopBtn.isHidden = true
            pauseBtn.isHidden = false
            do{
                let filename = getDirectory().appendingPathComponent("output.mp3")
                let path = Data(base64Encoded: indexRow)
                try? path!.write(to: filename, options: .atomicWrite)
                player = AVPlayer(url: filename)
                player.play()
                addPeriodicTimeObserver()
                self.audioProgressBar.progress = 0
             }
        }
            player.volume = 10.0
    }
    func getDirectory() -> URL {
        
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = path[0]
        print(documentDirectory)
        return documentDirectory
    }
    func addPeriodicTimeObserver() {
        // Notify every half second
        let timeScale = CMTimeScale(NSEC_PER_SEC)
        let time = CMTime(seconds: 0.5, preferredTimescale: timeScale)
        timeObserverToken = player.addPeriodicTimeObserver(forInterval: time,
                                                          queue: .main) {
            [weak self] time in
            if(ceilf(self!.audioProgressBar.progress * 10) == 10){
                self!.removePeriodicTimeObserver()
                self!.audioProgressBar.setProgress(0, animated: true)
                self!.pauseBtn.isHidden = true
                self!.playStopBtn.isHidden = false
            }
            let currentTime = Float(CMTimeGetSeconds(self!.player.currentTime()))
            self!.duration = Float(CMTimeGetSeconds(self!.player.currentItem!.duration))
            self!.dur = floorf((currentTime/self!.duration!) * self!.duration!)
            if (self!.dur! < 0){
                self!.audioDuration.text = "0"
            }else{
                self!.audioDuration.text = "\(self!.dur!)"
            }
            self!.audioProgressBar.setProgress(currentTime/self!.duration, animated: true)
        }
    }
    func removePeriodicTimeObserver() {
        if let timeObserverToken = timeObserverToken {
            player.removeTimeObserver(timeObserverToken)
            self.timeObserverToken = nil
        }
    }
}
extension String {
    func urlSafeBase64Decoded() -> String? {
        var st = self
            .replacingOccurrences(of: "_", with: "/")
            .replacingOccurrences(of: "-", with: "+")
        let remainder = self.count % 4
        if remainder > 0 {
            st = self.padding(toLength: self.count + 4 - remainder,
                              withPad: "=",
                              startingAt: 0)
        }
        guard let d = Data(base64Encoded: st, options: .ignoreUnknownCharacters) else{
            return nil
        }
        return String(data: d, encoding: .utf8)
    }
}
