//
//  Chat+CoreDataClass.swift
//  
//
//  Created by Muhammad Huzaifa on 09/11/2020.
//
//

import Foundation
import CoreData
import UIKit

@objc(Chat)
public class Chat: NSManagedObject {
    public func getMessagesData(id: String) -> [Chat]{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return [Chat()]
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let MessageFetchRequest = NSFetchRequest<Chat>(entityName: "Messages")

        do{
            let messageData = try managedObjectContext.fetch(MessageFetchRequest)
            return messageData
            
        }catch let error as NSError {
            print(error)
            return [Chat()]
        }
    }
    public func saveMessagesData() -> Bool {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return false
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        do{
            try managedObjectContext.save()
            return true
            
        }catch let error as NSError {
            print(error)
            return false
        }
    }
    public func getReceiveMessagesFromChannelOrIndividual(channelIdOrReceiverId: String) -> [Chat]{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return [Chat()]
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let MessagesFetchRequest = NSFetchRequest<Chat>(entityName: "Messages")

        MessagesFetchRequest.predicate = NSPredicate(format: "from == %@", channelIdOrReceiverId)
        
        do{
            let MessagesData = try managedObjectContext.fetch(MessagesFetchRequest)
            return MessagesData
            
        }catch let error as NSError {
            print(error)
            return [Chat()]
        }
    }
}
