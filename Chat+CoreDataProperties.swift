//
//  Chat+CoreDataProperties.swift
//  
//
//  Created by Muhammad Huzaifa on 09/11/2020.
//
//

import Foundation
import CoreData


extension Chat {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Chat> {
        return NSFetchRequest<Chat>(entityName: "Chat")
    }

    @NSManaged public var audio: String?
    @NSManaged public var files: String?
    @NSManaged public var from: String?
    @NSManaged public var message: String?
    @NSManaged public var timestamp: Date?
    @NSManaged public var video: String?

}
