//
//  Channel+CoreDataClass.swift
//  
//
//  Created by abdullah Javed on 09/11/2020.
//
//

import Foundation
import CoreData
import UIKit

@objc(Channel)
public class Channel: NSManagedObject {
    public func getChannelData(id: String) -> [Channel]{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return [Channel()]
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let channelFetchRequest = NSFetchRequest<Channel>(entityName: "Channel")

        do{
            let companiesData = try managedObjectContext.fetch(channelFetchRequest)
            return companiesData
            
        }catch let error as NSError {
            print(error)
            return [Channel()]
        }
    }
    public func saveChannelData(channel:Channel) -> Bool {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return false
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        do{
            try managedObjectContext.save()
            return true
            
        }catch let error as NSError {
            print(error)
            return false
        }
    }
    public func getSpecificChannelData(id: String) -> Channel{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return Channel()
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let channelFetchRequest = NSFetchRequest<Channel>(entityName: "channel")

        channelFetchRequest.predicate = NSPredicate(format: "id == %@", id)
        
        do{
            let channelsData = try managedObjectContext.fetch(channelFetchRequest)
            return channelsData[0]
            
        }catch let error as NSError {
            print(error)
            return Channel()
        }
    }
}
