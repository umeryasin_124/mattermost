//
//  Channel+CoreDataProperties.swift
//  
//
//  Created by abdullah Javed on 09/11/2020.
//
//

import Foundation
import CoreData


extension Channel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Channel> {
        return NSFetchRequest<Channel>(entityName: "Channel")
    }

    @NSManaged public var channelDescription: String?
    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var type: String?

}
