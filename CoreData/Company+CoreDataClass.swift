//
//  Company+CoreDataClass.swift
//  
//
//  Created by abdullah Javed on 09/11/2020.
//
//

import Foundation
import UIKit
import CoreData

@objc(Company)
public class Company: NSManagedObject {
    public func getCompanyData(token: String) -> [Company]{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return [Company()]
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let companyFetchRequest = NSFetchRequest<Company>(entityName: "Company")

        do{
            let companiesData = try managedObjectContext.fetch(companyFetchRequest)
            return companiesData
            
        }catch let error as NSError {
            print(error)
            return [Company()]
        }
    }
    public func saveCompanyData(company:Company,teams: NSSet) -> Bool {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return false
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        company.teams = teams
        do{
            try managedObjectContext.save()
            return true
            
        }catch let error as NSError {
            print(error)
            return false
        }
    }
    public func getSpecificCompanyData(id: String) -> Company{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return Company()
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let companyFetchRequest = NSFetchRequest<Company>(entityName: "Company")

        companyFetchRequest.predicate = NSPredicate(format: "id == %@", id)
        
        do{
            let companiesData = try managedObjectContext.fetch(companyFetchRequest)
            return companiesData[0]
            
        }catch let error as NSError {
            print(error)
            return Company()
        }
    }
}
