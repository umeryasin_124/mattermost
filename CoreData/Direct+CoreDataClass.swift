//
//  Direct+CoreDataClass.swift
//  
//
//  Created by abdullah Javed on 09/11/2020.
//
//

import Foundation
import CoreData
import UIKit

@objc(Direct)
public class Direct: NSManagedObject {
    public func getDirectData(id: String) -> [Direct]{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return [Direct()]
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let DirectFetchRequest = NSFetchRequest<Direct>(entityName: "Direct")

        do{
            let directData = try managedObjectContext.fetch(DirectFetchRequest)
            return directData
            
        }catch let error as NSError {
            print(error)
            return [Direct()]
        }
    }
    public func saveDirectData(Direct:Direct) -> Bool {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return false
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        do{
            try managedObjectContext.save()
            return true
            
        }catch let error as NSError {
            print(error)
            return false
        }
    }
    public func getSpecificDirectData(id: String) -> Direct{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return Direct()
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let DirectFetchRequest = NSFetchRequest<Direct>(entityName: "Direct")

        DirectFetchRequest.predicate = NSPredicate(format: "id == %@", id)
        
        do{
            let DirectsData = try managedObjectContext.fetch(DirectFetchRequest)
            return DirectsData[0]
            
        }catch let error as NSError {
            print(error)
            return Direct()
        }
    }
}
