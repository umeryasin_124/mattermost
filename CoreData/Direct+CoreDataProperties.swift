//
//  Direct+CoreDataProperties.swift
//  
//
//  Created by abdullah Javed on 09/11/2020.
//
//

import Foundation
import CoreData


extension Direct {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Direct> {
        return NSFetchRequest<Direct>(entityName: "Direct")
    }

    @NSManaged public var chat: Bool
    @NSManaged public var id: String?
    @NSManaged public var username: String?

}
