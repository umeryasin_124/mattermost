//
//  Messages+CoreDataClass.swift
//  
//
//  Created by abdullah Javed on 09/11/2020.
//
//

import Foundation
import CoreData
import UIKit

@objc(Messages)
public class Messages: NSManagedObject {
    public func getMessagesData(id: String) -> [Messages]{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return [Messages()]
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let MessageFetchRequest = NSFetchRequest<Messages>(entityName: "Messages")

        do{
            let messageData = try managedObjectContext.fetch(MessageFetchRequest)
            return messageData
            
        }catch let error as NSError {
            print(error)
            return [Messages()]
        }
    }
    public func saveMessagesData() -> Bool {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return false
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        do{
            try managedObjectContext.save()
            return true
            
        }catch let error as NSError {
            print(error)
            return false
        }
    }
    public func getChatByChannelId(channelId: String) -> [Messages]{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return [Messages()]
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let MessagesFetchRequest = NSFetchRequest<Messages>(entityName: "Messages")

        MessagesFetchRequest.predicate = NSPredicate(format: "channel_id == %@", channelId)
        
        do{
            let MessagesData = try managedObjectContext.fetch(MessagesFetchRequest)
            return MessagesData
            
        }catch let error as NSError {
            print(error)
            return [Messages()]
        }
    }
    public func getChatByReceiverId(receiverId: String) -> [Messages]{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return [Messages()]
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let MessagesFetchRequest = NSFetchRequest<Messages>(entityName: "Messages")

        MessagesFetchRequest.predicate = NSPredicate(format: "receiver_id == %@", receiverId)
        
        do{
            let MessagesData = try managedObjectContext.fetch(MessagesFetchRequest)
            return MessagesData
            
        }catch let error as NSError {
            print(error)
            return [Messages()]
        }
    }
}
