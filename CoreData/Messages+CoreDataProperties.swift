//
//  Messages+CoreDataProperties.swift
//  
//
//  Created by abdullah Javed on 09/11/2020.
//
//

import Foundation
import CoreData


extension Messages {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Messages> {
        return NSFetchRequest<Messages>(entityName: "Messages")
    }

    @NSManaged public var receiver_id: String?
    @NSManaged public var company_id: String?
    @NSManaged public var team_id: String?
    @NSManaged public var channel_id: String?
    @NSManaged public var message: String?
    @NSManaged public var files: String?
    @NSManaged public var audio: String?
    @NSManaged public var video: String?

}
