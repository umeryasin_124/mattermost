//
//  Team+CoreDataClass.swift
//  
//
//  Created by abdullah Javed on 09/11/2020.
//
//

import Foundation
import UIKit
import CoreData

@objc(Team)
public class Team: NSManagedObject {
    public func getTeamData(id: String) -> [Team]{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return [Team()]
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let TeamFetchRequest = NSFetchRequest<Team>(entityName: "Team")

        do{
            let teamData = try managedObjectContext.fetch(TeamFetchRequest)
            return teamData
            
        }catch let error as NSError {
            print(error)
            return [Team()]
        }
    }
    public func saveTeamData(team:Team,channels: NSSet,teamUsers: NSSet) -> Bool {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return false
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        team.channels = channels
        team.teamUsers = teamUsers
        do{
            try managedObjectContext.save()
            return true
            
        }catch let error as NSError {
            print(error)
            return false
        }
    }
    public func getSpecificTeamData(id: String) -> Team{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return Team()
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let TeamFetchRequest = NSFetchRequest<Team>(entityName: "Team")

        TeamFetchRequest.predicate = NSPredicate(format: "id == %@", id)
        
        do{
            let TeamsData = try managedObjectContext.fetch(TeamFetchRequest)
            return TeamsData[0]
            
        }catch let error as NSError {
            print(error)
            return Team()
        }
    }
}
