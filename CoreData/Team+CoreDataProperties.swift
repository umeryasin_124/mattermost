//
//  Team+CoreDataProperties.swift
//  
//
//  Created by abdullah Javed on 09/11/2020.
//
//

import Foundation
import CoreData


extension Team {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Team> {
        return NSFetchRequest<Team>(entityName: "Team")
    }

    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var channels: NSSet?
    @NSManaged public var teamUsers: NSSet?

}

// MARK: Generated accessors for channels
extension Team {

    @objc(addChannelsObject:)
    @NSManaged public func addToChannels(_ value: Channel)

    @objc(removeChannelsObject:)
    @NSManaged public func removeFromChannels(_ value: Channel)

    @objc(addChannels:)
    @NSManaged public func addToChannels(_ values: NSSet)

    @objc(removeChannels:)
    @NSManaged public func removeFromChannels(_ values: NSSet)

}

// MARK: Generated accessors for teamUsers
extension Team {

    @objc(addTeamUsersObject:)
    @NSManaged public func addToTeamUsers(_ value: Direct)

    @objc(removeTeamUsersObject:)
    @NSManaged public func removeFromTeamUsers(_ value: Direct)

    @objc(addTeamUsers:)
    @NSManaged public func addToTeamUsers(_ values: NSSet)

    @objc(removeTeamUsers:)
    @NSManaged public func removeFromTeamUsers(_ values: NSSet)

}
