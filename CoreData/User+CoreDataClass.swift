//
//  User+CoreDataClass.swift
//  
//
//  Created by abdullah Javed on 09/11/2020.
//
//

import Foundation
import UIKit
import CoreData

@objc(User)
public class User: NSManagedObject {
    func getUserData(token: String) -> User{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return User()
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let userFetchRequest = NSFetchRequest<User>(entityName: "User")
        
        userFetchRequest.predicate = NSPredicate(format: "token == %@", token)

        do{
            let user = try managedObjectContext.fetch(userFetchRequest)
            return user[0]
            
        }catch let error as NSError {
            print(error)
            return User()
        }
    }
    func getUserData(id: String) -> User{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return User()
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let userFetchRequest = NSFetchRequest<User>(entityName: "User")
        
        userFetchRequest.predicate = NSPredicate(format: "id == %@", id)

        do{
            let user = try managedObjectContext.fetch(userFetchRequest)
            return user[0]
            
        }catch let error as NSError {
            print(error)
            return User()
        }
    }
    func saveUserData(user:User,companies: NSSet) -> Bool {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return false
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        user.companies = companies
        do{
            try managedObjectContext.save()
            return true
            
        }catch let error as NSError {
            print(error)
            return false
        }
    }
    func editUserData(email: String) -> Bool{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return false
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let userFetchRequest = NSFetchRequest<User>(entityName: "User")
        userFetchRequest.predicate = NSPredicate(format: "email == %@", email)
        
        do{
            let user = try managedObjectContext.fetch(userFetchRequest)
            user[0].email = email
            
            try managedObjectContext.save()
            
            return true
            
        }catch let error as NSError {
            print(error)
            return false
        }
    }
    func deleteUser(email: String) -> Bool{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return false
        }
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let userFetchRequest = NSFetchRequest<User>(entityName: "User")
        userFetchRequest.predicate = NSPredicate(format: "email == %@", email)
        
        do{
            let user = try managedObjectContext.fetch(userFetchRequest)
            for obj in user {
                managedObjectContext.delete(obj)
            }
            
            try managedObjectContext.save()
            
            return true
            
        }catch let error as NSError {
            print(error)
            return false
        }
    }
}
