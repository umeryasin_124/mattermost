//
//  User+CoreDataProperties.swift
//  
//
//  Created by abdullah Javed on 09/11/2020.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var admin: Bool
    @NSManaged public var email: String?
    @NSManaged public var id: String?
    @NSManaged public var profile_picture: String?
    @NSManaged public var token: String?
    @NSManaged public var username: String?
    @NSManaged public var companies: NSSet?

}

// MARK: Generated accessors for companies
extension User {

    @objc(addCompaniesObject:)
    @NSManaged public func addToCompanies(_ value: Company)

    @objc(removeCompaniesObject:)
    @NSManaged public func removeFromCompanies(_ value: Company)
}
