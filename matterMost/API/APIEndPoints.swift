//
//  APIEndPoints.swift
//  matterMost
//
//  Created by abdullah Javed on 10/11/2020.
//

import Foundation

public class ENDPOINTS{
    
    static var firstLogin = "https://chat1.pf.com.pk/api/firstLogin"
    static var login = "https://chat1.pf.com.pk/api/login"
    static var logout = "https://chat1.pf.com.pk/api/logout"
    static var forgetPassword = "https://chat1.pf.com.pk/api/forgotPassword"
    static var enterOTP = "https://chat1.pf.com.pk/api/enterOtp"
    static var newPassword = "https://chat1.pf.com.pk/api/enterNewPassword"
    static var changePassword = "https://chat1.pf.com.pk/api/changepassword"
    static var searchByEmail = "https://chat1.pf.com.pk/api/user/searchUserByEmail"
    static var searchByUsername = "https://chat1.pf.com.pk/api/user/searchUserByUsername"
    static var updateUserName = "https://chat1.pf.com.pk/api/updateusername"
    static var createChannel = "https://chat1.pf.com.pk/api/channel/create"
    static var addTeamMember = "https://chat1.pf.com.pk/api/admin/addTeamMember"
    static var updateChannel = "https://chat1.pf.com.pk/api/channel/update"
    static var userInChannels = "https://chat1.pf.com.pk/api/channel/show"
    static var deleteMessage = "https://chat1.pf.com.pk/api/message/deleteMessage"
    static var editMessage = "https://chat1.pf.com.pk/api/message/editMessage"
    static var fetchIndividualMessages = "https://chat1.pf.com.pk/api/message/chat-receiver-id"
    static var fetchChannelMessages = "https://chat1.pf.com.pk/api/message/chat-group-id"
    static var sendMessage = "https://chat1.pf.com.pk/api/message/sendMessageIos"
    
    //static var sendMessage = "https://chat1.pf.com.pk/api/message/sendMessage"

    
    static var updateUserProfile = "https://chat1.pf.com.pk/api/updateuserprofile"
    static var insertUserInChannel = "https://chat1.pf.com.pk/api/channel/insert-user"
}
