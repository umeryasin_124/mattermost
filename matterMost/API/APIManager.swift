//
//  APIManager.swift
//  matterMost
//
//  Created by abdullah Javed on 10/11/2020.
//

import Foundation
import UIKit

class APIManager{
    var dictionary: [String:Any]
    var errorDetails: [String:Any]
    
    init() {
        dictionary = [String:Any]()
        errorDetails = [String:Any]()
    }
    public func login(email:String,password:String,completion: @escaping (Result<[String:Any],CustomError>) -> Void){
        dictionary["email"] = email
        dictionary["password"] = password
        let jsonData = try! JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        let url = URL(string: ENDPOINTS.login)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        request.cachePolicy = .reloadIgnoringLocalCacheData
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil else{
                completion(.failure(CustomError(title: error!.localizedDescription, code: STATUSCODES.INTERNAL_SERVER_ERROR.rawValue)))
                return
            }
            let httpresponse = response as? HTTPURLResponse
            let responseJSON:[String:Any]
            do{
                responseJSON = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                if(httpresponse?.statusCode == 200 || httpresponse?.statusCode == 201){
                    completion(.success(responseJSON))
                }else if(httpresponse?.statusCode == 402){
                    let error = CustomError(title: "Invalid Request Parameters", code: httpresponse!.statusCode)
                    completion(.failure(error))
                }
                else{
                    var error: CustomError!
                    if(!(responseJSON["status_message"] == nil)){
                        error = CustomError(title: responseJSON["status_message"] as!  String, code: httpresponse!.statusCode)
                    }else{
                        error = CustomError(title: "Something Went Wrong!", code: httpresponse!.statusCode)
                    }
                    completion(.failure(error))
                }
            }catch{
                let error = CustomError(title: "INVALID JSON", code: httpresponse!.statusCode)
                completion(.failure(error))
            }
        }.resume()
    }
    
    //API CALL WITH TOKEN
    public func apiCaller(url: String,sendBodyData: [String:Any],httpMethod: String,token:String,
                          completion: @escaping (Result<[String:Any],CustomError>) -> Void){
        let jsonData = try! JSONSerialization.data(withJSONObject: sendBodyData, options: .prettyPrinted)
        
        let url = URL(string: url)!
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(token, forHTTPHeaderField: "token")
        
        request.httpBody = jsonData
        request.cachePolicy = .reloadIgnoringLocalCacheData
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil else{
                completion(.failure(CustomError(title: error!.localizedDescription, code: STATUSCODES.INTERNAL_SERVER_ERROR.rawValue)))
                return
            }
            let httpresponse = response as? HTTPURLResponse
            let responseJSON:[String:Any]
            do{
                responseJSON = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                if(httpresponse?.statusCode == 200 || httpresponse?.statusCode == 201){
                    completion(.success(responseJSON))
                }else if(httpresponse?.statusCode == 402){
                    let error = CustomError(title: "Please check your parameters", code: httpresponse!.statusCode)
                    completion(.failure(error))
                }else{
                    print(responseJSON)
                    var error: CustomError!
                    if(!(responseJSON["status_message"] == nil)){
                        error = CustomError(title: responseJSON["status_message"] as!  String, code: httpresponse!.statusCode)
                    }else{
                        error = CustomError(title: "Something Went Wrong!", code: httpresponse!.statusCode)
                    }
                    completion(.failure(error))
                }
            }catch{
                let error = CustomError(title: "INVALID JSON", code: httpresponse!.statusCode)
                completion(.failure(error))
            }
        }.resume()
    }
    
    //API CALL WITHOUT TOKEN
    public func apiCaller(url: String,sendBodyData: [String:Any],httpMethod: String,
                          completion: @escaping (Result<[String:Any],CustomError>) -> Void){
        let jsonData = try! JSONSerialization.data(withJSONObject: sendBodyData, options: .prettyPrinted)
        
        let url = URL(string: url)!
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = jsonData
        request.cachePolicy = .reloadIgnoringLocalCacheData
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil else{
                completion(.failure(CustomError(title: error!.localizedDescription, code: STATUSCODES.INTERNAL_SERVER_ERROR.rawValue)))
                return
            }
            let httpresponse = response as? HTTPURLResponse
            let responseJSON = try! JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
            print(responseJSON)
            if(httpresponse?.statusCode == 200){
                completion(.success(responseJSON))
            }else if(httpresponse?.statusCode == 402){
                let error = CustomError(title: "Please check your parameters", code: httpresponse!.statusCode)
                completion(.failure(error))
            }
            else{
                var error: CustomError!
                if(!(responseJSON["status_message"] == nil)){
                    error = CustomError(title: responseJSON["status_message"] as!  String, code: httpresponse!.statusCode)
                }else{
                    error = CustomError(title: "Something Went Wrong!", code: httpresponse!.statusCode)
                }
                completion(.failure(error))
            }
        }.resume()
    }
    
    //API TO UPLOAD DATA TO SERVER
    public func uploadFileToServer(url: String,imageData:Data,httpMethod: String,token: String,completion: @escaping (Result<[String:Any],CustomError>) -> Void){
        
        let url = URL(string: url)!
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        request.addValue(token, forHTTPHeaderField: "token")
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpBody = createBodyWithParameters(filePathKey: "user_profile", imageDataKey: imageData, boundary: boundary)
        request.cachePolicy = .reloadIgnoringLocalCacheData
        URLSession.shared.dataTask(with: request){ (data, response, error) in
            guard let data = data, error == nil else{
                completion(.failure(CustomError(title: error!.localizedDescription, code: STATUSCODES.INTERNAL_SERVER_ERROR.rawValue)))
                return
            }
            let httpresponse = response as? HTTPURLResponse
            let responseJSON = try! JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
            print(responseJSON)
            if(httpresponse?.statusCode == 200){
                completion(.success(responseJSON))
            }else if(httpresponse?.statusCode == 402){
                let error = CustomError(title: "Please check your parameters", code: httpresponse!.statusCode)
                completion(.failure(error))
            }
            else{
                var error: CustomError!
                if(!(responseJSON["status_message"] == nil)){
                    error = CustomError(title: responseJSON["status_message"] as!  String, code: httpresponse!.statusCode)
                }else{
                    error = CustomError(title: "Something Went Wrong!", code: httpresponse!.statusCode)
                }
                completion(.failure(error))
            }
        }.resume()
    }
    public func uploadFileToServer(dict:[String:Any],url: String,imageData:Data,httpMethod: String,token: String,filePathKey:String,extensionT:String
                                   ,completion: @escaping (Result<[String:Any],CustomError>) -> Void){
        
        let url = URL(string: url)!
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        request.addValue(token, forHTTPHeaderField: "token")
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpBody = createBodyWithParametersAndDictionary(dict: dict,filePathKey: filePathKey, imageDataKey: imageData, boundary: boundary,extensionType: extensionT)
        request.cachePolicy = .reloadIgnoringLocalCacheData
        URLSession.shared.dataTask(with: request){ (data, response, error) in
            guard let data = data, error == nil else{
                completion(.failure(CustomError(title: error!.localizedDescription, code: STATUSCODES.INTERNAL_SERVER_ERROR.rawValue)))
                return
            }
            let httpresponse = response as? HTTPURLResponse
            let responseJSON = try! JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
            print(responseJSON)
            if(httpresponse?.statusCode == 200){
                completion(.success(responseJSON))
            }else if(httpresponse?.statusCode == 402){
                let error = CustomError(title: "Please check your parameters", code: httpresponse!.statusCode)
                completion(.failure(error))
            }
            else{
                let error = CustomError(title: responseJSON["status_message"] as! String, code: httpresponse!.statusCode)
                completion(.failure(error))
            }
        }.resume()
    }
    public func downloadFileToServerViaURL(url: String,httpMethod: String,token: String,completion: @escaping (Result<URL,CustomError>) -> Void){
        
        let url = URL(string: url)!
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        request.addValue(token, forHTTPHeaderField: "token")
        URLSession.shared.downloadTask(with: request) { (url, response, error) in
            guard let url = url, error == nil else{
                completion(.failure(CustomError(title: error!.localizedDescription, code: STATUSCODES.INTERNAL_SERVER_ERROR.rawValue)))
                return
            }
            let httpresponse = response as? HTTPURLResponse
            if(httpresponse?.statusCode == 200){
                completion(.success(url))
            }else if(httpresponse?.statusCode == 402){
                let error = CustomError(title: "Please check your parameters", code: httpresponse!.statusCode)
                completion(.failure(error))
            }else if(httpresponse?.statusCode == 405){
                let error = CustomError(title: "Method Not Allowed", code: httpresponse!.statusCode)
                completion(.failure(error))
            }
            else{
                let error = CustomError(title: "Something Went Wrong!", code: httpresponse!.statusCode)
                completion(.failure(error))
            }
        }.resume()
    }
    public func downloadFileToServer(url: String,httpMethod: String,token: String,completion: @escaping (Result<Data,CustomError>) -> Void){
        
        let url = URL(string: url)!
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        request.addValue(token, forHTTPHeaderField: "token")
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data, error == nil else{
                completion(.failure(CustomError(title: error!.localizedDescription, code: STATUSCODES.INTERNAL_SERVER_ERROR.rawValue)))
                return
            }
            let httpresponse = response as? HTTPURLResponse
            if(httpresponse?.statusCode == 200){
                completion(.success(data))
            }else if(httpresponse?.statusCode == 402){
                let error = CustomError(title: "Please check your parameters", code: httpresponse!.statusCode)
                completion(.failure(error))
            }else if(httpresponse?.statusCode == 405){
                let error = CustomError(title: "Method Not Allowed", code: httpresponse!.statusCode)
                completion(.failure(error))
            }
            else{
                let error = CustomError(title: "Something Went Wrong!", code: httpresponse!.statusCode)
                completion(.failure(error))
            }
        }.resume()
    }
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    func createBodyWithParameters(filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
            var body = Data();

            let filename = "user-profile.jpg"

            let mimetype = "image/jpg"

            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
            body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
            body.append(imageDataKey)
            body.appendString(string: "\r\n")

            body.appendString(string: "--\(boundary)--\r\n")

            return body
    }
    func createBodyWithParametersAndDictionary(dict:[String:Any],filePathKey: String?, imageDataKey: Data, boundary: String,extensionType:String) -> Data {
        var body = Data();

        dict.forEach { (key, value) in
            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString(string: "\(value)\r\n")
        }
        
        let filename = "user-profile" + extensionType

        let mimetype = "image/jpg"

        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString(string: "\r\n")

        body.appendString(string: "--\(boundary)--\r\n")

        return body
}
    public func errorExistOrNot(statusCode: Int) -> Error{
        switch statusCode {
        case STATUSCODES.BAD_REQUEST.rawValue:
            return STATUSCODES.BAD_REQUEST
        case STATUSCODES.UNAUTHORIZED.rawValue:
            return STATUSCODES.UNAUTHORIZED
        case STATUSCODES.FORBIDDEN.rawValue:
            return STATUSCODES.FORBIDDEN
        case STATUSCODES.RECORD_NOT_FOUND.rawValue:
            return STATUSCODES.RECORD_NOT_FOUND
        case STATUSCODES.METHOD_NOT_ALLOWED.rawValue:
            return STATUSCODES.METHOD_NOT_ALLOWED
        case STATUSCODES.INVALID_JSON.rawValue:
            return STATUSCODES.RECORD_NOT_FOUND
        case STATUSCODES.NOT_AVAILABLE.rawValue:
            return STATUSCODES.NOT_AVAILABLE
        case STATUSCODES.FAILED_REQUEST_SERVER.rawValue:
            return STATUSCODES.FAILED_REQUEST_SERVER
        case STATUSCODES.TOO_MANY_REQUESTS.rawValue:
            return STATUSCODES.TOO_MANY_REQUESTS
        case STATUSCODES.INTERNAL_SERVER_ERROR.rawValue:
            return STATUSCODES.INTERNAL_SERVER_ERROR
        case STATUSCODES.SERVICE_UNAVAILABLE.rawValue:
            return STATUSCODES.SERVICE_UNAVAILABLE
        default:
            return STATUSCODES.SERVICE_UNAVAILABLE
        }
    }
}
extension Data {
    mutating func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
