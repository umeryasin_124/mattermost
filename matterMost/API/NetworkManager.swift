//
//  NetworkManager.swift
//  matterMost
//
//  Created by abdullah Javed on 12/11/2020.
//

import Foundation

enum STATUSCODES:Int,Error {
    case SUCCESSFULL = 200
    case BAD_REQUEST = 400
    case UNAUTHORIZED = 401
    case FORBIDDEN = 403
    case RECORD_NOT_FOUND = 404
    case METHOD_NOT_ALLOWED = 405
    case INVALID_JSON = 406
    case NOT_AVAILABLE = 410
    case FAILED_REQUEST_SERVER = 418
    case TOO_MANY_REQUESTS = 429
    case INTERNAL_SERVER_ERROR = 500
    case SERVICE_UNAVAILABLE = 503
}
class CustomError: Error {

    var title: String?
    var code: Int
    
    init(title:String,code:Int) {
        self.title = title
        self.code = code
    }
}
