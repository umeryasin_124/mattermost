//
//  TableViewCell.swift
//  IOS Chat App Layout
//
//  Created by Muhammad Huzaifa on 02/11/2020.
//

import UIKit

protocol ChangeImage {
    func imageChange()
    func cellClicked(index:Int,isChecked: Bool)
}

class AddUsersInGroup: UITableViewCell {
    var parentController: UIViewController!
    var btnImage: Bool!
    
    @IBOutlet weak var labelTxt: UILabel!
    @IBOutlet weak var btnImg: UIButton!
    
    var delegate:ChangeImage!
    var index:Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    @IBAction func btnAction(_ sender: Any) {
        if(btnImage){
            btnImage = false
            btnImg.setImage(UIImage(named : "checked"), for: .normal)
            delegate.cellClicked(index: index, isChecked: true)
        }
        else{
            btnImage = true
            btnImg.setImage(UIImage(named : "unChecked"), for: .normal)
            delegate.cellClicked(index: index, isChecked: false)
        }

    }
    
}
