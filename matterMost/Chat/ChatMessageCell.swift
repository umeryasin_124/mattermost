//
//  ChatMessageCell.swift
//  chatscreen
//
//  Created by ethisham bader on 04/11/2020.
//

import UIKit

class ChatMessageCell: UITableViewCell {
    
    let messageLabel = UILabel()
    let timeStamp = UILabel()
    let bubbleBackgroundView = UIView()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        bubbleBackgroundView.layer.backgroundColor = UIColor(red: 5/255, green: 49/255, blue: 106/255, alpha: 1.0).cgColor
        messageLabel.textColor = .white
        bubbleBackgroundView.layer.cornerRadius = 5
        bubbleBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(bubbleBackgroundView)
        addSubview(timeStamp)
        addSubview(messageLabel)
        timeStamp.text = DateFormatter.localizedString(from: Date(), dateStyle: .none, timeStyle: .short)
        messageLabel.text = "Some message"
        timeStamp.textColor = UIColor.lightGray
        timeStamp.font = UIFont(name: timeStamp.font.fontName, size: 11)

        messageLabel.numberOfLines = 0
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        timeStamp.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            messageLabel.topAnchor.constraint(equalTo: topAnchor,constant: 16),
            messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -16),
            messageLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 250),
            bubbleBackgroundView.topAnchor.constraint(equalTo: messageLabel.topAnchor, constant: -12),
            bubbleBackgroundView.leadingAnchor.constraint(equalTo: messageLabel.leadingAnchor, constant: -16),
            bubbleBackgroundView.bottomAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 12),
            bubbleBackgroundView.trailingAnchor.constraint(equalTo: messageLabel.trailingAnchor, constant: 16),
            timeStamp.rightAnchor.constraint(equalTo: bubbleBackgroundView.rightAnchor,constant: -2),
            timeStamp.bottomAnchor.constraint(lessThanOrEqualTo: messageLabel.bottomAnchor,constant: 12),
            timeStamp.widthAnchor.constraint(lessThanOrEqualToConstant: 250)
        ]
        
        NSLayoutConstraint.activate(constraints)
        let leadingConstraint = messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 24)
        leadingConstraint.isActive = false
        let trailingContraint = messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24)
        trailingContraint.isActive = true
        let leadingConstraint1 = timeStamp.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 24)
        leadingConstraint1.isActive = false
        let trailingContraint1 = timeStamp.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10)
        trailingContraint1.isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
