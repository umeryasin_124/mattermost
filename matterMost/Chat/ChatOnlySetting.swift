//
//  ChatSettingsController.swift
//  matterMost
//
//  Created by ethisham bader on 14/11/2020.
//

import UIKit

class ChatOnlySetting: UITableViewController {
    
    @IBOutlet weak var muteImage: UIImageView!
    @IBOutlet weak var blockImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        changeImageColor()
        // Do any additional setup after loading the view.
    }
    
    @objc func changeImageColor(){
        let templateImage = self.muteImage.image!.withRenderingMode(.alwaysTemplate)
        self.muteImage.image = templateImage
        self.muteImage.tintColor = .darkGray
        
        let templateImage1 = self.blockImage.image!.withRenderingMode(.alwaysTemplate)
        self.blockImage.image = templateImage1
        self.blockImage.tintColor = .darkGray
    
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 0{
            let alert = UIAlertController(title: "Select Any One", message: "", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Mute for 6 hours", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
                
            }))
            alert.addAction(UIAlertAction(title: "Mute for 1 year", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
                
            }))
            alert.addAction(UIAlertAction(title: "Always Mute", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
                
            }))
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
                UIAlertAction in
            }
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }else if indexPath.section == 1 && indexPath.row == 0{
            let refreshAlert = UIAlertController(title: "Are you sure", message: "The user will be blocked", preferredStyle: UIAlertController.Style.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Block", style: .default, handler: { (action: UIAlertAction!) in
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
    }
}
