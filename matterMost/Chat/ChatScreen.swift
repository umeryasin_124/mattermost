//
//  ViewController.swift
//  chatscreen
//
//  Created by ethisham bader on 04/11/2020.
//

import UIKit
import AVFoundation
import IHKeyboardAvoiding
import AVKit
import ReplayKit
import ImageIO
import System
import SystemConfiguration

class ChatScreen: UIViewController,UITextViewDelegate,UIActionSheetDelegate,AVAudioPlayerDelegate,AVAudioRecorderDelegate , UISearchBarDelegate,UIScrollViewDelegate,UIScrollViewAccessibilityDelegate{
    
    @IBOutlet weak var recordScreenView: UIView!
    var videoString:String!
    var completeinfo: UserCompleteInfo!
    var i:Int = 0
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioPlayer:AVAudioPlayer!
    var searchBar: UISearchBar!
    var videoURL: URL?
    var numberOfRecords = 0;
    let myPickerController = UIImagePickerController()
    let documentPickerController = UIDocumentPickerViewController(documentTypes: ["public.item"], in: .import)
    var messagesHistory: [MessagesClass]!
    var filteredMessages = [MessagesClass]()
    var imageP,imageC: UIImage!
    var sendingMessage: Bool!
    var messageImages = [UIImage]()
    var documentPath:Data!
    // direct or channel name
    var userName: String!
    // direct or channel (id)
    var idT:Int!
    // direct or channel
    var idType:String!
    var audioIndex: Int!
    // Team id
    var extensionP:String!
    var teamID:Int!
    var selectteamindex: Int!
    var company_id:Int!
    var at: Bool!
    
    var audioString:String!
    var documentUrl: String!
    var documentSizeKB: Int!
    var documentString:String!
    @IBOutlet weak var titleChat: UINavigationItem!
    
    var trimmed: String = ""
    
    @IBOutlet weak var recordingInProgress: UILabel!
    @IBOutlet weak var gifPlayView: UIImageView!
    @IBOutlet weak var chatMessage: UITextView!
    @IBOutlet weak var voiceButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var emojiButton: UIButton!
    @IBOutlet weak var sheetAction: UIButton!
    @IBOutlet weak var buttonViews: UIView!
    @IBOutlet weak var profileImage: UIBarButtonItem!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var closePreview: UIButton!
    @IBOutlet weak var stopAudio: UIButton!
    var imageFromLibrary: UIImageView!
    var imageFromCamera: UIImageView!
    var choosedImage: Bool!
    var choosedAudio: Bool!
    var choosedVideo: Bool!
    var choosedDocument: Bool!
    var wantToShowHistory:Bool!
    var count = 0
    var index = 0
    var isSearching = false
    var sendingTime: String!
    @IBOutlet weak var tableView: UITableView!
    var y: CGFloat?
    var hasNotch: Bool!
    fileprivate let cellId = "id"
    var timer = Timer()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        //scheduledTimerWithTimeInterval()
        setTableViewAndViews()
        wantToShowHistory = false
        sendingMessage = true
        fetchMessages()
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(closeKeyboard))
        swipeDown.direction = .down
        view.addGestureRecognizer(swipeDown)
        
    }
    @objc func closeKeyboard(){
        self.view.endEditing(true)
    }
    func playGif(){
        gifPlayView.isHidden = false
        gifPlayView.loadGif(name: "audio")
        chatMessage.isHidden = true
    }
    func stopGif(){
        gifPlayView.isHidden = true
        chatMessage.isHidden = false
    }
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //Long press gesture
    func longPressGesture(){
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(sender:)))
        tableView.addGestureRecognizer(longPress)
    }
    @objc func handleLongPress(sender: UILongPressGestureRecognizer){
        if sender.state == .began{
            let menu = UIMenuController.shared
            becomeFirstResponder()
            let menuItemDelete = UIMenuItem(title: "Delete", action: #selector(handleMenuItemAction))
            let menuItemForward = UIMenuItem(title: "Forward", action: #selector(handleMenuItemAction))
            menu.menuItems = [menuItemDelete, menuItemForward]
            let location = sender.location(in: self.tableView)
            let menuLocation = CGRect(x: location.x, y: location.y,width: 0, height: 0)
            menu.setTargetRect(menuLocation, in: tableView)
            menu.setMenuVisible(true, animated: true)
        }
        
    }
    override var canBecomeFirstResponder: Bool{
        return true
    }
    
    @objc func handleMenuItemAction(){
        filteredMessages.remove(at: 0)
    }
    func editMessageDialog(indexPath:IndexPath,old_message:String){
        let alertController = UIAlertController(title: "Edit Your Message", message: "", preferredStyle: UIAlertController.Style.alert)
        let saveAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { [self] alert -> Void in
            let newMessage = alertController.textFields![0] as UITextField
            editMessageFromServer(indexPath: indexPath, newMessage: newMessage.text!)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
                                            (action : UIAlertAction!) -> Void in })
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter New Message"
            textField.text = old_message
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    func editMessageFromServer(indexPath:IndexPath,newMessage:String){
        let token = UserDefaults.standard.string(forKey: "token")
        var dicc = [String:Any]()
        dicc["message_id"] = filteredMessages[indexPath.row].id
        dicc["new_message"] = newMessage
        dicc["team_id"] = teamID
        dicc["company_id"] = company_id
        
        APIManager().apiCaller(url: ENDPOINTS.editMessage, sendBodyData: dicc, httpMethod: "POST", token: token!) { [self] (result) in
            switch result{
            case .success(let response):
                DispatchQueue.main.async {
                    showCustomAlert(title: "Message Edited", message: response["status_message"] as! String)
                    filteredMessages[indexPath.row].message = newMessage
                    tableView.reloadData()
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    showCustomAlert(title: "Error", message: error.title!)
                }
                break
            }
        }
    }
    func deleteMessageFromServer(indexPath:IndexPath){
        let token = UserDefaults.standard.string(forKey: "token")
        var dicc = [String:Any]()
        dicc["message_id"] = filteredMessages[indexPath.row].id!
        self.filteredMessages.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
        dicc["team_id"] = teamID
        dicc["company_id"] = company_id
        APIManager().apiCaller(url: ENDPOINTS.deleteMessage, sendBodyData: dicc, httpMethod: "POST", token: token!) { [self] (result) in
            switch result{
            case .success(let response):
                DispatchQueue.main.async {
                    showCustomAlert(title: "Message Deleted", message: response["status_message"] as! String)
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    showCustomAlert(title: "Error", message: error.title!)
                }
                break
            }
        }
        
    }
    @IBAction func cameraOpen(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            myPickerController.delegate = self
            myPickerController.sourceType = UIImagePickerController.SourceType.camera
            myPickerController.allowsEditing = false
            self.present(myPickerController,animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "UnExpected Behaviour", message: "Camera Not Avaliable", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            //canel out action
                                            
                                          }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func stopAudioRecording(_ sender: Any) {
        stopGif()
        voiceButton.isHidden = false
        stopAudio.isHidden = true
        
        chatMessage.text = "Type your message"
        chatMessage.isEditable = true
        audioRecorder.stop()
        choosedAudio = true
        audioRecorder = nil
        UserDefaults.standard.set(numberOfRecords, forKey: "myNumber")
        numberOfRecords += 1
        // wantToShowHistory = false
        
        count = count + 1
        let path = getDirectory().appendingPathComponent("\(String(describing: numberOfRecords - 1)).m4a")
        audioString = try? Data(contentsOf: path).base64EncodedString()
        filteredMessages.append(MessagesClass(user_id: -1,id: -1,audio: audioString!, timeStamp: "", userNamepp: completeinfo.data.username))
        tableView.insertRows(at: [IndexPath(row: index, section: 0)], with: .top)
        tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .bottom, animated: true)
        index = index + 1
        
        choosedAudio = true
        choosedImage = false
        choosedVideo = false
        choosedDocument = false
        sendButton.isHidden = false
        sendMessage()
        
        
    }
    @IBAction func startAudioRecording(_ sender: Any) {
        playGif()
        chatMessage.isEditable = false
        chatMessage.text = "Recording in progress.."
        voiceButton.isHidden = true
        sendButton.isHidden = true
        // sendButton.isEnabled = false
        stopAudio.isHidden = false
        audioIndex = numberOfRecords
        let filename = getDirectory().appendingPathComponent("\(numberOfRecords).m4a")
        if(audioRecorder == nil){
            
            let settings = [
                AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                AVSampleRateKey: 12000,
                AVNumberOfChannelsKey: 1,
                AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
            ]
            //Start Audio Recording
            do{
                audioRecorder = try AVAudioRecorder(url: filename, settings: settings)
                audioRecorder.delegate = self
                audioRecorder.record()
                print("successfully Record \(numberOfRecords)")
            }
            catch{
                diplayAlert(title: "Oops", message: "Recording Failed")
            }
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if((UIApplication.shared.keyWindow?.safeAreaInsets.bottom) == nil){
            hasNotch = false
            // self.messageView.frame = CGRect(x: 24, y: self.view.frame.height - (self.messageView.frame.height + 20), width: messageView.frame.width, height: messageView.frame.height)
        }else{
            if ((UIApplication.shared.keyWindow?.safeAreaInsets.bottom)! > 0){
                hasNotch = true
            }else{
                hasNotch = false
                //self.messageView.frame = CGRect(x: 24, y: self.view.frame.height - (self.messageView.frame.height + 20), width: messageView.frame.width, height: messageView.frame.height)
            }
        }
    }
    func getDirectory() -> URL {
        
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = path[0]
        print(documentDirectory)
        return documentDirectory
    }
    
    func diplayAlert(title:String, message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title:"Dismiss", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    func parseTimeStamp(timeResult:String) -> String{
        let start = timeResult.index(timeResult.startIndex, offsetBy: 11)
        let end = timeResult.index(timeResult.endIndex, offsetBy: -11)
        let range = start..<end
        let mySubstring = timeResult[range]  // play
        return String(mySubstring)
    }
    
    @IBAction func send(_ sender: Any) {
        // wantToShowHistory.
        sendingMessage = false
        count = count + 1
        trimmed = chatMessage.text.trimmingCharacters(in: .whitespacesAndNewlines)
        sendingTime = DateFormatter.localizedString(from: Date(), dateStyle: .none, timeStyle: .short)
        if(imageP == nil && imageC == nil){
            if(!(chatMessage.text == "Type your message") && !(trimmed.isEmpty)){
                sendMessage()
                filteredMessages.append(MessagesClass(user_id: -1,id: -1,message: trimmed, timeStamp: sendingTime,userNamepp: completeinfo.data.username))
                tableView.insertRows(at: [IndexPath(row: index, section: 0)], with: .top)
                //tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .top, animated: true)
                index = index + 1
                chatMessage.text = ""
                voiceButton.isHidden = false
                cameraButton.isHidden = false
                choosedImage = false
                choosedAudio = false
                choosedVideo = false
                choosedDocument = false
                closePreview.isHidden = true
                imagePreview.isHidden = true
                self.view.endEditing(false)
            }
        }else{
            filteredMessages.append(MessagesClass(user_id: -1,id: -1,image: "", timeStamp: "", userNamepp: completeinfo.data.username))
            sendMessage()
            tableView.insertRows(at: [IndexPath(row: index, section: 0)], with: .top)
            //tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .top, animated: true)
            index = index + 1
            chatMessage.text = ""
            voiceButton.isHidden = false
            cameraButton.isHidden = false
            choosedImage = false
            choosedAudio = false
            choosedVideo = false
            choosedDocument = false
            closePreview.isHidden = true
            imagePreview.isHidden = true
            self.view.endEditing(false)
        }
    }
    @IBAction func closePreviewView(_ sender: Any) {
        choosedImage = false
        closePreview.isHidden = true
        imagePreview.isHidden = true
    }
    @IBAction func sheetAction(_ sender: Any) {
        let alert = UIAlertController(title: "Select Any One", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Document", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.documentLibrary()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
            UIAlertAction in
        }
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func btnSearchAction() {
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: (self.navigationController?.navigationBar.frame.width)!, height: (self.navigationController?.navigationBar.frame.height)!))
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        searchBar.backgroundColor = .white
        self.navigationController?.navigationBar.addSubview(searchBar)
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.isHidden = true
        filteredMessages = messagesHistory
        UIApplication.shared.sendAction(#selector(resignFirstResponder), to: nil, from: nil, for: nil)
        tableView.reloadData()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if(searchText.isEmpty){
            wantToShowHistory = true
            isSearching = false
            filteredMessages = messagesHistory
            tableView.reloadData()
        }else{
            wantToShowHistory = true
            isSearching = true
            filteredMessages = messagesHistory.filter { $0.message.contains(searchText)}
            tableView.reloadData()
        }
    }
    @IBAction func attachSheet(_ sender: Any) {
        btnSearchAction()
    }
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    @IBAction func chatSettings(_ sender: Any) {
        
        if at {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ChatSettingController") as! ChatSettingController
            vc.isChatType = true
            vc.selectedTeamID = selectteamindex
            vc.teamID = teamID
            vc.channelID = idT
            vc.CompleteInfo = completeinfo
            vc.companyID = company_id
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ChatOnlySetting") as! ChatOnlySetting
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func showCustomAlert(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    func getThumbnailImage(url: URL) -> UIImage{
        let asset = AVAsset(url: url)
        let avAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        avAssetImageGenerator.appliesPreferredTrackTransform = true
        var time = asset.duration
        time.value = min(time.value,2)
        do{
            avAssetImageGenerator.requestedTimeToleranceBefore = .zero
            avAssetImageGenerator.requestedTimeToleranceAfter = .zero
            let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: time, actualTime: nil)
            let thumbImage = UIImage(cgImage: cgThumbImage)
            return thumbImage
        }catch{
            print(error.localizedDescription)
        }
        return UIImage()
    }
    func callApi(dictt: [String:Any]){
        let token = UserDefaults.standard.string(forKey: "token")
        let apiin = APIManager()
        apiin.apiCaller(url: ENDPOINTS.sendMessage, sendBodyData: dictt, httpMethod: "POST",token: token!) { [self] (result) in
            switch result{
            case .success(let response):
                print(response)
                wantToShowHistory = true
                break
            case .failure(let error):
                print(error)
                DispatchQueue.main.async {
                    self.showCustomAlert(title: "sendingoplo Failed", message: error.title!)
                }
            }
        }
    }
    func setTableViewAndViews(){
        wantToShowHistory = false
        sendingMessage = true
        
        let broadcastPickerView = RPSystemBroadcastPickerView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        recordScreenView.addSubview(broadcastPickerView)
        broadcastPickerView.preferredExtension = "com.milan.nosal.broadcast-extension"
        broadcastPickerView.backgroundColor = .clear
        titleChat.title = userName
        tableView.isUserInteractionEnabled = true
        KeyboardAvoiding.avoidingView = self.view
        messagesHistory = [MessagesClass]()
        //        y = self.messageView.frame.minY
        stopAudio.isHidden = true
        imagePreview.isHidden = true
        closePreview.isHidden = true
        choosedImage = false
        choosedAudio = false
        choosedVideo = false
        choosedDocument = false
        imageFromLibrary = UIImageView()
        imageFromCamera = UIImageView()
        tableView.register(UINib(nibName: "TextMessageCell", bundle: nil), forCellReuseIdentifier: "textcell")
        tableView.register(UINib(nibName: "ImageMessageCell", bundle: nil), forCellReuseIdentifier: "imagecell")
        tableView.register(UINib(nibName: "AudioCell", bundle: nil), forCellReuseIdentifier: "AudioCell")
        tableView.register(UINib(nibName: "VideoMessageCell", bundle: nil), forCellReuseIdentifier: "videocell")
        tableView.register(UINib(nibName: "DocumentMessageCell", bundle: nil), forCellReuseIdentifier: "documentcell")
        tableView.separatorStyle = .none
        chatMessage.delegate = self
        self.chatMessage.delegate = self
        tableView.tableFooterView = UIView.init(frame: .zero)
        tableView.delegate = self
        tableView.dataSource = self
        chatMessage.delegate = self
        chatMessage.textColor = UIColor.lightGray
        sendButton.layer.borderColor = UIColor.lightGray.cgColor
        sendButton.layer.borderWidth = 1.0
    }
}
