//
//  ChatScreenPickers.swift
//  matterMost
//
//  Created by abdullah Javed on 19/11/2020.
//

import Foundation
import UIKit
import AVFoundation

extension ChatScreen : UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIDocumentPickerDelegate{
    func documentLibrary()
    {
        documentPickerController.delegate = self
        self.present(documentPickerController, animated: true, completion: nil)
        
    }
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first?.lastPathComponent else {
            return
        }
        do {
            let resources = try urls.first!.resourceValues(forKeys:[.fileSizeKey])
            let fileSize = resources.fileSize!
            print("hello heree")
            print (fileSize / 1024)
            documentSizeKB = fileSize + 1
        } catch {
            print("Error: \(error)")
        }
        documentUrl = urls.first!.deletingPathExtension().lastPathComponent
        extensionP = urls.first?.pathExtension
        let filename = getDirectory().appendingPathComponent("output." + extensionP!)
        documentPath = try? Data(contentsOf: urls.first!)
        try? documentPath!.write(to: filename, options: .atomicWrite)
        documentString = documentPath?.base64EncodedString()
        filteredMessages.append(MessagesClass(user_id: -1,id: -1,document: documentUrl,extensionP: extensionP, size: "", userNamepp: completeinfo.data.username))
        choosedDocument = true
        sendMessage()
        count = count + 1
        wantToShowHistory = false
        tableView.insertRows(at: [IndexPath(row: index, section: 0)], with: .top)
        tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .top, animated: true)
        index = index + 1
        documentPickerController.dismiss(animated: true, completion: nil)
        print("import result : \(myURL)")
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        //dismiss(animated: true, completion: nil)
    }
    func photoLibrary(){
        myPickerController.sourceType = .photoLibrary
        myPickerController.delegate = self;
        myPickerController.mediaTypes = ["public.image", "public.movie"]
        myPickerController.allowsEditing = true
        self.present(myPickerController, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let videoURL = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerMediaURL")] as? URL{
            choosedVideo = true
            choosedAudio = false
            choosedImage = false
            choosedDocument = false
            count = count + 1
            self.videoURL = videoURL
            videoString = try? Data(contentsOf: videoURL).base64EncodedString()
            filteredMessages.append(MessagesClass(user_id: -1,id: -1,video: videoString!, timeStamp: "", userNamepp: completeinfo.data.username))
            tableView.insertRows(at: [IndexPath(row: index, section: 0)], with: .top)
            tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .top, animated: true)
            index = index + 1
            wantToShowHistory = false
            sendMessage()
            myPickerController.dismiss(animated: true, completion: nil)
        }
        else{
            if(picker.sourceType == .photoLibrary){
                imageP = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage
                imagePreview.image = imageP
            }else{
                imageC = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerOriginalImage")] as? UIImage
                imagePreview.image = imageC
            }
            wantToShowHistory = false
            choosedImage = true
            imageFromLibrary.image = imageP
            imageFromCamera.image = imageC
            imagePreview.isHidden = false
            closePreview.isHidden = false
            wantToShowHistory = false
                        // sendButton.isHidden = false
           // sendButton.isEnabled = true
            voiceButton.isHidden = true
            cameraButton.isHidden = true
            myPickerController.dismiss(animated: true, completion: nil)
        }
    }
    func previewImageFromVideo(url:NSURL) -> UIImage? {
        let asset = AVAsset(url:url as URL)
        let imageGenerator = AVAssetImageGenerator(asset:asset)
        imageGenerator.appliesPreferredTrackTransform = true
        
        var time = asset.duration
        time.value = min(time.value,2)
        
        do {
            let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            return nil
        }
    }
    
}
extension UIViewController {
    func hideKeyboardWhenTappedAround(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
}
extension String {
    
    func fromBase64URL() -> String? {
        var base64 = self
        base64 = base64.replacingOccurrences(of: "-", with: "+")
        base64 = base64.replacingOccurrences(of: "_", with: "/")
        while base64.count % 4 != 0 {
            base64 = base64.appending("=")
        }
        guard let data = Data(base64Encoded: base64) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64URL() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}

