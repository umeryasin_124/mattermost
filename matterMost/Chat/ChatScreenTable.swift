//
//  ChatScreenTable.swift
//  matterMost
//
//  Created by abdullah Javed on 19/11/2020.
//

import Foundation
import UIKit

extension ChatScreen: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredMessages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(wantToShowHistory){
            if(filteredMessages.count > 0 && indexPath.row < filteredMessages.count){
                if(filteredMessages[indexPath.row].image != nil){
                    choosedImage = false
                    let cell = tableView.dequeueReusableCell(withIdentifier: "imagecell", for: indexPath) as! ImageMessageCell
                    let token = UserDefaults.standard.string(forKey: "token")
                    let path = filteredMessages[indexPath.row].image
                    APIManager().downloadFileToServer(url: path!, httpMethod: "POST", token: token!) { (result) in
                        switch result{
                        case .success(let response):
                            DispatchQueue.main.async {
                                cell.imageView?.image = self.resizeImage(image: UIImage(data: response)!, targetSize: CGSize(width: 259, height: 265))
                                cell.namee.text = self.filteredMessages[indexPath.row].userNamepp
                            }
                                break
                            case .failure(let error):
                                print(error)
                                DispatchQueue.main.async {
                                    self.showCustomAlert(title: "sendingoplo Failed", message: error.title!)
                            }
                        }
                    }
                        //cell.imageMessage.image = filteredMessages[indexPath.row].image
                        return cell
                }else if (filteredMessages[indexPath.row].audio != nil){
                    choosedAudio = false
                    let cell = tableView.dequeueReusableCell(withIdentifier: "AudioCell", for: indexPath) as! AudioCell
                    cell.userNameeel.text = filteredMessages[indexPath.row].userNamepp
                    cell.parentViewController = self
                    cell.indexRow = filteredMessages[indexPath.row].audio
                    cell.userNameeel.text = filteredMessages[indexPath.row].userNamepp
                    return cell
                }else if(filteredMessages[indexPath.row].video != nil){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "videocell", for: indexPath) as! VideoMessageCell
                    cell.parentViewController = self
                    cell.videoURL = filteredMessages[indexPath.row].video
                    let filename = getDirectory().appendingPathComponent("output.mp4")
                    let path = Data(base64Encoded: filteredMessages[indexPath.row].video)
                    try? path!.write(to: filename, options: .atomicWrite)
                    cell.imageThumbnail?.image = getThumbnailImage(url: filename)
                    cell.uname.text = filteredMessages[indexPath.row].userNamepp
                    return cell
                }else if(filteredMessages[indexPath.row].document != nil){
                    choosedDocument = false
                    let cell = tableView.dequeueReusableCell(withIdentifier: "documentcell", for: indexPath) as! DocumentMessageCell
                    cell.documentName.text = filteredMessages[indexPath.row].document
                    cell.extensionP = filteredMessages[indexPath.row].extensionP
                    cell.documentURL = filteredMessages[indexPath.row].document
                    cell.documentSize.text = filteredMessages[indexPath.row].size
                    cell.useraName.text = filteredMessages[indexPath.row].userNamepp
                    cell.parentViewController = self
                    return cell
                }
                else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "textcell", for: indexPath) as! TextMessageCell
                    cell.timeStamp.text = filteredMessages[indexPath.row].timeStamp
                    cell.messageLabel.text = filteredMessages[indexPath.row].message
                    cell.senderName.text = filteredMessages[indexPath.row].userNamepp
                    cell.textLabel?.numberOfLines = 0
                    return cell
                }
            }else{
                wantToShowHistory = false
                return UITableViewCell()
            }
        }else{
            if(choosedImage){
                chatMessage.text = ""
                choosedImage = false
                let cell = tableView.dequeueReusableCell(withIdentifier: "imagecell", for: indexPath) as! ImageMessageCell
                if(imageFromLibrary.image != nil || imageFromCamera != nil){
                    if(imageFromLibrary.image != nil){
                        cell.imageMessage.image = imageFromLibrary.image
                    }else{
                        cell.imageMessage.image = imageFromCamera.image
                    }
                }
                return cell
            }else if (choosedAudio){
                choosedAudio = false
                let cell = tableView.dequeueReusableCell(withIdentifier: "AudioCell", for: indexPath) as! AudioCell
                cell.parentViewController = self
                filteredMessages.append(MessagesClass(user_id: -1,id: -1,audio: audioString,timeStamp: "", userNamepp: completeinfo.data.username))
                cell.userNameeel.text = completeinfo.data.username
                cell.indexRow = audioString
                return cell
            }else if(choosedVideo){
                choosedVideo = false
                let cell = tableView.dequeueReusableCell(withIdentifier: "videocell", for: indexPath) as! VideoMessageCell
                cell.parentViewController = self
                let video = try? Data(contentsOf: videoURL!).base64EncodedString()
                filteredMessages.append(MessagesClass(user_id: -1,id: -1,video: video!, timeStamp: "", userNamepp: completeinfo.data.username))
                cell.videoURL = video!
                cell.imageThumbnail?.image = getThumbnailImage(url: videoURL!)
                cell.uname.text = completeinfo.data.username
                return cell
            }else if(choosedDocument){
                choosedDocument = false
                let cell = tableView.dequeueReusableCell(withIdentifier: "documentcell", for: indexPath) as! DocumentMessageCell
                cell.documentName.text = documentUrl
                cell.documentSize.text = String((documentSizeKB / 1024) + 1) + "KB"
                cell.useraName.text = completeinfo.data.username
                return cell
            }
            else if(!chatMessage.text.isEmpty){
                let cell = tableView.dequeueReusableCell(withIdentifier: "textcell", for: indexPath) as! TextMessageCell
                cell.timeStamp.text = DateFormatter.localizedString(from: Date(), dateStyle: .none, timeStyle: .short)
                cell.messageLabel.text = trimmed
                cell.textLabel?.numberOfLines = 0
                cell.senderName.text = completeinfo.data.username
                return cell
            }
        }
        return TableViewCell()
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if chatMessage.text == "Type your message" {
            chatMessage.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if chatMessage.text == "" {
            chatMessage.text = "Type your message"
            
        }
        
    }
    func textViewDidChange(_ textView: UITextView) {
        if !chatMessage.text.isEmpty {
        }else{
        }
        
        if chatMessage.textColor == UIColor.lightGray {
            chatMessage.text = nil
            chatMessage.textColor = UIColor.black
        }
        
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
              let currentText:String = chatMessage.text
            let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)
            if updatedText.isEmpty && updatedText.count > 0 && updatedText == "" && updatedText == "\n" {
                chatMessage.textColor = UIColor.lightGray
                voiceButton.isHidden = false
                cameraButton.isHidden = false
                chatMessage.selectedTextRange = chatMessage.textRange(from: chatMessage.beginningOfDocument, to: chatMessage.beginningOfDocument)
            } else if chatMessage.textColor == UIColor.lightGray && !text.isEmpty {
                chatMessage.textColor = UIColor.black
                chatMessage.text = "Type your message"
            }else {
                return true
            }
            return false
        }
    }
    //Delete messages
    @available(iOS 13.0, *)
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        return UIContextMenuConfiguration(identifier: indexPath as NSIndexPath, previewProvider: nil) { [self] _ in
            if(filteredMessages[indexPath.row].user_id! == completeinfo.data.id){
                return UIMenu(title: "", children: [
                        UIAction(title: "Delete", image: UIImage(systemName: "trash"), attributes: .destructive) { [self] action in
                                self.count = self.count - 1
                                self.index = self.index - 1
                                deleteMessageFromServer(indexPath: indexPath)
                                print(tableView)
                        },
                        UIAction(title: "Edit", image: UIImage(), attributes: .destructive) { [self] action in
                            if(filteredMessages[indexPath.row].audio == nil && filteredMessages[indexPath.row].document == nil && filteredMessages[indexPath.row].image == nil){
                                editMessageDialog(indexPath: indexPath,old_message: filteredMessages[indexPath.row].message)
                            }else{
                                let alertController = UIAlertController(title: "Editing", message: "Audio, Images & Documents Cannot Be Edited", preferredStyle: .alert)
                                alertController.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: nil))
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }
                ])
            }else{
                return UIMenu()
            }
        }
    }
}
