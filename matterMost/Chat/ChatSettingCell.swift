//
//  ChatSettingCell.swift
//  matterMost
//
//  Created by ethisham bader on 14/11/2020.
//

import UIKit

class ChatSettingCell: UITableViewCell {

    @IBOutlet weak var chatSettingIcon: UIImageView!
    @IBOutlet weak var chatSettingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
