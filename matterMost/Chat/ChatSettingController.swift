//
//  ChatSettingController.swift
//  matterMost
//
//  Created by ethisham bader on 14/11/2020.
//

import UIKit

class ChatSettingController: UITableViewController {
    
    var teamID:Int!
    var companyID:Int!
    var channelID:Int!
    var CompleteInfo:UserCompleteInfo!
    let settings = [
        "Channel Id",
        "Member Id",
        "Team Id",
        "Company Id",
        "Mute"
    ]
    var ID = [String]()
    var isChatType: Bool!
    var data = [String: Any]()
    var apiManager: APIManager!
    var selectedTeamID:Int!
    
    @IBOutlet weak var infoImage: UIImageView!
    @IBOutlet weak var addImage: UIImageView!
    @IBOutlet weak var muteImage: UIImageView!
    @IBOutlet weak var leaveImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiManager = APIManager()
        isChatType = true
        
        changeImageColor()
        // Do any additional setup after loading the view.
    }
    
    @objc func changeImageColor(){
        let templateImage = self.infoImage.image!.withRenderingMode(.alwaysTemplate)
        self.infoImage.image = templateImage
        self.infoImage.tintColor = .darkGray
        
        let templateImage1 = self.addImage.image!.withRenderingMode(.alwaysTemplate)
        self.addImage.image = templateImage1
        self.addImage.tintColor = .darkGray
        
        let templateImage2 = self.muteImage.image!.withRenderingMode(.alwaysTemplate)
        self.muteImage.image = templateImage2
        self.muteImage.tintColor = .darkGray
        
        let templateImage3 = self.leaveImage.image!.withRenderingMode(.alwaysTemplate)
        self.leaveImage.image = templateImage3
        self.leaveImage.tintColor = .darkGray
    }
    
    func showCustomAlert(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 0{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "viewInfoViewController") as! viewInfoViewController
            vc.teamID = teamID
            vc.companyID = companyID
            vc.channelID = channelID
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.section == 0 && indexPath.row == 1{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "GroupController") as! GroupController
            vc.userCompleteInfo = CompleteInfo
            vc.companyID = companyID
            vc.selectedTeamID = selectedTeamID
            vc.channelId = channelID
            vc.teamID = teamID
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.section == 1 && indexPath.row == 0{
            
            let alert = UIAlertController(title: "Select Any One", message: "", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Mute for 6 hours", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
                
            }))
            alert.addAction(UIAlertAction(title: "Mute for 1 year", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
                
            }))
            alert.addAction(UIAlertAction(title: "Always Mute", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
                
            }))
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
                UIAlertAction in
            }
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }else if indexPath.section == 1 && indexPath.row == 1{
            let refreshAlert = UIAlertController(title: "Sure", message: "You will be deleted from Group", preferredStyle: UIAlertController.Style.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [self] (action: UIAlertAction!) in
                let url = "https://chat1.pf.com.pk/api/channel/delete"
                let token = UserDefaults.standard.string(forKey: "token")
                let ID = UserDefaults.standard.string(forKey: "userId")
                data = [
                    "user_id": [ID!],
                    "channel_id": String(channelID),
                    "team_id": String(teamID),
                    "company_id": String(companyID)
                ]
                apiManager.apiCaller(url: url, sendBodyData: data, httpMethod: "POST", token: token!) { [self] (result) in
                    
                    switch result{
                    case .success( _):
                        DispatchQueue.main.async { [self] in
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
                            vc.completeInfo = CompleteInfo
                            vc.selectedCompanyId = companyID
                            self.navigationController?.pushViewController(vc, animated: true)
                            self.showCustomAlert(title:"Success", message: "Group Left")
                            
                        }
                        break
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self.showCustomAlert(title:"Error", message: error.title!)
                        }
                    }
                }
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                
            }))
            
            present(refreshAlert, animated: true, completion: nil)
            
        }
    }
}
