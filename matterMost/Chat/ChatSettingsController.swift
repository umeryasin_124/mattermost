//
//  ChatSettingsController.swift
//  matterMost
//
//  Created by ethisham bader on 14/11/2020.
//

import UIKit

class ChatSettingsController: UITableViewController {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        // Do any additional setup after loading the view.
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 0{
            let alert = UIAlertController(title: "Select Any One", message: "", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Mute for 6 hours", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
                
            }))
            alert.addAction(UIAlertAction(title: "Mute for 1 year", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
               
            }))
            alert.addAction(UIAlertAction(title: "Always Mute", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
               
            }))
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
                UIAlertAction in
            }
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }else if indexPath.section == 1 && indexPath.row == 0{
            let refreshAlert = UIAlertController(title: "Are you sure", message: "The user will be blocked", preferredStyle: UIAlertController.Style.alert)

            refreshAlert.addAction(UIAlertAction(title: "Block", style: .default, handler: { (action: UIAlertAction!) in
              print("Handle Ok logic here")
              }))

            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
              print("Handle Cancel Logic here")
              }))

            present(refreshAlert, animated: true, completion: nil)
        }
    }
}
