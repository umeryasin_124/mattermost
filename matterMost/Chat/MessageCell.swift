//
//  MessageCell.swift
//  TestChat
//
//  Created by ethisham bader on 03/11/2020.
//

import UIKit

class MessageCell: UITableViewCell {

    @IBOutlet weak var imageUpload: UIImageView!
    @IBOutlet weak var sendMessageLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
}
