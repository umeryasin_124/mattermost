//
//  SendingMessages.swift
//  matterMost
//
//  Created by abdullah Javed on 19/11/2020.
//

import Foundation

extension ChatScreen{
    func fetchMessages(){
        if(idType == "direct"){
            let apiino = APIManager()
            var dictto = [String:Any]()
            dictto["receiverId"] = idT
            dictto["company_id"] = company_id
            dictto["team_id"] = teamID
            let tokeno = UserDefaults.standard.string(forKey: "token")

            apiino.apiCaller(url: ENDPOINTS.fetchIndividualMessages, sendBodyData: dictto, httpMethod: "POST",token: tokeno!) { [self] (result) in
                switch result{
                    case .success(let response):
                        filteredMessages.removeAll()
                        let data = response["data"] as! [String:Any]
                        let messagesq = data["messages"] as! [[String:Any]]
                        for i in (0 ..< messagesq.count).reversed(){
                            let timee = parseTimeStamp(timeResult: messagesq[i]["created_at"] as! String)
                            var audio: String? = ""
                            var message:String? = ""
                            var userN:String? = ""
                            var video:String? = ""
                            var type:String? = ""
                            if(!(messagesq[i]["av_file"] as? String == nil)){
                                audio = messagesq[i]["av_file"] as? String
                                type = messagesq[i]["type"] as? String
                                userN = messagesq[i]["username"] as? String
                                if(type! == "audio"){
                                    userN = messagesq[i]["username"] as? String
                                    let id = messagesq[i]["user_id"] as? Int
                                    userN = messagesq[i]["username"] as? String
                                    filteredMessages.append(MessagesClass(user_id: id!, id: (messagesq[i]["id"] as? Int)!,audio: audio!, timeStamp: timee, userNamepp: userN!))
                                }else if(type! == "video"){
                                    video = messagesq[i]["av_file"] as? String
                                    let id = messagesq[i]["user_id"] as? Int
                                    userN = messagesq[i]["username"] as? String
                                    filteredMessages.append(MessagesClass(user_id: id!, id: (messagesq[i]["id"] as? Int)!,video: video!, timeStamp: timee, userNamepp: userN!))
                                }
                            }else if(!(messagesq[i]["message"] as? String == nil)){
                                message = messagesq[i]["message"] as? String
                                userN = messagesq[i]["username"] as? String
                                let id = messagesq[i]["user_id"] as? Int
                                filteredMessages.append(MessagesClass(user_id: id!, id: (messagesq[i]["id"] as? Int)!,message: message!, timeStamp: timee,userNamepp: userN!))
                            }else{
                                let data = messagesq[i]["attachments"] as? [[String:Any]]
                                if(data!.count > 0){
                                    let id = messagesq[i]["user_id"] as? Int
                                    let path = data![0]["path"] as! String
                                    userN = messagesq[i]["username"] as? String
                                    if(data![0]["extension"] as! String == "png" || data![0]["extension"] as! String == "jpg"
                                        || data![0]["extension"] as! String == "jpeg" || data![0]["extension"] as! String == "svg"){
                                        userN = messagesq[i]["username"] as? String
                                        filteredMessages.append(MessagesClass(user_id: id!, id: (messagesq[i]["id"] as? Int)!,image: path, timeStamp: timee, userNamepp: userN!))
                                    }else{
                                        userN = messagesq[i]["username"] as? String
                                        filteredMessages.append(MessagesClass(user_id: id!, id: (messagesq[i]["id"] as? Int)!,document: path,extensionP: data![0]["extension"] as! String,size: data![0]["size"] as! String, userNamepp: userN!))
                                    }
                                }
                            }
                        }
                        if(filteredMessages.count > 0){
                            wantToShowHistory = true
                            count = filteredMessages.count
                            sendingMessage = false
                            index = count
                            DispatchQueue.main.async {
                                tableView.reloadData()
                                tableView.scrollToRow(at: IndexPath(row: count-1 , section: 0), at: .top, animated: true)

                            }
                        }
                       
                        break
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self.showCustomAlert(title: "No Chat History", message: error.title!)
                    }
                }
            }

        }else if (idType == "channel"){
            
            let apiinsdq = APIManager()
            var dictctq = [String:Any]()
            dictctq["channel_id"] = idT
            dictctq["company_id"] = company_id
            dictctq["team_id"] = teamID
            let tokenq = UserDefaults.standard.string(forKey: "token")
            apiinsdq.apiCaller(url: ENDPOINTS.fetchChannelMessages, sendBodyData: dictctq, httpMethod: "POST",token: tokenq!) { [self] (result) in
                switch result{
                    case .success(let response):
                        filteredMessages.removeAll()
                        let data = response["data"] as! [String:Any]
                        let messagesq = data["messages"] as! [[String:Any]]
                        for i in (0 ..< messagesq.count).reversed(){
                            let timee = parseTimeStamp(timeResult: messagesq[i]["created_at"] as! String)
                            var audio: String? = ""
                            var message:String? = ""
                            var userN:String? = ""
                            var video:String? = ""
                            var type:String? = ""
                            if(!(messagesq[i]["av_file"] as? String == nil)){
                                audio = messagesq[i]["av_file"] as? String
                                type = messagesq[i]["type"] as? String
                                userN = messagesq[i]["username"] as? String
                                if(type! == "audio"){
                                    userN = messagesq[i]["username"] as? String
                                    let id = messagesq[i]["user_id"] as? Int
                                    userN = messagesq[i]["username"] as? String
                                    filteredMessages.append(MessagesClass(user_id: id!, id: (messagesq[i]["id"] as? Int)!,audio: audio!, timeStamp: timee, userNamepp: userN!))
                                }else if(type! == "video"){
                                    video = messagesq[i]["av_file"] as? String
                                    let id = messagesq[i]["user_id"] as? Int
                                    userN = messagesq[i]["username"] as? String
                                    filteredMessages.append(MessagesClass(user_id: id!, id: (messagesq[i]["id"] as? Int)!,video: video!, timeStamp: timee, userNamepp: userN!))
                                }
                            }else if(!(messagesq[i]["message"] as? String == nil)){
                                message = messagesq[i]["message"] as? String
                                userN = messagesq[i]["username"] as? String
                                let id = messagesq[i]["user_id"] as? Int
                                filteredMessages.append(MessagesClass(user_id: id!, id: (messagesq[i]["id"] as? Int)!,message: message!, timeStamp: timee,userNamepp: userN!))
                            }else{
                                let data = messagesq[i]["attachments"] as? [[String:Any]]
                                if(data!.count > 0){
                                    let id = messagesq[i]["user_id"] as? Int
                                    let path = data![0]["path"] as! String
                                    userN = messagesq[i]["username"] as? String
                                    if(data![0]["extension"] as! String == "png" || data![0]["extension"] as! String == "jpg"
                                        || data![0]["extension"] as! String == "jpeg" || data![0]["extension"] as! String == "svg"){
                                        userN = messagesq[i]["username"] as? String
                                        filteredMessages.append(MessagesClass(user_id: id!, id: (messagesq[i]["id"] as? Int)!,image: path, timeStamp: timee, userNamepp: userN!))
                                    }else{
                                        userN = messagesq[i]["username"] as? String
                                        filteredMessages.append(MessagesClass(user_id: id!, id: (messagesq[i]["id"] as? Int)!,document: path,extensionP: data![0]["extension"] as! String,size: data![0]["size"] as! String, userNamepp: userN!))
                                    }
                                }
                            }
                        }
                        if(filteredMessages.count > 0){
                            wantToShowHistory = true
                            count = filteredMessages.count
                            sendingMessage = false
                            index = count
                            DispatchQueue.main.async {
                                tableView.reloadData()
                                tableView.scrollToRow(at: IndexPath(row: count-1 , section: 0), at: .top, animated: true)

                            }
                        }
                       
                       break
                    case .failure(let error):
                        print(error.code)
                        DispatchQueue.main.async {
                            self.showCustomAlert(title: "Fetching Message Failed", message: error.title!)
                    }
                }
            }
        }
    }
    func sendMessage(){
            var dictt = [String:Any]()
        if(idType == "direct"){
            dictt["receiver_id"] = idT
        }else{
            dictt["channel_id"] = idT
        }
            dictt["company_id"] = company_id
            dictt["team_id"] = teamID
        if(choosedImage){
            dictt["message"] = ""
            let token = UserDefaults.standard.string(forKey: "token")
            if(!(imageP == nil)){
                APIManager().uploadFileToServer(dict: dictt, url: ENDPOINTS.sendMessage, imageData: imageP.pngData()!, httpMethod: "POST", token: token!, filePathKey: "files", extensionT: ".png") { (result) in
                    switch result{
                    case .success(let response):
                            print(response)
                            break
                        case .failure(let error):
                            print(error)
                            DispatchQueue.main.async {
                                self.showCustomAlert(title: "sendingoplo Failed", message: error.title!)
                        }
                    }
                }
            }else if(!(imageC == nil)){
                APIManager().uploadFileToServer(dict: dictt, url: ENDPOINTS.sendMessage, imageData: imageC.pngData()!, httpMethod: "POST", token: token!, filePathKey: "files", extensionT: ".png") { (result) in
                    switch result{
                    case .success(let response):
                            print(response)
                            break
                        case .failure(let error):
                            print(error)
                            DispatchQueue.main.async {
                                self.showCustomAlert(title: "No Chat History Found", message: error.title!)
                        }
                    }
                }
            }
            
        }else if(choosedVideo){
            dictt["video"] = videoString!
            callApi(dictt: dictt)
        }else if(choosedAudio){
            do{
                dictt["audio"] = audioString!
                callApi(dictt: dictt)

            }
        }else if(choosedDocument){
            let token = UserDefaults.standard.string(forKey: "token")
            APIManager().uploadFileToServer(dict: dictt, url: ENDPOINTS.sendMessage, imageData: documentPath!, httpMethod: "POST", token: token!, filePathKey: "files", extensionT: "." + extensionP!) { (result) in
                switch result{
                case .success(let response):
                        print(response)
                        break
                    case .failure(let error):
                        print(error)
                        DispatchQueue.main.async {
                            self.showCustomAlert(title: "sendingoplo Failed", message: error.title!)
                    }
                }
            }
        }else{
           // dictt["message"] = chatMessage.text
            dictt["message"] = trimmed
            callApi(dictt: dictt)
        }
    }

}
