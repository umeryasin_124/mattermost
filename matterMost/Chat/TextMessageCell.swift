//
//  TextMessageCell.swift
//  matterMost
//
//  Created by ethisham bader on 12/11/2020.
//

import UIKit

class TextMessageCell: UITableViewCell {
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var imageSender: UIImageView!
    @IBOutlet weak var timeStamp: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //timeStamp.text = "0900"


    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
