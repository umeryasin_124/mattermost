import UIKit

class ConfirmEmail: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailErrorField: UILabel!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var iconBtn: UIButton!
    @IBOutlet weak var activityLoaderr: UIActivityIndicatorView!
    
    var emailDic = [String: String]() //initialising dictonary
    var apiManager: APIManager! //Api Manager Class initialised
    
    let emailImage = UIImage(named: "email")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityLoaderr.isHidden = true
        apiManager = APIManager()
        self.navigationController?.navigationBar.isHidden = false
        emailField.addLine(position: .LINE_POSITION_BOTTOM, color: .gray, width: 1)  //Adding line under EmailField
        emailField.delegate = self
        
        let tintedImage = emailImage?.withRenderingMode(.alwaysTemplate)
        iconBtn.setImage(tintedImage, for: .normal)
        iconBtn.tintColor = .darkGray
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        activityLoaderr.isHidden = true
        continueBtn.isUserInteractionEnabled = true
        
    }

    //Sending Valid Email to server to change Password
    @IBAction func confirmEmail(_ sender: Any) {
        continueBtn.isUserInteractionEnabled = false
        if(emailTextField.text!.isEmpty){
            emailErrorField.text = "Email Field is Empty"
            emailErrorField.isHidden = false
            continueBtn.isUserInteractionEnabled = true
        }
        else{
            if(Validator.isValidEmail(emailTextField.text!)){
                if ConnectivityService().isConnectedToNetwork(){
                    activityLoaderr.isHidden = false
                    activityLoaderr.startAnimating()
                    apiCall() //Api Call Here
                }else{
                    activityLoaderr.stopAnimating()
                    activityLoaderr.isHidden = true
                    showCustomAlert(title: "Internet Not Connected", message: "Connection Error")
                }
                emailErrorField.isHidden = true
            }else{
                emailErrorField.text! = "Email Not Valid*"
                emailErrorField.isHidden = false
                continueBtn.isUserInteractionEnabled = true
            }
        }
    }
    //Confirm Email Api Method
    func apiCall(){
        emailDic["email"] = emailTextField.text //Setting Data to Dictonary
        apiManager.apiCaller(url: ENDPOINTS.forgetPassword, sendBodyData: emailDic, httpMethod: "POST") { [self] (result)  in
            switch result{
            case .success( _):
                DispatchQueue.main.async {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                    vc.email = emailTextField.text
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    activityLoaderr.stopAnimating()
                    activityLoaderr.isHidden = true
                    showCustomAlert(title: "Email Failed", message: error.title!)
                    continueBtn.isUserInteractionEnabled = true
                }
            }
        }
    }
    //Custom Alert Dialog Box
    func showCustomAlert(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}

