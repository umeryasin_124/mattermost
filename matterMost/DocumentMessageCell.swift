//
//  DocumentMessageCell.swift
//  matterMost
//
//  Created by ethisham bader on 13/11/2020.
//

import UIKit

class DocumentMessageCell: UITableViewCell {

    @IBOutlet weak var documentName: UILabel!
    @IBOutlet weak var documentSize: UILabel!
    @IBOutlet weak var documentDownload: UIButton!
    @IBOutlet weak var documentTime: UILabel!
    @IBOutlet weak var documentView: UIView!
    
    @IBOutlet weak var docImage: UIImageView!
    @IBOutlet weak var useraName: UILabel!
    
    let downloadImage = UIImage(named: "download Document")
    var documentURL:String!
    var extensionP:String!
    var parentViewController:UIViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        documentView.layer.borderColor = UIColor.lightGray.cgColor
        documentView.layer.borderWidth = 1.0
        
        let templateImage = self.docImage.image!.withRenderingMode(.alwaysTemplate)
        self.docImage.image = templateImage
        self.docImage.tintColor = .darkGray
        
        let tintedImage = downloadImage?.withRenderingMode(.alwaysTemplate)
        documentDownload.setImage(tintedImage, for: .normal)
        documentDownload.tintColor = .darkGray

        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onCLickDownloadButton(_ sender: Any) {
        
        let token = UserDefaults.standard.string(forKey: "token")
        let filename = getDirectory().appendingPathComponent("\(NSDate())" + "." + extensionP)
        APIManager().downloadFileToServerViaURL(url: documentURL, httpMethod: "GET", token: token!) { [self] (result) in
            switch result{
            case .success(let response):
                        do {
                            try FileManager.default.copyItem(at: response, to: filename)
                            do {
                                //Show UIActivityViewController to save the downloaded file
                                //try FileManager.default.createDirectory(at: filename, withIntermediateDirectories: true, attributes: nil)
                                let contents  = try FileManager.default.contentsOfDirectory(at: getDirectory(), includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                                for indexx in 0..<contents.count {
                                    if contents[indexx].lastPathComponent == filename.lastPathComponent {
                                        let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                                        DispatchQueue.main.async {
                                            parentViewController.present(activityViewController, animated: true, completion: nil)
                                        }
                                    }
                                }
                            }
                            catch (let err) {
                                print("error: \(err)")
                            }
                        } catch (let writeError) {
                            print("Error creating a file (destinationFileUrl) : \(writeError)")
                        }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    func getDirectory() -> URL {
        
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = path[0]
        print(documentDirectory)
        return documentDirectory
    }
}
extension UIViewController {

func showToast(message : String, font: UIFont) {

    let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 200, height: 35))
    toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    toastLabel.textColor = UIColor.white
    toastLabel.font = font
    toastLabel.textAlignment = .center;
    toastLabel.text = message
    toastLabel.alpha = 1.0
    toastLabel.layer.cornerRadius = 10;
    toastLabel.clipsToBounds  =  true
    self.view.addSubview(toastLabel)
    UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
         toastLabel.alpha = 0.0
    }, completion: {(isCompleted) in
        toastLabel.removeFromSuperview()
    })
} }
