//
//  EditGroupViewController.swift
//  matterMost
//
//  Created by mujtaba Hassan on 10/11/2020.
//

import UIKit

class EditGroupViewController: UIViewController {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var editGroupName: UITextField!
    
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    
    var checkGroupName: String? = ""
    
    var groupNameDic = [String: String]()
    
    var apiManager: APIManager!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        apiManager = APIManager()
        
        editGroupName.addLine(position: .LINE_POSITION_BOTTOM, color: .lightGray, width: 1)
        
        doneBtn.isEnabled = false
        checkGroupName = editGroupName.text
    
    }
    @IBAction func onClickDone(_ sender: Any) {
        doneBtn.isEnabled = false
        if(checkGroupName == editGroupName.text){
            
            showCustomAlert(title: "Group Name not changed", message: "Previous Group Name is not changed Please Check")
            doneBtn.isEnabled = true
        }
        else{
            if(editGroupName.text!.isEmpty){
                showCustomAlert(title: "Error", message: "Group Name field is Empty")
                doneBtn.isEnabled = true
            } else{
                if ConnectivityService().isConnectedToNetwork(){
                    //getiing token
                    let userToken = UserDefaults.standard.string(forKey: "token")
                    
                    //addding to grouo name to dictonary
                    groupNameDic["name"] = editGroupName.text
                    
                    // calling api
                    apiManager.apiCaller(url: ENDPOINTS.updateChannel, sendBodyData: groupNameDic, httpMethod: "POST", token: userToken!) { [self] (result) in
                        switch result {
                        case .success( _):
                            DispatchQueue.main.async {
//                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                                let vc = storyboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
//                                vc.email = emailTextField.text
//                                self.navigationController?.pushViewController(vc, animated: false)
                                showCustomAlert(title: "Done", message: "Succusfully Group Name Changed")
                                
                            }
                            break
                        case .failure(let error):
                            DispatchQueue.main.async {
                               // activityLoaderr.stopAnimating()
                               // activityLoaderr.isHidden = true
                                showCustomAlert(title: "Group Name Change Failed", message: error.title!)
                                doneBtn.isEnabled = true
                            }
                        
                        }
                    }
                } else{
                    showCustomAlert(title: "Internet Connection Error", message: "Internet Not Connected Please Connect")
                }
            }
        }
        
    }
  
    @IBAction func onClickEditGroupName(_ sender: Any) {
        
        doneBtn.isEnabled = true
    }
    
    @IBAction func editBtn(_ sender: Any) {
        
        let alert = UIAlertController(title: "Select Any One", message: "", preferredStyle: .actionSheet)
               let camera = UIAlertAction(title: "Take Photo", style: .default) {
                   UIAlertAction in
                self.phoneCamera()
               }
               alert.addAction(camera)
        
        alert.addAction(UIAlertAction(title: "Open Library", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
                    self.photoLibrary()
                }))
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) {
            UIAlertAction in
        }
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil)
               
    }
    
    func photoLibrary()
    {

        let vc = UIImagePickerController()
        vc.sourceType = UIImagePickerController.SourceType.photoLibrary
        vc.delegate = self
        vc.allowsEditing = true
        self.present(vc, animated: true)

    }
    
    func phoneCamera()
    {

        let vc1 = UIImagePickerController()
        if( UIImagePickerController.isSourceTypeAvailable(.camera)){
            vc1.sourceType = .camera
            vc1.allowsEditing = true
            vc1.delegate = self
            self.present(vc1, animated: true)
        }else{
            showCustomAlert(title: "Camera Error", message: "Camera not available. Please check")
        }
        

    }
    
    func showCustomAlert(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    

   
}

extension EditGroupViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")]as? UIImage{
            profileImage.image = image
        }
    
        
       // print("\(info)")
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
   
    

}

