import UIKit
class GeneralSViewController: UIViewController {
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    @IBOutlet weak var imageViews: UIImageView!
    @IBOutlet weak var usernameText: UITextField!

    @IBOutlet weak var editBtn: UIButton!
    
    @IBOutlet weak var checkUserNameLbl: UILabel!
    var usernameDic = [String:String]()
    var profileImageDic = [String:Data]()
    
    var apiManager: APIManager!
    var isProfilePic: Bool!

    var checkUsername: String!
    var checkImage: Data!
    var encodedImage: Data!

    let editImage = UIImage(named: "edit")
    
    override func viewDidLoad() {
        apiManager = APIManager()
        super.viewDidLoad()
        isProfilePic = false
        self.doneBtn.isEnabled = false

        checkImage = encodedImage
        let  profileUrl = UserDefaults.standard.data(forKey: "userProfilePic")
        let  username = UserDefaults.standard.string(forKey: "userName")
        usernameText.text = username

        if !(profileUrl == nil){
            self.imageViews.image = UIImage(data: profileUrl!)
        }
        
        let tintedImage = editImage?.withRenderingMode(.alwaysTemplate)
        editBtn.setImage(tintedImage, for: .normal)
        editBtn.tintColor = .darkGray
    }
    override func viewDidAppear(_ animated: Bool) {
        checkUsername = usernameText.text
        checkUserNameLbl.isHidden = true
    }
    
    @IBAction func userNameTxt(_ sender: Any) {
        doneBtn.isEnabled = true
    }
    @IBAction func onclickDone(_ sender: Any) {
        self.doneBtn.isEnabled = false

        if ConnectivityService().isConnectedToNetwork(){
            if !(usernameText.text == checkUsername) {
                updateUsernameAPI()
                checkUserNameLbl.isHidden = true
            }
            else{
                checkUserNameLbl.text = "Username not changed"
                checkUserNameLbl.isHidden = false
                doneBtn.isEnabled = true
            }
            
            if(isProfilePic){
                updateProfilePictureAPI()
                isProfilePic = false
                checkUserNameLbl.isHidden = true
                doneBtn.isEnabled = false
            }
        }
        else{
            showCustomAlert(title: "Internet Not Connected", message: "Connection Error")
            self.doneBtn.isEnabled = true
        }

    }
    
    func updateUsernameAPI(){
        
        usernameDic["username"] = usernameText.text
        let userToken = UserDefaults.standard.string(forKey: "token")

        //Api Call Here
        apiManager.apiCaller(url: ENDPOINTS.updateUserName, sendBodyData: usernameDic, httpMethod: "POST", token:userToken!) { [self] (result)  in

            switch result{
            case .success(let response):
                DispatchQueue.main.async {
                    showCustomAlert(title: "Updated", message: response["status_message"] as! String)
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    showCustomAlert(title: "Profile Image Updation Failed", message: error.title!)
                    doneBtn.isEnabled = true
                }
            }
        }
    }
    
    
    func updateProfilePictureAPI(){
        profileImageDic["user_profile"] = checkImage
        let userToken = UserDefaults.standard.string(forKey: "token")

        apiManager.uploadFileToServer(url: ENDPOINTS.updateUserProfile, imageData: encodedImage, httpMethod: "POST", token: userToken!) { [self] (result) in
            switch result{
            case .success(let response):
                DispatchQueue.main.async {
                    showCustomAlert(title: "Successfully Profile Image Updated", message: response["status_message"] as! String)
                    self.doneBtn.isEnabled = true
                    self.doneBtn.isEnabled = true
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    showCustomAlert(title: "Error", message: error.title!)
                    self.doneBtn.isEnabled = false
                }
            }
        }
    }
    
    
    func showCustomAlert(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }

    @IBAction func onCLickAttachment(_ sender: Any) {
        
        self.doneBtn.isEnabled = true
        let actionsheet = UIAlertController(title: "Action Needed", message: "Choose your best option to upload profile image", preferredStyle: .actionSheet)

        let libButton = UIAlertAction(title: "Select Image From Library", style: .default) { (libSelected) in

            let imagepicker = UIImagePickerController()
            imagepicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            imagepicker.allowsEditing = true
            imagepicker.delegate = self
            self.present(imagepicker, animated: true, completion: nil)
        }
        let CameraButton = UIAlertAction(title: "Capture Image From Camera", style: .default) { (camSelected) in
            if( UIImagePickerController.isSourceTypeAvailable(.camera)){
                let imagepicker = UIImagePickerController()
                imagepicker.sourceType = UIImagePickerController.SourceType.camera
                imagepicker.allowsEditing = true
                imagepicker.delegate = self
                self.present(imagepicker, animated: true, completion: nil)
            }else{
                let alert = UIAlertController(title: "UnExpected Behaviour", message: "Camera Not Avaliable", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok",
                                              style: UIAlertAction.Style.default,
                                              handler: {(_: UIAlertAction!) in

                                              }))
                self.present(alert, animated: true, completion: nil)
            }
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel) {  (cancelSeleted) in
        }
        actionsheet.addAction(libButton)
        actionsheet.addAction(CameraButton)
        actionsheet.addAction(cancelButton)
        present(actionsheet, animated: true, completion: nil)
    }
}

extension GeneralSViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        if let image_data = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as? UIImage {
            let imageData:Data = image_data.jpegData(compressionQuality: 0.1)!
            if let finalImage = UIImage(data: imageData){
                self.imageViews.image = finalImage
                encodedImage = imageData
                isProfilePic = true
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
}


