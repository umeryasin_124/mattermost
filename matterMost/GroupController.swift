//
//  GroupController.swift
//  matterMost
//
//  Created by Umer yasin on 06/11/2020.
//
import UIKit

class GroupController: UIViewController, ChangeImage {
    
    func imageChange() {}
    func cellClicked(index: Int, isChecked: Bool) {isSelected[index] = isChecked}
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var checkImage: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var nextBtn: UIBarButtonItem!
    
    
    var apiManager: APIManager!
    var data = [String: Any]()
    var id : Int!
    
    var userCompleteInfo: UserCompleteInfo!
    var groupName: String!
    var groupType: String!
    var groupDis: String!
    var channelId: Int!
    var teamID:Int!
    var companyID:Int!
    
    var selectedUserIds =  [Int]()
    var isSelected = [Bool]()
    var chkImage = false
    var count : Int = 0
    var selectedCompanyId:Int!
    var selectedTeamID:Int!
    var ids = [Int]()
    
    var searchMember = [String]()
    var searching = false
    
    var userList = [String]()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView(frame: .zero)
        apiManager = APIManager()
        
        setTableView()
        
        let users = getTeamUsers()
        
        for i in 0 ..< users.count{
            if(!(users[i].username == nil)){
                ids.append(users[i].id)
                userList.append(users[i].username!)
            }
        }
        
        isSelected = [Bool](repeating: false, count: ids.count)
    }
    
    
    
    
    func isSelectedCount() -> Int{
        var count = 0
        for i in 0 ..< isSelected.count{
            if isSelected[i]{
                count = count + 1
            }
        }
        return count
    }
    
    
    
    
    
    
    @IBAction func addMemberNext(_ sender: Any) {
        
        for i in 0 ..< isSelected.count{
            if isSelected[i] { selectedUserIds.append(ids[i])}
        }
        
        if isSelectedCount() == 0{
            showCustomAlert(title: "Failure", message: "Please Add a member ")
        }else{
            nextBtn.isEnabled = false
            createGroupAPICall()}
    }
    
    
    
    func getTeamUsers() -> [Users]{
        for i in 0 ..< userCompleteInfo.data.companies.count{
            if userCompleteInfo.data.companies[i].id == companyID!{
                if(userCompleteInfo.data.companies[i].teams.count > 0){
                    teamID = userCompleteInfo.data.companies[i].teams[selectedTeamID].id
                    return userCompleteInfo.data.companies[i].teams[selectedTeamID].users
                }
            }
        }
        return [Users]()
    }
    
    
    func addUsersApiCall(){
        
        let url = ENDPOINTS.insertUserInChannel
        let token = UserDefaults.standard.string(forKey: "token")
        
        data ["user_id"] = selectedUserIds
        data ["team_id"] = String(teamID)
        data ["company_id"] = String(companyID)
        data ["channel_id"] = String(channelId)
        
        apiManager.apiCaller(url: url, sendBodyData: data, httpMethod: "POST", token: token!) { (result) in
            
            switch result{
            case .success(let response):
                DispatchQueue.main.async { [self] in
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "HomeScreen") as! HomeScreen
                    vc.completeInfo = userCompleteInfo
                    vc.selectedCompanyId = companyID
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    self.showCustomAlert(title:"Sucessful", message: response["status_message"] as! String)
                    
                    nextBtn.isEnabled = true
                };break
                
            case .failure(let error):
                DispatchQueue.main.async { [self] in
                    self.showCustomAlert(title:"Error in Adding User", message: error.title!)
                    nextBtn.isEnabled = true
                }
            }
        }
    }
    
    
    func createGroupAPICall(){
        
        let url = ENDPOINTS.createChannel
        let token = UserDefaults.standard.string(forKey: "token")
        
        data ["name"] = groupName
        data ["type"] = groupType
        data ["team_id"] = String(teamID)
        data ["company_id"] = String(companyID)
        data ["description"] = groupDis
        
        apiManager.apiCaller(url: url, sendBodyData: data, httpMethod: "POST", token: token!) { (result) in
            switch result{
            case .success(let response):
                DispatchQueue.main.async { [self] in
                    
                   // let dataU =  response["data"] as! [String:Any]
                    channelId = response["id"] as? Int
                    addUsersApiCall()
                }
                break
                
            case .failure(let error):
                DispatchQueue.main.async {
                    self.showCustomAlert(title:"Error", message: error.title!)
                }
            }
        }
    }
    
    
    
    private func setTableView(){
        let nib = UINib(nibName: "AddUsersInGroup", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "cellIdentifier")
    }
    
    
    @IBAction func checkButtonImage(_ sender: Any) {
        if(chkImage){
            chkImage = false
            checkImage.setImage(UIImage(named : "unChecked"), for: .normal)
            tableView.reloadData()
        }
        else{
            chkImage = true
            checkImage.setImage(UIImage(named : "checked"), for: .normal)
            tableView.reloadData()
        }
    }
}




extension GroupController : UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searching{return searchMember.count}
        else{return userList.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath) as! AddUsersInGroup
        
        if (searching){
            cell.labelTxt.text = searchMember[indexPath.row]
        }else{
            cell.labelTxt.text = userList[indexPath.row]}
        
        
        cell.delegate = self
        cell.index = indexPath.row
        
        
        if(chkImage){
            isSelected[indexPath.row] = true
            cell.btnImg.setImage(UIImage(named : "checked"), for: .normal)
            cell.btnImage = false
        }else{
            isSelected[indexPath.row] = false
            
            cell.btnImg.setImage(UIImage(named : "unChecked"), for: .normal)
            cell.btnImage = true
        }
        
        return cell
        
    }
}



extension GroupController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.frame.height / 13
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! AddUsersInGroup
        id = ids[indexPath.row]
        
        if(cell.btnImage){
            count = count + 1
            isSelected[indexPath.row] = true
            cell.btnImage = false
            cell.btnImg.setImage(UIImage(named : "checked"), for: .normal)
            
        }else{
            isSelected[indexPath.row] = false
            cell.btnImage = true
            cell.btnImg.setImage(UIImage(named : "unChecked"), for: .normal)}
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    }
    
}



extension GroupController : UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchMember = userList.filter({$0.lowercased().prefix(searchText.count) == searchText.lowercased()} )
        searching = true
        tableView.reloadData()}
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBar.text = ""
        tableView.reloadData()}
    
    func showCustomAlert(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)}
}

