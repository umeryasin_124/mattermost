//
//  GroupDetailViewController.swift
//  ForgetPassword
//
//  Created by Aamna Mohsin on 04/11/2020.
//

import UIKit
import TPKeyboardAvoiding
import AVFoundation
import IHKeyboardAvoiding

class GroupDetailViewController: UIViewController {
    
    private var isPrivate: Bool = false
    private var isPublic: Bool = true
    
    @IBOutlet weak var groupName: UITextField!
    @IBOutlet weak var descripionText: UITextView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var publicBoolImage: UIImageView!
    @IBOutlet weak var privateBoolImage:  UIImageView!
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    
    var data = [String: String]()
    var type: String! = "public"
    var completeInfo: UserCompleteInfo!
    var apiManager: APIManager!
    var teamID:Int!
    var selectedTeamIndex:Int!
    var companyID:Int!
    var channelID:Int!
    
    @IBAction func Done(_ sender: Any) {
        if ConnectivityService().isConnectedToNetwork() {
            
            if (groupName.text != ""){
                doneBtn.isEnabled = false
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "GroupController") as! GroupController
                vc.selectedTeamID = selectedTeamIndex
                vc.companyID = companyID
                vc.groupName = groupName.text
                vc.groupType = type
                vc.groupDis = descripionText.text
                vc.userCompleteInfo = completeInfo
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            showCustomAlert(title: "", message: "No Internet Connection")}
        
    }
    
    
    
    @IBAction func isPublicGroup(_ sender: Any) {
        if isPublic{
            isPublic = false
            isPrivate = true
            type = "public"
            publicBoolImage.image = UIImage(named: "checked")
            privateBoolImage.image = UIImage(named: "unChecked")
            
        }
    }
    
    
    @IBAction func isPrivateGroup(_ sender: Any) {
        isPublic = true
        isPrivate = false
        type = "private"
        publicBoolImage.image = UIImage(named: "unChecked")
        privateBoolImage.image = UIImage(named: "checked")
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiManager = APIManager()
        self.hideKeyboardWhenTappedAround()
        descripionText.delegate = self
        descripionText.text = "Type group description..."
        descripionText.textColor = UIColor.lightGray
        KeyboardAvoiding.avoidingView = self.view
    }
    
    override func viewDidAppear(_ animated: Bool) {
        doneBtn.isEnabled = true
    }
    
    
    @IBAction func editBtn(_ sender: Any) {
        
        let alert = UIAlertController(title: "Upload Picture", message: "", preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Take Photo", style: .default) { [self]
            UIAlertAction in
            
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
            switch cameraAuthorizationStatus {
            case .notDetermined: requestCameraPermission()
            case .authorized: presentCamera()
            case .restricted, .denied: alertCameraAccessNeeded()
            }
            
        }
        alert.addAction(camera)
        alert.addAction(UIAlertAction(title: "Open Library", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) {
            UIAlertAction in
        }
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func photoLibrary(){
        
        let vc = UIImagePickerController()
        vc.sourceType = UIImagePickerController.SourceType.photoLibrary
        vc.delegate = self;
        vc.allowsEditing = true
        self.present(vc, animated: true)
    }
    func phoneCamera(){
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .camera
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self;
        self.present(imagePickerController, animated: true)
    }
    
    func  requestCameraPermission(){
        AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
                                        guard accessGranted == true else { return }
                                        self.presentCamera()})
    }
    func presentCamera() {
        let photoPicker = UIImagePickerController()
        photoPicker.sourceType = .camera
        photoPicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        self.present(photoPicker, animated: true, completion: nil)
    }
    
    
    
    func alertCameraAccessNeeded() {
        let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
        
        let alert = UIAlertController(
            title: "Need Camera Access",
            message: "Camera access is required to make full use of this app.",
            preferredStyle: UIAlertController.Style.alert
        )
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    
    func promptAlert(errorMessage: String) {
        let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel) { (alert) -> Void in
            UIApplication.shared.canOpenURL(URL(string: UIApplication.openSettingsURLString)!)
        })
        
        present(alert, animated: true)
    }
    //    func apiCall(){
    //        let url = ENDPOINTS.createChannel
    //        let token = UserDefaults.standard.string(forKey: "token")
    //        let name: String = groupName.text!
    //        data = [
    //            "name": name,
    //            "type": type,
    //            "team_id": String(teamID),
    //            "company_id": String(companyID),
    //            "description": descripionText.text
    //        ]
    //        apiManager.apiCaller(url: url, sendBodyData: data, httpMethod: "POST", token: token!) { (result) in
    //
    //            switch result{
    //            case .success(let response):
    //                DispatchQueue.main.async { [self] in
    //
    //                    self.doneBtn.isEnabled = true
    //                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    //                    let vc = storyboard.instantiateViewController(withIdentifier: "GroupController") as! GroupController
    //                    let dataU =  response["data"] as! [String:Any]
    //                    vc.channelId = dataU["id"] as? Int
    //                    vc.selectedTeamID = selectedTeamIndex
    //                    vc.companyID = companyID
    //                    vc.userCompleteInfo = completeInfo
    //                    self.navigationController?.pushViewController(vc, animated: true)
    //                }
    //                break
    //            case .failure(let error):
    //                DispatchQueue.main.async {
    //                    self.doneBtn.isEnabled = true
    //                    self.showCustomAlert(title:"Error", message: error.title!)
    //                }
    //            }
    //        }
    //    }
    func showCustomAlert(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}
extension GroupDetailViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")]as? UIImage{
            profileImage.image = image
        }
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    
}
extension GroupDetailViewController:  UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if descripionText.textColor == UIColor.lightGray {
            descripionText.text = ""
            if #available(iOS 13.0, *) {
                descripionText.textColor = .secondaryLabel
            } else {
                descripionText.textColor = .black
            }
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if descripionText.text == "" {
            descripionText.text = "Type group description..."
            descripionText.textColor = UIColor.lightGray
        }
    }
    
    
    
}
