//
//  DashboardGestures.swift
//  matterMost
//
//  Created by abdullah Javed on 19/11/2020.
//

import Foundation
import UIKit

extension HomeScreen{
    @objc func rightSwipeGesture() {
        if(firstTab){
            isMenuOpen = true
            openMenu()
        }else if(secondTab){
            type = "channel"
            usersString.removeAll()
            ids.removeAll()
            self.tabBar.selectedItem = tabBar.items![0]
            firstTab  = true
            secondTab = false
            thirdTab = false
            addIcon.image = UIImage(named: "add")
            let channels = getPublicChannels()
            for i in 0 ..< channels.count{
                ids.append(channels[i].id)
                usersString.append(channels[i].name)
            }
            realData = usersString
            tableView.reloadData()
        }else if(thirdTab){
            type = "channel"
            usersString.removeAll()
            ids.removeAll()
            self.tabBar.selectedItem = tabBar.items![1]
            firstTab  = false
            secondTab = true
            thirdTab = false
            addIcon.image = UIImage(named: "add")
            let channels = getPrivateChannels()
            for i in 0 ..< channels.count{
                ids.append(channels[i].id)
                usersString.append(channels[i].name)
            }
            realData = usersString
            tableView.reloadData()
        }
    }
    @objc func leftSwipeGesture() {
        if(isMenuOpen){
            isMenuOpen = false
            closeMenu()
        }else if(firstTab){
            type = "channel"
            usersString.removeAll()
            ids.removeAll()
            self.tabBar.selectedItem = tabBar.items![1]
            secondTab = true
            firstTab = false
            thirdTab = false
            let channels = getPrivateChannels()
            for i in 0 ..< channels.count{
                ids.append(channels[i].id)
                usersString.append(channels[i].name)
            }
            realData = usersString
            addIcon.image = UIImage(named: "add")
            tableView.reloadData()
        }else if(secondTab){
            type = "direct"
            usersString.removeAll()
            ids.removeAll()
            self.tabBar.selectedItem = tabBar.items![2]
            thirdTab = true
            firstTab = false
            secondTab = false
            addIcon.image = UIImage(named: "chatBubble")
            let direct = getUsersWithDirectChat()
            for i in 0 ..< direct.count{
                if(direct[i].chat){
                    if(!(direct[i].username == nil)){
                        ids.append(direct[i].id)
                        usersString.append(direct[i].username!)
                    }
                }
            }
            realData = usersString
            tableView.reloadData()
        }
    }
}
