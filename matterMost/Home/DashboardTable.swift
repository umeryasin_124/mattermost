//
//  DashboardTable.swift
//  matterMost
//
//  Created by abdullah Javed on 19/11/2020.
//

import Foundation
import UIKit

extension HomeScreen : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersString.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "usercell", for: indexPath) as! UserCell
        cell.parentViewController = self
        cell.usernameLabel.text = usersString[indexPath.row]
        if(firstTab){
            if(channels[indexPath.row].joined!){
                cell.joinButton.isHidden = true
            }else{
                cell.joinButton.isHidden = false
                cell.userID = completeInfo.data.id
                cell.channelID = channels[indexPath.row].id
                cell.teamID = getTeamID()
                cell.companyID = selectedCompanyId
            }
        }else{
            cell.isPublic = false
            cell.joinButton.isHidden = true
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(firstTab){
            if(channels[indexPath.row].joined!){
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let settingsController = storyboard.instantiateViewController(withIdentifier: "ChatScreen") as! ChatScreen
                if(!(searchBar == nil)){
                    searchBar.isHidden = true
                }
                if firstTab || secondTab {
                    settingsController.at = true
                }else{
                    settingsController.at = false
                }
                settingsController.completeinfo = completeInfo
                settingsController.company_id = selectedCompanyId
                settingsController.modalPresentationStyle = .fullScreen
                settingsController.userName = usersString[indexPath.row]
                settingsController.idT = ids[indexPath.row]
                settingsController.idType = type
                settingsController.teamID = getTeamID()
                settingsController.selectteamindex = teamIndex
                self.navigationController?.navigationBar.tintColor = .white
                self.navigationController?.pushViewController(settingsController, animated: true)
            }
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let settingsController = storyboard.instantiateViewController(withIdentifier: "ChatScreen") as! ChatScreen
            if(!(searchBar == nil)){
                searchBar.isHidden = true
            }
            if firstTab || secondTab {
                settingsController.at = true
            }else{
                settingsController.at = false
            }
            settingsController.completeinfo = completeInfo
            settingsController.company_id = selectedCompanyId
            settingsController.modalPresentationStyle = .fullScreen
            settingsController.userName = usersString[indexPath.row]
            settingsController.idT = ids[indexPath.row]
            settingsController.idType = type
            settingsController.teamID = getTeamID()
            settingsController.selectteamindex = teamIndex
            
            self.navigationController?.navigationBar.tintColor = .white
            self.navigationController?.pushViewController(settingsController, animated: true)
        }
    }
}

