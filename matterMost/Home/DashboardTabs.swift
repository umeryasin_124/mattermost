//
//  DashboardTabs.swift
//  matterMost
//
//  Created by abdullah Javed on 19/11/2020.
//

import Foundation
import UIKit

extension HomeScreen : UITabBarDelegate{
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 0:
            type = "channel"
            usersString.removeAll()
            ids.removeAll()
            firstTab  = true
            secondTab = false
            thirdTab = false
            channels = getPublicChannels()
            for i in 0 ..< channels.count{
                ids.append(channels[i].id)
                usersString.append(channels[i].name)
            }
            realData = usersString
            addIcon.image = UIImage(named: "add")
            tableView.reloadData()
            break
        case 1:
            type = "channel"
            usersString.removeAll()
            ids.removeAll()
            secondTab = true
            firstTab = false
            thirdTab = false
            addIcon.image = UIImage(named: "add")
            let channels = getPrivateChannels()
            for i in 0 ..< channels.count{
                ids.append(channels[i].id)
                usersString.append(channels[i].name)
            }
            realData = usersString
            tableView.reloadData()
            break
        case 2:
            type = "direct"
            usersString.removeAll()
            ids.removeAll()
            thirdTab = true
            firstTab = false
            secondTab = false
            addIcon.image = UIImage(named: "chatBubble")
            let direct = getUsersWithDirectChat()
            for i in 0 ..< direct.count{
                if(direct[i].chat){
                    if(!(direct[i].username == nil)){
                        ids.append(direct[i].id)
                        usersString.append(direct[i].username!)
                    }
                }
            }
            realData = usersString
            tableView.reloadData()
            break
            
        default: break
        }
    }
    func getPrivateChannels() -> [PrivateChannel]{
        for i in 0 ..< completeInfo.data.companies.count{
            if completeInfo.data.companies[i].id == selectedCompanyId!{
                if(completeInfo.data.companies[i].teams.count > 0){
                    return completeInfo.data.companies[i].teams[teamIndex].privateChannels
                }
            }
        }
        return []
    }
    func getPublicChannels() -> [PublicChannel]{
        for i in 0 ..< completeInfo.data.companies.count{
            if completeInfo.data.companies[i].id == selectedCompanyId!{
                if(completeInfo.data.companies[i].teams.count > 0){
                    return completeInfo.data.companies[i].teams[teamIndex].publicChannels
                }
            }
        }
        return []
    }
    func getUsersWithDirectChat() -> [Users]{
        for i in 0 ..< completeInfo.data.companies.count{
            if completeInfo.data.companies[i].id == selectedCompanyId!{
                if(completeInfo.data.companies[i].teams.count > 0){
                    return completeInfo.data.companies[i].teams[teamIndex].users
                }
            }
        }
        return []
    }
    func getTeamID() -> Int{
        for i in 0 ..< completeInfo.data.companies.count{
            if completeInfo.data.companies[i].id == selectedCompanyId!{
                return completeInfo.data.companies[i].teams[teamIndex].id
            }
        }
        return 0
    }
}
