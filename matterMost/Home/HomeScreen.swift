//
//  HomeScreen.swift
//  ChatApp
//
//  Created by abdullah Javed on 28/10/2020.
//

import UIKit
import TPKeyboardAvoiding

class HomeScreen: UIViewController, NavigationMenuViewDelegate,UISearchBarDelegate {
    @IBOutlet weak var tabBar: UITabBar!
    var navigationMenuView: NavigationMenuView!
    var blackScreen: UIView!
    var searchBar:UISearchBar!
    var usersString = [String]()
    @IBOutlet weak var addIcon: UIImageView!
    var realData: [String]!
    var isMenuOpen: Bool!
    var firstTab:Bool!
    var secondTab:Bool!
    var thirdTab:Bool!
    @IBOutlet weak var tableView: UITableView!
    var apiResponse:[String:Any]!
    var completeInfo: UserCompleteInfo!
    var selectedCompanyId: Int!
    var teamId: Int!
    var teamIndex = 0
    var ids = [Int]()
    var type:String!
    var channels: [PublicChannel]!
    
    var apiManager: APIManager!
    var data = [String: Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        apiManager = APIManager()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "UserCell", bundle: nil), forCellReuseIdentifier: "usercell")
        
        firstTabSetup()
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(rightSwipeGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(leftSwipeGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        tabBar.selectedItem = tabBar.items?.first
        tabBar.delegate = self
        let btnMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "menu"), style: .plain, target: self, action: #selector(btnMenuAction))
        btnMenu.tintColor=UIColor(red: 255, green: 255, blue: 255, alpha: 1.0)
        let btnSearch = UIBarButtonItem(image: #imageLiteral(resourceName: "search"), style: .plain, target: self, action: #selector(btnSearchAction))
        btnSearch.tintColor=UIColor(red: 255, green: 255, blue: 255, alpha: 1.0)
        
        self.navigationItem.leftBarButtonItem = btnMenu
        self.navigationItem.rightBarButtonItem = btnSearch
        
        navigationMenuView = NavigationMenuView(frame: CGRect(x: 0, y: 0, width: 0, height: self.view.frame.height))
        navigationMenuView.delegate = self
        navigationMenuView.selectedCompanyId = selectedCompanyId
        navigationMenuView.userCompleteInfo = completeInfo
        navigationMenuView.layer.zPosition=100
        self.view.isUserInteractionEnabled = true
        self.navigationController?.view.addSubview(navigationMenuView)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 5/255, green: 49/255, blue: 106/255, alpha: 1.0)
        
        
        
        blackScreen = UIView(frame: self.view.bounds)
        blackScreen.backgroundColor=UIColor(white: 0, alpha: 0.5)
        blackScreen.isHidden=true
        self.view.addSubview(blackScreen)
        blackScreen.layer.zPosition = 99
        let tapGestRecognizer = UITapGestureRecognizer(target: self, action: #selector(blackScreenTapAction(sender:)))
        blackScreen.addGestureRecognizer(tapGestRecognizer)
    }
    func firstTabSetup(){
        type = "channel"
        usersString.removeAll()
        ids.removeAll()
        tabBar.selectedItem = tabBar.items![0]
        firstTab = true
        secondTab = false
        thirdTab = false
        isMenuOpen = false
        channels = getPublicChannels()
        for i in 0 ..< channels.count{
            ids.append(channels[i].id)
            usersString.append(channels[i].name)
        }
        realData = usersString
        addIcon.image = UIImage(named: "add")
        tableView.reloadData()
    }
    @objc func btnMenuAction() {
        openMenu()
    }
    @objc func blackScreenTapAction(sender: UITapGestureRecognizer) {
        blackScreen.isHidden=true
        blackScreen.frame=self.view.bounds
        self.view.layoutIfNeeded()
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3) {
                self.navigationMenuView.frame=CGRect(x: 0, y: 0, width: 0, height: self.navigationMenuView.frame.height)
                self.view.layoutIfNeeded()
            }
        }
    }
    @objc func btnSearchAction() {
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: (self.navigationController?.navigationBar.frame.width)!, height: (self.navigationController?.navigationBar.frame.height)!))
        
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        searchBar.backgroundColor = .white
        searchBar.tintColor = UIColor(red: 5/255, green: 49/255, blue: 106/255, alpha: 1.0)
        
        self.navigationController?.navigationBar.addSubview(searchBar)
    }
    func navigationDidSelectRowAt(row: Int) {
        usersString.removeAll()
        teamIndex = row
        closeMenu()
        firstTabSetup()
    }
    func navigationButtonClicked(buttonTag: Int) {
        if(buttonTag == 1){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let settingsController = storyboard.instantiateViewController(withIdentifier: "SettingssTableViewController")
            if(!(searchBar == nil)){
                searchBar.isHidden = true
            }
            settingsController.modalPresentationStyle = .fullScreen
            closeMenu()
            self.navigationController?.navigationBar.tintColor = .white
            self.navigationController?.pushViewController(settingsController, animated: true)
        }else if(buttonTag == 2){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let companyPicker = storyboard.instantiateViewController(withIdentifier: "SelectCompany") as! SelectCompany
            if(!(searchBar == nil)){
                searchBar.isHidden = true
            }
            companyPicker.modalPresentationStyle = .fullScreen
            companyPicker.apiResponse = apiResponse
            self.dismiss(animated: true, completion: nil)
            self.present(companyPicker, animated: true, completion: nil)
        }else if(buttonTag == 3){
            logOutAPI()
        }
    }
    
    func logOutAPI(){
        
        let url = ENDPOINTS.logout
        let token = UserDefaults.standard.string(forKey: "token")
        
        
        let alert = UIAlertController(title: "Logout", message: "Are you sure you want to logout", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { [self] (action: UIAlertAction!) in
            do{
                
                apiManager.apiCaller(url: url, sendBodyData: data, httpMethod: "POST", token: token!) { [self] (result) in
                    
                    switch result{
                    case .success( _):
                        DispatchQueue.main.async {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let settingsController = storyboard.instantiateViewController(withIdentifier: "SignIn")
                            if(!(searchBar == nil)){
                                searchBar.isHidden = true
                            }
                            UserDefaults.standard.set("", forKey: "token")
                            settingsController.modalPresentationStyle = .fullScreen
                            closeMenu()
                            self.navigationController?.navigationBar.tintColor = .white
                            self.navigationController?.pushViewController(settingsController, animated: true)
                            
                        }
                        break
                    case .failure(let error):
                        DispatchQueue.main.async {
                            print("Error \(error)")
                            self.showCustomAlert(title:"Error", message: error.title!)
                            
                        }
                    }
                }
                
            }
            catch{}
            
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        
        self.present(alert, animated: true)
        
        
    }
    
    func showCustomAlert(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.isHidden = true
        usersString = realData
        UIApplication.shared.sendAction(#selector(resignFirstResponder), to: nil, from: nil, for: nil)
        tableView.reloadData()
    }
    func openMenu(){
        blackScreen.isHidden=false
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3, animations: { [self] in
                self.navigationMenuView.frame=CGRect(x: 0, y: 0, width: 240, height: self.navigationMenuView.frame.height)
                self.view.layoutIfNeeded()
            }) { (complete) in
                self.blackScreen.frame = CGRect(x: self.navigationMenuView.frame.width, y: 0, width: self.view.frame.width-self.navigationMenuView.frame.width, height: self.view.bounds.height+100)
                self.view.layoutIfNeeded()
            }
        }
    }
    func closeMenu(){
        blackScreen.isHidden=true
        blackScreen.frame=self.view.bounds
        self.view.layoutIfNeeded()
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3) {
                self.navigationMenuView.frame = CGRect(x: 0, y: 0, width: 0, height: self.navigationMenuView.frame.height)
                self.view.layoutIfNeeded()
            }
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if(searchText.isEmpty){
            usersString = realData
            tableView.reloadData()
        }else{
            usersString = realData.filter { $0.lowercased().contains(searchText.lowercased())}
            tableView.reloadData()
        }
    }
    @IBAction func addGroup(_ sender: Any) {
        if(firstTab){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "GroupDetailViewController") as! GroupDetailViewController
            if(!(searchBar == nil)){
                searchBar.isHidden = true
            }
            vc.completeInfo = completeInfo
            vc.companyID = selectedCompanyId
            vc.teamID = getTeamID()
            vc.selectedTeamIndex = teamIndex
            self.navigationController?.pushViewController(vc, animated: true)
        }else if(secondTab){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "GroupDetailViewController") as! GroupDetailViewController
            if(!(searchBar == nil)){
                searchBar.isHidden = true
            }
            vc.completeInfo = completeInfo
            vc.companyID = selectedCompanyId
            vc.teamID = getTeamID()
            vc.selectedTeamIndex = teamIndex
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "UserMessagesViewController") as! UserMessagesViewController
            if(!(searchBar == nil)){
                searchBar.isHidden = true
            }
            vc.userCompleteInfo = completeInfo
            vc.selectedCompanyId = selectedCompanyId
            vc.selectedTeamID = teamIndex
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}
