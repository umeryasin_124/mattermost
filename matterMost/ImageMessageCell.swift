//
//  ImageMessageCell.swift
//  chatscreen
//
//  Created by ethisham bader on 04/11/2020.
//

import UIKit

class ImageMessageCell: UITableViewCell {

    @IBOutlet weak var sendingTime: UILabel!
    @IBOutlet weak var imageMessage: UIImageView!
    @IBOutlet weak var imageTextMessage: UILabel!
    
    @IBOutlet weak var namee: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        sendingTime.text = DateFormatter.localizedString(from: Date(), dateStyle: .none, timeStyle: .short)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
