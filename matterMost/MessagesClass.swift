//
//  Messages.swift
//  matterMost
//
//  Created by ethisham bader on 09/11/2020.
//

import Foundation
import UIKit

class MessagesClass {
    var timeStamp:String!
    var message:String!
    var audio:String!
    var video:String!
    var image:String!
    var userNamepp:String!
    var document:String!
    var id:Int!
    var user_id:Int!
    var extensionP:String!
    var size:String!
    
    init (user_id:Int,id:Int,document:String,extensionP:String,size:String,userNamepp:String){
        self.user_id = user_id
        self.id = id
        self.document = document
        self.extensionP = extensionP
        self.size = size
        self.userNamepp = userNamepp

    }
    init(user_id:Int,id:Int,message:String,timeStamp:String,userNamepp:String) {
        self.user_id = user_id
        self.id = id
        self.message = message
        self.timeStamp = timeStamp
        self.userNamepp = userNamepp
    }
    init(user_id:Int,id:Int,image:String,timeStamp:String,userNamepp:String) {
        self.user_id = user_id
        self.id = id
        self.image = image
        self.userNamepp = userNamepp

    }
    init(user_id:Int,id:Int,audio:String,timeStamp:String,userNamepp:String) {
        self.user_id = user_id
        self.id = id
        self.audio = audio
        self.userNamepp = userNamepp
    }
    init(user_id:Int,id:Int,video:String,timeStamp:String,userNamepp:String) {
        self.user_id = user_id
        self.id = id
        self.video = video
        self.userNamepp = userNamepp

    }
    func getMessage() -> String{
        return message
    }
    func getDocument() -> String{
        return document
    }
    func getAudioMessage() -> String{
        return audio
    }
    func getVideoMessage() -> String{
        return video
    }
    func getImageMessage() -> String{
        return image
    }
    
}
