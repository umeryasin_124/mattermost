//
//  Messages.swift
//  matterMost
//
//  Created by mujtaba Hassan on 09/11/2020.
//

import Foundation
import UIKit

class Message {
    
    var id:Int
    var recieverId: String
    var companyId: String
    var timeStamp: String
    
    init(){
        
        self.id = 0
        self.recieverId = " "
        self.companyId = " "
        self.timeStamp = " "
    }
    
    init(id: Int, recieverId: String, companyId: String, timeStamp: String) {
        
        self.id = id
        self.recieverId = recieverId
        self.companyId = companyId
        self.timeStamp = timeStamp
    }
    
    func setId(id: Int){
        self.id = id
    }
    
    func setRecieverId(recieverId: String){
        self.recieverId = recieverId
    }
    
    func setRecieverId(senderId: String){
        self.companyId = senderId
    }
    
    func setTimeStamp(timeStamp: String){
        self.timeStamp = timeStamp
    }
    
 
    class TextMessage: Message {
        
        var textData: String = " "
        
       override init() {
            self.textData = " "
            super.init()
        }
        
        init(id: Int, recieverId: String, companyId: String, timeStamp: String, textData: String){
            self.textData = textData
            super.init(id: id, recieverId: recieverId, companyId: companyId, timeStamp: timeStamp)
        }
        
        func setTextData(textData: String){
            self.textData = textData
        }
    }
    
    class AudioMessage: Message {
        
        var audioData: String = " "
        
        override init() {
            self.audioData = " "
            super.init()
        }
        
        init(id: Int, recieverId: String, companyId: String, timeStamp: String, audioData: String){
            self.audioData = audioData
            super.init(id: id, recieverId: recieverId, companyId: companyId, timeStamp: timeStamp)
        }
        
        func setAudioData(audioData: String){
            self.audioData = audioData
        }
    }
    
    class VideoMessage: Message {
        
        var videoData: String = " "
        
        override init() {
            self.videoData = " "
            super.init()
        }
        
        init(id: Int, recieverId: String, companyId: String, timeStamp: String, videoData: String){
            self.videoData = videoData
            super.init(id: id, recieverId: recieverId, companyId: companyId, timeStamp: timeStamp)
        }
        
        
        func setvideoData(videoData: String){
            self.videoData = videoData
        }
    }
    
    class DocMessage: Message {
        
        var docData: String = " "
        
        override init() {
            self.docData = " "
            super.init()
        }
        
        init(id: Int, recieverId: String, companyId: String, timeStamp: String, docData: String) {
            self.docData = docData
            super.init(id: id, recieverId: recieverId, companyId: companyId, timeStamp: timeStamp)
        }
        
        func setAudioData(docData: String){
            self.docData = docData
        }
        
    }
  
}


