
import Foundation

struct UserCompleteInfo: Codable {
    let statusCode: Int
    let statusMessage: String
    let admin, accountSetup: Bool
    let token: String
    let data: DataClassU

    init() {
        statusCode = 0
        statusMessage = ""
        admin = false
        accountSetup = false
        token = ""
        data = DataClassU()
    }
    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case statusMessage = "status_message"
        case admin
        case accountSetup = "account_setup"
        case token, data
    }
}

// MARK: - DataClass
struct DataClassU: Codable {
    let id: Int
    let email, username: String
    let profilePicture: String?
    let companies: [CompanyU]
    init() {
        id = 0
        email = ""
        username = ""
        profilePicture = nil
        companies = [CompanyU()]
    }
    enum CodingKeys: String, CodingKey {
        case id, email, username
        case profilePicture = "profile_picture"
        case companies
    }
}


// MARK: - Company
struct CompanyU: Codable {
    let id: Int
    let name: String
    let teams: [TeamU]
    
    init() {
            id = 0
            name = ""
            teams = [TeamU()]
    }
}

// MARK: - Team
struct TeamU: Codable {
    let id: Int
    let name: String
    let privateChannels: [PrivateChannel]
    let publicChannels: [PublicChannel]
    let users: [Users]

    init() {
        id = 0
        name = ""
        privateChannels = [PrivateChannel()]
        publicChannels =  [PublicChannel()]
        users = [Users()]
    }
    enum CodingKeys: String, CodingKey {
        case id, name
        case privateChannels = "private_channels"
        case publicChannels = "public_channels"
        case users
    }
}

// MARK: - PrivateChannel
struct PrivateChannel: Codable {
    let id: Int
    let name: String
    
    init() {
        id = 0
        name = ""
    }
}

// MARK: - PublicChannel
struct PublicChannel: Codable {
    let id: Int
    let name: String
    let joined: Bool?
    
    init() {
        id = 0
        name = ""
        joined = false
    }
}

// MARK: - User
struct Users: Codable {
    let id: Int
    let username: String?
    let email: String
    let chat: Bool
    
    init() {
        id = 0
        username = ""
        email = ""
        chat = false
    }
}
struct GroupMembers: Codable {
    let id: Int
    let username: String?
    let email: String
    let profile_picture: String
    
    init() {
        id = 0
        username = ""
        email = ""
        profile_picture = ""
    }
}
