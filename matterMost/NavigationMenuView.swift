//
//  NavigationMenuView.swift
//  ChatApp
//
//  Created by abdullah Javed on 28/10/2020.
//

import UIKit

protocol NavigationMenuViewDelegate {
    func navigationDidSelectRowAt(row: Int)
    func navigationButtonClicked(buttonTag: Int)
}



class NavigationMenuView: UIView,UITableViewDelegate,UITableViewDataSource{
    var dataDictionary = [String: String]()

    var apiManger: APIManager!
    
    var delegate: NavigationMenuViewDelegate?
    static var menuView: NavigationMenuView!
    var tableView: UITableView!
    var settings:UIButton!
    var switchCompany:UIButton!
    var logOut:UIButton!
    var emailLabel: UILabel!
    var userCompleteInfo:UserCompleteInfo!
    var selectedCompanyId: Int!
    var teams: [TeamU]!
    var profile: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    
    var teamsCount = 0
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clipsToBounds=true
        guard let contentView = Bundle(for: type(of: self)).loadNibNamed("MenuView", owner: self, options: nil)?.first as? UIView else {
            return
        }
        setupViews(contentView: contentView)
    }
    func setupViews(contentView: UIView){
        contentView.frame = self.frame
        self.addSubview(contentView)
        tableView = (contentView.viewWithTag(100) as! UITableView)
        settings = (contentView.viewWithTag(101) as! UIButton)
        switchCompany = (contentView.viewWithTag(102) as! UIButton)
        logOut = (contentView.viewWithTag(103) as! UIButton)
        emailLabel = (contentView.viewWithTag(104) as! UILabel)
        profile = (contentView.viewWithTag(105) as! UIImageView)
        
       // emailLabel.text = UserDefaults.standard.string(forKey: "email")
        let  profileUrl = UserDefaults.standard.data(forKey: "userProfilePic")
        if !(profileUrl == nil){
            self.profile.image = UIImage(data: profileUrl!)
        }
        
        settings.addTarget(self,action: #selector(settingsAction),for: .touchUpInside)
        switchCompany.addTarget(self,action: #selector(switchCompanyAction),for: .touchUpInside)
        logOut.addTarget(self,action: #selector(logoutAction),for: .touchUpInside)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MenuCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    @objc func settingsAction(){
        self.delegate?.navigationButtonClicked(buttonTag: 1)
    }
    @objc func switchCompanyAction(){
        self.delegate?.navigationButtonClicked(buttonTag: 2)
    }
    @objc func logoutAction(){
        self.delegate?.navigationButtonClicked(buttonTag: 3)
    }

    func getContentView() -> UIView{
        guard let contentView = Bundle(for: type(of: self)).loadNibNamed("MenuView", owner: self, options: nil)?.first as? UIView else {
            return UIView()
        }
        return contentView
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        emailLabel.text = userCompleteInfo.data.email
        for i in 0 ..< userCompleteInfo.data.companies.count{
            print(userCompleteInfo.data.companies)
            if userCompleteInfo.data.companies[i].id == selectedCompanyId!{
                teams = userCompleteInfo.data.companies[i].teams
                teamsCount = userCompleteInfo.data.companies[i].teams.count
                return teamsCount
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath)
        if(cell.isSelected){
            cell.backgroundColor = .darkGray
            cell.isSelected = false
        }else{
            cell.backgroundColor = .clear
        }
        cell.selectionStyle = .none
        cell.textLabel?.text = teams[indexPath.row].name
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0 ..< teamsCount{
            if(!(i == indexPath.row)){
                tableView.cellForRow(at: IndexPath(row: i, section: 0))?.isSelected = true
            }
        }
        self.delegate?.navigationDidSelectRowAt(row: indexPath.row)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    

}
