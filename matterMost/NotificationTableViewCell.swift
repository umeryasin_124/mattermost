//
//  NotificationTableViewCell.swift
//  matterMost
//
//  Created by Umer yasin on 02/11/2020.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var imgetoChange: UIImageView!
    var notification:Bool = true
    var uiviewcontroller = UIViewController()

    var muteImage = UIImage(named: "notiNo")
    var unmuteImage = UIImage(named: "notiYes")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let templateImage = self.imgetoChange.image!.withRenderingMode(.alwaysTemplate)
        self.imgetoChange.image = templateImage
        self.imgetoChange.tintColor = .darkGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func onCLickButton(_ sender: Any) {
        
        if notification {
            
            let alert = UIAlertController(title: "Alert", message: "Are you Sure want to Mute", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "No",
                                          style: UIAlertAction.Style.default,
                                                  handler: {(_: UIAlertAction!) in
                                                   
                    }))
            alert.addAction(UIAlertAction(title: "Yes",
                                          style: UIAlertAction.Style.default,
                                                  handler: {(_: UIAlertAction!) in
                                                    self.notification = false
                                                    self.imgetoChange.image = UIImage(named: "notiNo")
                                                    let templateImage = self.imgetoChange.image!.withRenderingMode(.alwaysTemplate)
                                                    self.imgetoChange.image = templateImage
                                                    self.imgetoChange.tintColor = .darkGray
                    }))
            uiviewcontroller.present(alert, animated: true, completion: nil)
        }else{
            
            let alert = UIAlertController(title: "Alert", message: "Are you Sure want to UnMute", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "No",
                                          style: UIAlertAction.Style.default,
                                                  handler: {(_: UIAlertAction!) in
                    }))
            alert.addAction(UIAlertAction(title: "Yes",
                                          style: UIAlertAction.Style.default,
                                                  handler: {(_: UIAlertAction!) in
                                                    self.notification = true
                                                    self.imgetoChange.image = UIImage(named: "notiYes")
                                                    let templateImage = self.imgetoChange.image!.withRenderingMode(.alwaysTemplate)
                                                    self.imgetoChange.image = templateImage
                                                    self.imgetoChange.tintColor = .darkGray
                    }))
            uiviewcontroller.present(alert, animated: true, completion: nil)
        }
    }
}
