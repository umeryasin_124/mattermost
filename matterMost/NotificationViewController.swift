//
//  NotificationViewController.swift
//  matterMost
//
//  Created by Umer yasin on 10/11/2020.
//

import UIKit

class NotificationViewController: UIViewController {
    
    var notification:Bool = true
    var muteImage = UIImage(named: "notiNo")
    var unmuteImage = UIImage(named: "notiYes")
    
    @IBOutlet weak var ImageViewImahge: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let templateImage = self.ImageViewImahge.image!.withRenderingMode(.alwaysTemplate)
        self.ImageViewImahge.image = templateImage
        self.ImageViewImahge.tintColor = .darkGray
        
    }
    @IBAction func onCLickButton(_ sender: Any) {
        if notification {
            let alert = UIAlertController(title: "Alert", message: "Are you Sure want to Mute", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "No",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                          }))
            alert.addAction(UIAlertAction(title: "Yes",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.notification = false
                                            self.ImageViewImahge.image = UIImage(named: "notiNo")
                                            let templateImage = self.ImageViewImahge.image!.withRenderingMode(.alwaysTemplate)
                                            self.ImageViewImahge.image = templateImage
                                            self.ImageViewImahge.tintColor = .darkGray
                                            
                                          }))
            present(alert, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Alert", message: "Are you Sure want to UnMute", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "No",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                          }))
            alert.addAction(UIAlertAction(title: "Yes",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.notification = true
                                            self.ImageViewImahge.image = UIImage(named: "notiYes")
                                            let templateImage = self.ImageViewImahge.image!.withRenderingMode(.alwaysTemplate)
                                            self.ImageViewImahge.image = templateImage
                                            self.ImageViewImahge.tintColor = .darkGray
                                            
                                          }))
            present(alert, animated: true, completion: nil)
        }
    }
}
