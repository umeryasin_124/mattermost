import UIKit
import IHKeyboardAvoiding

class OTPViewController: UIViewController{
    
    var recivedCode: String! = ""
    var email: String! = "" //Recieving Email from Confirm Email Screen
    
    var dataDictionary = [String: String]() //Dictonary to send Data to Server
    var apiManger: APIManager!
    
    @IBOutlet weak var activityLoaderr: UIActivityIndicatorView!
    @IBOutlet weak var codeTextFeild1: UITextField!
    @IBOutlet weak var codeTextFeild2: UITextField!
    @IBOutlet weak var codeTextFeild3: UITextField!
    @IBOutlet weak var codeTextFeild4: UITextField!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var invalidCodeLable: UILabel!
    @IBOutlet weak var subView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityLoaderr.isHidden = true
        apiManger = APIManager()
        invalidCodeLable.isHidden = true
        self.hideKeyboardWhenTappedAround()
        
        let newBackButton = UIBarButtonItem(title:"SignIn", style: UIBarButtonItem.Style.plain, target: self, action: #selector(OTPViewController.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
    }
    override func viewDidAppear(_ animated: Bool) {
        KeyboardAvoiding.avoidingView = self.subView
        activityLoaderr.isHidden = true
        setDelegates()
    }
    
    //Naviagting Back to Sign Screen
    @objc func back(sender: UIBarButtonItem) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //Button to send Otp Code to server
    @IBAction func onContinue(_ sender: Any) {
        if codeTextFeild1.text != "" && codeTextFeild2.text != "" && codeTextFeild3.text != "" && codeTextFeild4.text != ""{
            invalidCodeLable.isHidden = true
            if ConnectivityService().isConnectedToNetwork() {
                activityLoaderr.isHidden = false
                activityLoaderr.startAnimating()
                
                getTheCode() // getting appended OTP
                otpApiCall() //OTP Api Call Here
            }else{
                activityLoaderr.stopAnimating()
                activityLoaderr.isHidden = true
                showCustomAlert(title: "", message: "No Internet Connection")}
        }else{
            resetFields() //Clearing All Otp Fields
        }
    }
    
    //OTP code requested
    @IBAction func resendCode(_ sender: Any) {
        if ConnectivityService().isConnectedToNetwork() {
            activityLoaderr.isHidden = false
            activityLoaderr.startAnimating()
            resendOtpApiCall() //Resending Otp Code to Email
        }else{
            activityLoaderr.stopAnimating()
            activityLoaderr.isHidden = true
            showCustomAlert(title: "", message: "No Internet Connection")}
    }
    func checkFieldLimit(codeTextFeild: UITextField)-> Bool{
        if codeTextFeild.text!.count > 0{
            return true
        }else
        { return false}
    }
    
    //Appending OTP to Fields
    func getTheCode(){
        recivedCode = codeTextFeild1.text!
        recivedCode!.append(codeTextFeild2.text!)
        recivedCode!.append(codeTextFeild3.text!)
        recivedCode!.append(codeTextFeild4.text!)
    }
    //Clearing OTP Fields
    func resetFields(){
        invalidCodeLable.text = "Invalid Code"
        invalidCodeLable.isHidden = false
        
        codeTextFeild1.text? = ""
        codeTextFeild2.text? = ""
        codeTextFeild3.text? = ""
        codeTextFeild4.text? = ""
    }
    //Sending OTP to server
    func otpApiCall(){
        dataDictionary = ["email": email, "otp": recivedCode]
        let url = ENDPOINTS.enterOTP
        apiManger.apiCaller(url: url, sendBodyData: dataDictionary, httpMethod: "POST"){[self] (result) in
            switch result{
            case .success( _):
                DispatchQueue.main.async {
                    activityLoaderr.stopAnimating()
                    activityLoaderr.isHidden = true
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "UpdatePassword") as! UpdatePassword
                    vc.email = email
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    resetFields()
                    activityLoaderr.stopAnimating()
                    showCustomAlert(title: "OTP Failed", message: error.title!)
                    activityLoaderr.isHidden = true
                }
            }
        }
    }
    
    //requesting otp again from server
    func resendOtpApiCall(){
        dataDictionary["email"] = email
        //Api Call Here
        apiManger.apiCaller(url: ENDPOINTS.forgetPassword, sendBodyData: dataDictionary, httpMethod: "POST") { [self] (result)  in
            switch result{
            case .success(let response):
                DispatchQueue.main.async {
                    showCustomAlert(title: "Email Send", message: response["status_message"] as! String)
                    invalidCodeLable.text = "Check Your email for new Code"
                    invalidCodeLable.isHidden = false
                    activityLoaderr.isHidden = true
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    showCustomAlert(title: "Email Sending Failed", message: error.title!)
                    activityLoaderr.isHidden = true
                }
            }
        }
    }
    
    func showCustomAlert(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func setDelegates(){
        codeTextFeild1.delegate = self
        codeTextFeild2.delegate = self
        codeTextFeild3.delegate = self
        codeTextFeild4.delegate = self
    }
}

//Delagate Functions of TextFields
extension OTPViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if checkFieldLimit(codeTextFeild: textField){
            textField.text = ""
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if checkFieldLimit(codeTextFeild: textField){
            if(textField == codeTextFeild1){
                codeTextFeild1.endEditing(true)
                if(codeTextFeild2.text?.count == 0){
                    codeTextFeild2.becomeFirstResponder()
                }
            }
            if(textField == codeTextFeild2){
                codeTextFeild2.endEditing(true)
                if(codeTextFeild3.text?.count == 0){
                    codeTextFeild3.becomeFirstResponder()
                }
            }
            if(textField == codeTextFeild3){
                codeTextFeild3.endEditing(true)
                if(codeTextFeild4.text?.count == 0){
                    codeTextFeild4.becomeFirstResponder()
                }
            }
            if(textField == codeTextFeild4){
                codeTextFeild4.endEditing(true)
            }
        }
        return true
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if checkFieldLimit(codeTextFeild: textField){
            if(textField == codeTextFeild1){
                codeTextFeild1.endEditing(true)
                if(codeTextFeild2.text?.count == 0){
                    codeTextFeild2.becomeFirstResponder()
                }
            }
            if(textField == codeTextFeild2){
                codeTextFeild2.endEditing(true)
                if(codeTextFeild3.text?.count == 0){
                    codeTextFeild3.becomeFirstResponder()
                }
            }
            if(textField == codeTextFeild3){
                codeTextFeild3.endEditing(true)
                if(codeTextFeild4.text?.count == 0){
                    codeTextFeild4.becomeFirstResponder()
                }
            }
            if(textField == codeTextFeild4){
                codeTextFeild4.endEditing(true)
            }
        }
    }
    
}
