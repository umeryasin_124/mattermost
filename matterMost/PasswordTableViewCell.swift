//
//  PasswordTableViewCell.swift
//  matterMost
//
//  Created by Umer yasin on 02/11/2020.
//


import UIKit
import IHKeyboardAvoiding
import TPKeyboardAvoiding

class PasswordTableViewCell: UITableViewCell {
    
    //current pass
    @IBOutlet weak var newPassText: UITextField!
    @IBOutlet weak var newPassEdit: UIButton!
    //confirm pass
    @IBOutlet weak var confirmCurrenttxt: UITextField!
    //new passs
    @IBOutlet weak var confirmPassText: UITextField!
    @IBOutlet weak var confirmPassCross: UIButton!
    @IBOutlet weak var confirmPassTick: UIButton!
    
    var uiviewcontroller = UIViewController()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        confirmPassTick.isHidden = true
        confirmPassCross.isHidden = true
        self.newPassText.isUserInteractionEnabled = false
        self.confirmPassText.isUserInteractionEnabled = false
        self.confirmCurrenttxt.isUserInteractionEnabled = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    @IBAction func onclickEditBtn(_ sender: Any) {
        newPassEdit.isHidden = true
        confirmPassTick.isHidden = false
        confirmPassCross.isHidden = false
        confirmCurrenttxt.isHidden = false
        self.newPassText.isUserInteractionEnabled = true
        self.confirmPassText.isUserInteractionEnabled = true
        self.confirmCurrenttxt.isUserInteractionEnabled = true
    }
    @IBAction func onCLickTick(_ sender: Any) {
        newPassEdit.isHidden = false
        confirmPassTick.isHidden = true
        confirmPassCross.isHidden = true
        let alert = UIAlertController(title: "Alert", message: "Are you Sure want to Change Password", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.newPassText.text = ""
                                        self.confirmPassText.text = ""
                                        self.confirmCurrenttxt.text = ""
                                        self.newPassText.isUserInteractionEnabled = false
                                        self.confirmPassText.isUserInteractionEnabled = false
                                        self.confirmCurrenttxt.isUserInteractionEnabled = false
                                      }))
        alert.addAction(UIAlertAction(title: "Yes",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.newPassText.text = ""
                                        self.confirmPassText.text = ""
                                        self.confirmCurrenttxt.text = ""
                                        self.newPassText.isUserInteractionEnabled = false
                                        self.confirmPassText.isUserInteractionEnabled = false
                                        self.confirmCurrenttxt.isUserInteractionEnabled = false
                                        if(Validator.isValidPassword(self.confirmPassText.text!)){
                                            print("yes")
                                        }
                                        
                                      }))
        uiviewcontroller.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func onClickCross(_ sender: Any) {
        newPassEdit.isHidden = false
        confirmPassTick.isHidden = true
        confirmPassCross.isHidden = true
        self.newPassText.text = ""
        self.confirmPassText.text = ""
        self.confirmCurrenttxt.text = ""
        self.newPassText.isUserInteractionEnabled = false
        self.confirmPassText.isUserInteractionEnabled = false
        self.confirmCurrenttxt.isUserInteractionEnabled = false
        
    }
}
