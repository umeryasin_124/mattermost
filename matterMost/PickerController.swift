//
//  PickerController.swift
//  IOS Chat App Layout
//
//  Created by Muhammad Huzaifa on 02/11/2020.
//

import UIKit

class PickerController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    @IBOutlet weak var nextBtn: UIBarButtonItem!
    
    @IBOutlet weak var myPicker: UIPickerView!
    var list = ["Techline","PF","DevCo","Quick Bit","Other"]
    @IBOutlet weak var selectCompanyView: UIToolbar!
    @IBOutlet weak var selectCompany: UIBarButtonItem!
    @IBOutlet weak var moveNext: UIButton!
    var pickerView: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        pickerView = UIPickerView()
//        pickerView.delegate = self
//        pickerView.dataSource = self
//        let toolBar = UIToolbar()
//        toolBar.barStyle = UIBarStyle.default
//        toolBar.isTranslucent = true
//        toolBar.tintColor = UIColor(red: 5/255, green: 49/255, blue: 106/255, alpha: 1)
//        toolBar.sizeToFit()
//
//        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(donePicker))
//        toolBar.setItems([doneButton], animated: false)
//        toolBar.isUserInteractionEnabled = true
//        self.textPicker.inputView = pickerView
//        self.textPicker.inputAccessoryView = toolBar
        self.myPicker.delegate = self
        self.myPicker.dataSource = self
        self.myPicker.isHidden = true
        self.moveNext.isEnabled = false
        if(self.moveNext.isEnabled == false)
        {
            self.moveNext.setTitleColor(.gray, for: .normal)
        }

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        selectCompanyView.addGestureRecognizer(tap)
    }
//    @objc func donePicker() {
//        self.selectCompany.title = self.list[myPicker.selectedRow(inComponent: 0)]
//        textPicker.resignFirstResponder()
//        //self.myPicker.isHidden = true
//        self.nextBtn.isEnabled = true
//        self.moveNext.isEnabled = true
//        if(self.moveNext.isEnabled == true){
//            self.moveNext.setTitleColor(.white, for: .normal)
//        }
//    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        print("Hello")
        self.myPicker.isHidden = false

    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return list.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
      return  self.list[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

         self.selectCompany.title = self.list[row]
         self.myPicker.isHidden = true
        self.nextBtn.isEnabled = true
        self.moveNext.isEnabled = true
        if(self.moveNext.isEnabled == true)
        {
            self.moveNext.setTitleColor(.white, for: .normal)
        }

    }
    
    @IBAction func openPicker(_ sender: Any) {
        if(self.selectCompany.title == "Select Company"){
            self.myPicker.isHidden = false
            //self.nextBtn.isEnabled = false
            self.moveNext.isEnabled = false
            if(self.moveNext.isEnabled == false)
            {
                self.moveNext.setTitleColor(.gray, for: .normal)
            }

        }
        else
        {
            if(self.myPicker.isHidden){
                self.myPicker.isHidden = false
               // self.selectCompany.title = self.list[self.pickerView.selectedRow(inComponent: 0)]
               // nextBtn.isEnabled = false
                moveNext.isEnabled = false
                if(self.moveNext.isEnabled == false)
                {
                    self.moveNext.setTitleColor(.gray, for: .normal)
                }

            }else{
                self.myPicker.isHidden = true
               // self.selectCompany.title = self.list[self.pickerView.selectedRow(inComponent: 0)]
                //nextBtn.isEnabled = true
                moveNext.isEnabled = true
                if(self.moveNext.isEnabled == true)
                {
                    self.moveNext.setTitleColor(.white, for: .normal)
                }
            }
        }
        
    }
    @IBAction func nextBtn(_ sender: Any) {
        if(self.selectCompany.title == "Select Company"){
            self.myPicker.isHidden = false
            //self.nextBtn.isEnabled = false
            self.moveNext.isEnabled = false
            if(self.moveNext.isEnabled == false)
            {
                self.moveNext.setTitleColor(.gray, for: .normal)
            }
            
        }
        else
        {
            if(self.myPicker.isHidden){
                self.myPicker.isHidden = false
               // self.selectCompany.title = self.list[self.pickerView.selectedRow(inComponent: 0)]
               // nextBtn.isEnabled = false
                moveNext.isEnabled = false
                if(self.moveNext.isEnabled == false)
                {
                    self.moveNext.setTitleColor(.gray, for: .normal)
                }
            
            }else{
                self.myPicker.isHidden = true
               // self.selectCompany.title = self.list[self.pickerView.selectedRow(inComponent: 0)]
                nextBtn.isEnabled = true
                moveNext.isEnabled = true
                if(self.moveNext.isEnabled == true)
                {
                    
                    self.moveNext.setTitleColor(.white, for: .normal)
                }
            }
        }
    }
    @IBAction func moveNext(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homescreen = storyboard.instantiateViewController(withIdentifier: "HomeScreen")
        self.dismiss(animated: true, completion: nil)
        self.present(homescreen, animated: true, completion: nil)
    }

}
