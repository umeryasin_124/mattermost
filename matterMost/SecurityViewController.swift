//
//  SecurityViewController.swift
//  matterMost
//
//  Created by Umer yasin on 10/11/2020.
//

import UIKit

class SecurityViewController: UIViewController {
    
    let visibleImage = UIImage(named: "eye")
    let invisibleImage = UIImage(named: "eyeGone")
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    var currentBool: Bool = true
    var newBool: Bool = true
    var confirmBool: Bool = true
    var apiManager: APIManager!
    var changePassDic = [String: String]()
    
    @IBOutlet weak var currentPassEye: UIButton!
    @IBOutlet weak var newPassEye: UIButton!
    @IBOutlet weak var confirmPassEye: UIButton!
    @IBOutlet weak var currentPassTextField: UITextField!
    @IBOutlet weak var newPassTextField: UITextField!
    @IBOutlet weak var confirmPassTextField: UITextField!
    @IBOutlet weak var currentPassError: UILabel!
    @IBOutlet weak var newPassError: UILabel!
    @IBOutlet weak var confirmPassError: UILabel!
    
    var userToken: String = ""
    var count: Int = 0
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiManager = APIManager()
        updateIconColor() 
        doneBtn.isEnabled = true
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        userToken = UserDefaults.standard.string(forKey: "token")!
    }
    
    func updateIconColor() {
        
        let tintedImage = visibleImage?.withRenderingMode(.alwaysTemplate)
        currentPassEye.setImage(tintedImage, for: .normal)
        currentPassEye.tintColor = .darkGray
        
        let tintedImage1 = visibleImage?.withRenderingMode(.alwaysTemplate)
        newPassEye.setImage(tintedImage1, for: .normal)
        newPassEye.tintColor = .darkGray
        
        let tintedImage2 = visibleImage?.withRenderingMode(.alwaysTemplate)
        confirmPassEye.setImage(tintedImage2, for: .normal)
        confirmPassEye.tintColor = .darkGray
    }
    
    @IBAction func currentPassHide(_ sender: Any) {
        if(currentBool){
            currentPassTextField.isSecureTextEntry = false
            currentBool = false
    
            let tintedImage1 = invisibleImage?.withRenderingMode(.alwaysTemplate)
            currentPassEye.setImage(tintedImage1, for: .normal)
            currentPassEye.tintColor = .darkGray
            
        }else{
            currentBool = true
            currentPassTextField.isSecureTextEntry = true
            
            let tintedImage1 = visibleImage?.withRenderingMode(.alwaysTemplate)
            currentPassEye.setImage(tintedImage1, for: .normal)
            currentPassEye.tintColor = .darkGray
        }
    }
    @IBAction func done(_ sender: Any) {
        doneBtn.isEnabled = false
        currentPassError.isHidden = true
        newPassError.isHidden = true
        confirmPassError.isHidden = true
        if(!currentPassTextField.text!.isEmpty && !newPassTextField.text!.isEmpty && !confirmPassTextField.text!.isEmpty){
            if(matchPassword()){
                if ConnectivityService().isConnectedToNetwork(){
                    changePassDic["password"] = currentPassTextField.text
                    changePassDic["newpassword"] = newPassTextField.text
                    changePassDic["newpassword_confirmation"] = confirmPassTextField.text
                    if( currentPassTextField.text == newPassTextField.text && newPassTextField.text == confirmPassTextField.text)
                    {
                        showCustomAlert(title: "Email Failed", message: " Current password should be different from new password ")
                    }
                        else
                        {
                            if(Validator.isValidPassword(newPassTextField.text!)){
                                apiManager.apiCaller(url: ENDPOINTS.changePassword, sendBodyData: changePassDic, httpMethod: "POST", token: userToken) { [self] (result)  in
                                    switch result{
                                    case .success( _):
                                        DispatchQueue.main.async {
                                            showCustomAlert(title: "Succusfully Changed", message: "Your Password has been changed")
                                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                            let vc = storyboard.instantiateViewController(withIdentifier: "SettingssTableViewController") as! SettingssTableViewController
                                            self.navigationController?.pushViewController(vc, animated: false)
                                            doneBtn.isEnabled = true
                                            
                                        }
                                        break
                                    case .failure(let error):
                                        DispatchQueue.main.async {
                                            showCustomAlert(title: "Email Failed", message: error.title!)
                                            doneBtn.isEnabled = true
                                        }
                                    }
                                }
                            }
                            else{
                                showCustomAlert(title: "Wrong pattern ", message: "Enter correct pattern")
                            }
                        }
                }else{
                    showCustomAlert(title: "Internet Not Connected", message: "Connection Error")
                    doneBtn.isEnabled = true
                }
            }
        }
        else{
            showCustomAlert(title: "Missing Parameters", message: "Please enter required Fields")
            doneBtn.isEnabled = true
        }
    }
    @IBAction func newPassHide(_ sender: Any) {
        if(newBool){
            newPassTextField.isSecureTextEntry = false
            newBool = false
            
            let tintedImage1 = invisibleImage?.withRenderingMode(.alwaysTemplate)
            newPassEye.setImage(tintedImage1, for: .normal)
            newPassEye.tintColor = .darkGray
            
        }else{
            newBool = true
            newPassTextField.isSecureTextEntry = true
            newPassEye.setImage(visibleImage, for: .normal)
            
            let tintedImage1 = visibleImage?.withRenderingMode(.alwaysTemplate)
            newPassEye.setImage(tintedImage1, for: .normal)
            newPassEye.tintColor = .darkGray
        }
    }
    @IBAction func confirmPassHide(_ sender: Any) {
        if(confirmBool){
            confirmPassTextField.isSecureTextEntry = false
            confirmBool = false
    
            let tintedImage1 = invisibleImage?.withRenderingMode(.alwaysTemplate)
            confirmPassEye.setImage(tintedImage1, for: .normal)
            confirmPassEye.tintColor = .darkGray
        }else{
            confirmBool = true
            confirmPassTextField.isSecureTextEntry = true
            let tintedImage1 = visibleImage?.withRenderingMode(.alwaysTemplate)
            confirmPassEye.setImage(tintedImage1, for: .normal)
            confirmPassEye.tintColor = .darkGray
        }
    }
    func matchPassword() -> Bool{
        if newPassTextField.text == confirmPassTextField.text{
            doneBtn.isEnabled = true
            return true
            
        }else{
            newPassError.text = "Password doesn't match"
            newPassError.isHidden = false
            doneBtn.isEnabled = true
            return false
        }
    }
    func isEmptyField() {
        if currentPassTextField.text?.count == 0{
            currentPassError.isHidden = false
        }
        if newPassTextField.text?.count == 0{
            newPassError.text = "Field Required*"
            newPassError.isHidden = false
        }
        if confirmPassTextField.text?.count == 0{
            confirmPassError.isHidden = false
        }
    }
    func passIsSame(){
        let currentPass = currentPassTextField.text
        let newPass = newPassTextField.text
        
        if(currentPass == newPass){
            showCustomAlert(title: "Password Update Failed", message: "New Password must be different")
        }
    }
    func showCustomAlert(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}
