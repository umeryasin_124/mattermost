//
//  PickerController.swift
//  IOS Chat App Layout
//
//  Created by Muhammad Huzaifa on 02/11/2020.
//

import UIKit

class SelectCompany: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate {
    
    
    @IBOutlet weak var nextBtn: UIBarButtonItem!
    
    @IBOutlet weak var helloUser: UILabel!
    @IBOutlet weak var myPicker: UIPickerView!
    var companyIds: [Int] = [Int]()
    var list: [String]!
    @IBOutlet weak var selectCompanyView: UIToolbar!
    @IBOutlet weak var selectCompany: UIBarButtonItem!
    @IBOutlet weak var moveNext: UIButton!
    var pickerView: UIPickerView!
    @IBOutlet weak var textPicker: UITextField!
    var apiResponse:[String:Any]!
    var email: String!
    var userData:UserCompleteInfo!
    override func viewDidLoad() {
        super.viewDidLoad()
        populateCompanyPicker()
        textPicker.delegate = self
        textPicker.tintColor = .clear
        pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        selectCompany.tintColor = .black
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 5/255, green: 49/255, blue: 106/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.done, target: self, action: #selector(cancelPicker))
        toolBar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.textPicker.inputView = pickerView
        self.textPicker.inputAccessoryView = toolBar
        self.moveNext.isEnabled = false
        if(!self.moveNext.isEnabled)
        {
            self.moveNext.setTitleColor(.gray, for: .normal)
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        selectCompanyView.addGestureRecognizer(tap)
    }
    func saveUserDataInCoreData(apiResponse:[String:Any],completion: @escaping (UserCompleteInfo) -> Void){
        do{
            let data = try JSONSerialization.data(withJSONObject: apiResponse, options: [])
            let jsonDecoder = JSONDecoder()
            let userData = try jsonDecoder.decode(UserCompleteInfo.self, from: data)
            completion(userData)
        }catch _{
            completion(UserCompleteInfo())
        }
    }
    func populateCompanyPicker(){
        let data = apiResponse["data"] as? [String:Any]
        let companyList = data!["companies"] as? [[String:Any]]
        let username = data!["username"] as? String
        
        helloUser.text = "Hello  \(username!)"
        list = [String]()
        for i in 0 ..< companyList!.count{
            companyIds.append(companyList![i]["id"] as! Int)
            list.append(companyList![i]["name"] as! String)
        }
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if(textField.text!.count > 0){
            textField.text = ""
        }
    }
    @objc func donePicker() {
        self.selectCompany.title = self.list[pickerView.selectedRow(inComponent: 0)]
        textPicker.resignFirstResponder()
        self.pickerView.isHidden = false
        self.nextBtn.isEnabled = true
        self.moveNext.isEnabled = true
        if(self.moveNext.isEnabled == true){
            self.moveNext.setTitleColor(.white, for: .normal)
        }
    }
    @objc func cancelPicker() {
        
        self.textPicker.text = nil
        self.textPicker.resignFirstResponder()
        
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.pickerView.isHidden = false
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return list.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return  self.list[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.nextBtn.isEnabled = true
        self.moveNext.isEnabled = true
        if(self.moveNext.isEnabled)
        {
            self.moveNext.setTitleColor(.gray, for: .normal)
        }
        self.moveNext.isEnabled = false
        
    }
    @IBAction func openPicker(_ sender: Any) {
        pickerView.isHidden = false
    }
    @IBAction func nextBtn(_ sender: Any) {
        if(self.selectCompany.title == "Select Company"){
            self.pickerView.isHidden = false
            self.moveNext.isEnabled = false
            if(self.moveNext.isEnabled == false)
            {
                self.moveNext.setTitleColor(.gray, for: .normal)
            }
        }
        else
        {
            if(self.pickerView.isHidden){
                self.pickerView.isHidden = false
                moveNext.isEnabled = false
                if(self.moveNext.isEnabled == false)
                {
                    self.moveNext.setTitleColor(.gray, for: .normal)
                }
            }else{
                self.pickerView.isHidden = true
                nextBtn.isEnabled = true
                moveNext.isEnabled = true
                if(self.moveNext.isEnabled == true)
                {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "HomeScreenNavigation")
                    self.present(vc, animated: true, completion: nil)
                    self.moveNext.setTitleColor(.white, for: .normal)
                }
            }
        }
    }
    @IBAction func moveNext(_ sender: Any) {
        saveUserDataInCoreData(apiResponse: apiResponse) { (userdetails) in
            self.userData = userdetails
            self.performSegue(withIdentifier: "HomeScreenNavigation", sender: self)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let navigationContoller = segue.destination as! UINavigationController
        let receiverViewController = navigationContoller.topViewController as! HomeScreen
        receiverViewController.completeInfo = userData
        receiverViewController.selectedCompanyId = companyIds[self.pickerView.selectedRow(inComponent: 0)]
        receiverViewController.apiResponse = apiResponse
        receiverViewController.selectedCompanyId = companyIds[self.pickerView.selectedRow(inComponent: 0)]
    }
}
