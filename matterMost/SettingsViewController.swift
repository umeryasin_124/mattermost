//
//  SettingsViewController.swift
//  matterMost
//
//  Created by Umer yasin on 02/11/2020.
//

import UIKit

struct cellData{
    var open = Bool()
    var title = String()
    var image = String()
    var sectionData = [String]()
}
class SettingsViewController: UIViewController , UITableViewDelegate, UITableViewDataSource, MainImageSelected, MainUserNAme{
    
    @IBOutlet weak var userNameShow: UILabel!
    @IBOutlet weak var MianImage: UIImageView!
    static var count:Int = 0
    var tabeleViewData = [cellData]()
    var generalView: UIView!
    var profileUrl: String = ""
    
    var apiManager: APIManager!
    
    @IBOutlet weak var tableView: UITableView!
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tabeleViewData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tabeleViewData[section].open == true{
            return 2
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell" , for: indexPath)
            cell.textLabel!.text = tabeleViewData[indexPath.section].title
            cell.textLabel?.font = .boldSystemFont(ofSize: 18)
            cell.imageView?.image = UIImage(named: tabeleViewData[indexPath.section].image)
            
            return cell
        }else{
            if(indexPath.section == 0){
                let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell" , for: indexPath) as! TableViewCell
                cell.uiviewcontroller = self
                cell.MainImageSelectedDelegate = self
                cell.mainuserNameDelegate = self
                tableView.rowHeight = 130
                return cell
            }
            else if(indexPath.section == 1){
                let cell = tableView.dequeueReusableCell(withIdentifier: "PasswordTableViewCell" , for: indexPath) as! PasswordTableViewCell
                tableView.rowHeight = 250
                cell.uiviewcontroller = self
                
                return cell
            }
            else if(indexPath.section == 2){
                let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell" , for: indexPath) as! NotificationTableViewCell
                tableView.rowHeight = 55
                cell.uiviewcontroller = self
                return cell
            }else{
                return TableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(!tabeleViewData[indexPath.section].open){
            for (index,_) in tabeleViewData.enumerated() {
                if tabeleViewData[index].open{
                    tableView.rowHeight = 50
                    tabeleViewData[index].open = false
                    let section = IndexSet.init(integer: index)
                    tableView.reloadSections(section, with: .none)
                }
            }
        }
        if tabeleViewData[indexPath.section].open == true {
            tableView.rowHeight = 50
            tabeleViewData[indexPath.section].open = false
            let section = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(section, with: .none)
        }else{
            tableView.rowHeight = 50
            tabeleViewData[indexPath.section].open = true
            let section = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(section, with: .none)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiManager = APIManager()
        print("Heloooooooooo")
        let nib = UINib(nibName: "TableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "TableViewCell")
        let nibp = UINib(nibName: "PasswordTableViewCell", bundle: nil)
        tableView.register(nibp, forCellReuseIdentifier: "PasswordTableViewCell")
        let nibn = UINib(nibName: "NotificationTableViewCell", bundle: nil)
        tableView.register(nibn, forCellReuseIdentifier: "NotificationTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        tabeleViewData = [cellData(open: false, title: "General",image: "setto", sectionData: []),
                          cellData(open: false, title: "Security",image: "privacy", sectionData: []),
                          cellData(open: false, title: "Notifications",image: "notifications", sectionData: [])]
        
        profileUrl = UserDefaults.standard.string(forKey: "userProfilePic")!
    }
    func showCustomAlert(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    func updateImage(iamge: UIImage) {
        MianImage.image = iamge
        MianImage.contentMode = .scaleAspectFit
    }
    
    func updaterUserNmae(useranme: String) {
        userNameShow.text = useranme
    }
}
