//
//  SettingssTableViewController.swift
//  matterMost
//
//  Created by Umer yasin on 10/11/2020.
//

import UIKit

class SettingssTableViewController: UITableViewController {
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var userName_Lbl: UILabel!
    @IBOutlet weak var email_Lbl: UILabel!
    @IBOutlet weak var settingimg: UIImageView!
    @IBOutlet weak var privacy: UIImageView!
    @IBOutlet weak var notifications: UIImageView!
    
    var apiManager: APIManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.perform(#selector(changeImageColor), with: nil, afterDelay: 0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        apiManager = APIManager()
        let  userName = UserDefaults.standard.string(forKey: "userName")!
        let  profileUrl = UserDefaults.standard.data(forKey: "userProfilePic")
        let  userEmail = UserDefaults.standard.string(forKey: "email")!
        
        userName_Lbl.text = userName
        email_Lbl.text = userEmail
        if !(profileUrl == nil){
            self.profilePic.image = UIImage(data: profileUrl!)
        }
    }
    func apiCallDownloadImage(profileUrl: String){
        let token = UserDefaults.standard.string(forKey: "token")!
        let  profileUrl = UserDefaults.standard.string(forKey: "userProfilePic")!
        
        apiManager.downloadFileToServer(url: profileUrl, httpMethod: "Post", token: token) { [self] (result)  in
            switch result{
            case .success(let response):
                DispatchQueue.main.async {
                    self.profilePic.image = UIImage(data: response)
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    showCustomAlert(title: "Email Failed", message: error.title!)
                }
            }
        }
    }
    func showCustomAlert(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func changeImageColor(){
        let templateImage = self.settingimg.image!.withRenderingMode(.alwaysTemplate)
        self.settingimg.image = templateImage
        self.settingimg.tintColor = .darkGray
        
        let templateImage1 = self.privacy.image!.withRenderingMode(.alwaysTemplate)
        self.privacy.image = templateImage1
        self.privacy.tintColor = .darkGray
        
        let templateImage2 = self.notifications.image!.withRenderingMode(.alwaysTemplate)
        self.notifications.image = templateImage2
        self.notifications.tintColor = .darkGray
    }
}
