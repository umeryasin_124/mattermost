    import UIKit
import IHKeyboardAvoiding
class SignIn: UIViewController,UITextFieldDelegate{
    
    @IBOutlet weak var email: UIButton!
    @IBOutlet weak var activityLoaderr: UIActivityIndicatorView!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailErrorField: UILabel!
    @IBOutlet weak var passwordErrorField: UILabel!
    @IBOutlet weak var eyeButton: UIButton!
    
    var apiManager = APIManager()
    
    var secureTextEntry: Bool!
    let visibleImage = UIImage(named: "eye")
    let invisibleImage = UIImage(named: "eyeGone")
    let invisibleImage1 = UIImage(named: "email")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityLoaderr.isHidden = true
        apiManager = APIManager()
        self.navigationController?.navigationBar.isHidden = true
        self.hideKeyboardWhenTappedAround()
        secureTextEntry = true
        activityLoaderr.isHidden = true
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    override func viewDidAppear(_ animated: Bool) {
        let tintedImage = visibleImage?.withRenderingMode(.alwaysTemplate)
        eyeButton.setImage(tintedImage, for: .normal)
        eyeButton.tintColor = .darkGray
        
        let tintedImage1 = invisibleImage1?.withRenderingMode(.alwaysTemplate)
       email.setImage(tintedImage1, for: .normal)
        email.tintColor = .darkGray
    }
    @IBAction func passwordTxt(_ sender: UITextField) {
        KeyboardAvoiding.avoidingView = self.loginBtn
    }
    func addTapGestures(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(closeKeyboard(_:)))
        view.addGestureRecognizer(tap)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    @IBAction func hideShowPassword(_ sender: Any) {
        if(secureTextEntry){
            passwordTextField.isSecureTextEntry = false
            secureTextEntry = false
            let tintedImage = invisibleImage?.withRenderingMode(.alwaysTemplate)
            eyeButton.setImage(tintedImage, for: .normal)
            eyeButton.tintColor = .darkGray
        }else{
            passwordTextField.isSecureTextEntry = true
            secureTextEntry = true
            let tintedImage = visibleImage?.withRenderingMode(.alwaysTemplate)
            eyeButton.setImage(tintedImage, for: .normal)
            eyeButton.tintColor = .darkGray
        }
    }
    @IBAction func login(_ sender: Any) {
        if(!emailTextField.text!.isEmpty && !passwordTextField.text!.isEmpty){
            if(!Validator.isValidEmail(emailTextField.text!)){
                emailErrorField.text = "Email Not Valid*"
                emailErrorField.isHidden = false
            }else{
                if ConnectivityService().isConnectedToNetwork() {
                    loginBtn.isUserInteractionEnabled = false
                    activityLoaderr.isHidden = false
                    activityLoaderr.startAnimating()
                    apiManager.login(email: emailTextField.text!, password: passwordTextField.text!) { [self] (result) in
                        
                        switch result{
                        case .success(let response):
                            if(response["admin"] as! Bool){
                                showCustomAlert(title: "Admin", message: "Please refer to dashboard")
                            }else{
                                if(response["account_setup"] as! Bool){
                                    DispatchQueue.main.async {
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = storyboard.instantiateViewController(withIdentifier: "firsttimeloginViewController") as! firsttimeloginViewController
                                        vc.token = response["token"] as? String
                                        UserDefaults.standard.set(response["token"] as? String, forKey: "token")
                                        let dataU = response["data"] as! [String: Any]
                                        UserDefaults.standard.set(dataU["email"] as? String, forKey: "email")
                                        activityLoaderr.stopAnimating()
                                        activityLoaderr.isHidden = true
                                        loginBtn.isUserInteractionEnabled = true
                                        self.present(vc, animated: true, completion: nil)
                                    }
                                }else{
                                    DispatchQueue.main.async {
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = storyboard.instantiateViewController(withIdentifier: "SelectCompany") as! SelectCompany
                                        
                                        vc.apiResponse = response
                                        vc.email = emailTextField.text
                                        UserDefaults.standard.set(response["token"] as? String, forKey: "token")
                                        let dataU = response["data"] as? [String: Any]
                                        
                                        UserDefaults.standard.set(dataU!["id"] as? Int, forKey: "userId")
                                        UserDefaults.standard.set(dataU!["username"] as? String, forKey: "userName")
                                        UserDefaults.standard.set(dataU!["email"] as? String, forKey: "email")
                                        let profileUrl = dataU!["profile_picture"] as? String
                                        
                                        if profileUrl == nil{
                                            UserDefaults.standard.set(nil, forKey: "userProfilePic")
                                            activityLoaderr.stopAnimating()
                                            activityLoaderr.isHidden = true
                                            loginBtn.isUserInteractionEnabled = true
                                            self.present(vc, animated: true, completion: nil)
                                            
                                        }else{
                                            apiCallDownloadImage(profileUrl: profileUrl!)
                                            activityLoaderr.stopAnimating()
                                            activityLoaderr.isHidden = true
                                            loginBtn.isUserInteractionEnabled = true
                                            self.present(vc, animated: true, completion: nil)
                                        }
                                    }
                                }
                            }
                            break
                        case .failure(let error):
                            DispatchQueue.main.async {
                                showCustomAlert(title: "Login UnSuccessful", message: error.title!)
                                activityLoaderr.stopAnimating()
                                activityLoaderr.isHidden = true
                                loginBtn.isUserInteractionEnabled = true
                            }
                        }
                    }
                }else{
                    showCustomAlert(title: "Login Failed", message: "No Internet Connection")
                }
                emailErrorField.isHidden = true
            }
        }else{
            if(emailTextField.text!.isEmpty){
                emailErrorField.isHidden = false
            }
            if(passwordTextField.text!.isEmpty){
                passwordErrorField.isHidden = false
            }
        }
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if(textField.tag == 0){
            if(!emailTextField.text!.isEmpty){
                emailErrorField.isHidden = true
            }else{
                emailErrorField.isHidden = false
            }
        }else{
            if(!passwordTextField.text!.isEmpty){
                passwordErrorField.isHidden = true
            }else{
                passwordErrorField.isHidden = false
            }
        }
    }
    @objc func closeKeyboard(_: Any){
        view.endEditing(true)
    }

    func showCustomAlert(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func apiCallDownloadImage(profileUrl: String){
        let token = UserDefaults.standard.string(forKey: "token")!
        apiManager.downloadFileToServer(url: profileUrl, httpMethod: "Post", token: token) { [self] (result)  in
            switch result{
            case .success(let response):
                DispatchQueue.main.async {
                    UserDefaults.standard.set(response as Data, forKey: "userProfilePic")
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    showCustomAlert(title: "Email Failed", message: error.title!)
                }
            }
        }
    }
}

enum LINE_POSITION {
    case LINE_POSITION_TOP
    case LINE_POSITION_BOTTOM
}
extension UIView {
    func addLine(position : LINE_POSITION, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false 
        self.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .LINE_POSITION_TOP:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        }
    }
}
