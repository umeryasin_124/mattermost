//
//  TableViewCell.swift
//  matterMost
//
//  Created by Umer yasin on 02/11/2020.
//

import UIKit
import AVKit


protocol MainImageSelected {
    func updateImage(iamge: UIImage)
}
protocol MainUserNAme {
    func updaterUserNmae(useranme: String)
}
class TableViewCell: UITableViewCell{

    
    @IBOutlet weak var imageEdit: UIButton!
    @IBOutlet weak var imageCanel: UIButton!
    @IBOutlet weak var imageTick: UIButton!
    var ImageFormGalleryorCamera : UIImage!
    var orignalImage:UIImage!
    
    var MainImageSelectedDelegate:MainImageSelected!
    var mainuserNameDelegate:MainUserNAme!

    
    var uiviewcontroller = UIViewController()
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userNametick: UIButton!
    @IBOutlet weak var editUserName: UIButton!
    @IBOutlet weak var crossInsideTextBox: UIButton!
    var orignalName:String!    
    
    @IBOutlet weak var profileImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userName.text = "Umer Yasin"
        userName.isUserInteractionEnabled = false
        userNametick.isHidden = true
        crossInsideTextBox.isHidden = true
        orignalName = userName.text!
        imageCanel.isHidden = true
        imageTick.isHidden = true
        orignalImage = UIImage(named: "profile")!
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onclickEditUsername(_ sender: Any) {
        userName.isUserInteractionEnabled = true
        userNametick.isHidden = false
        editUserName.isHidden = true
        crossInsideTextBox.isHidden = false

    }
    @IBAction func onClickusenameTick(_ sender: Any) {
        
        userNametick.isHidden = true
        crossInsideTextBox.isHidden = true
        editUserName.isHidden = false
        userName.isUserInteractionEnabled = false
        orignalName = userName.text!
        mainuserNameDelegate.updaterUserNmae(useranme: orignalName)
    }
    
    @IBAction func onclickInsidecrossBox(_ sender: Any) {
        userName.text = orignalName
        userNametick.isHidden = true
        crossInsideTextBox.isHidden = true
        editUserName.isHidden = false
        userName.isUserInteractionEnabled = false
    }
    
    
    @IBAction func onclickEditProfileImage(_ sender: Any) {
        
        imageEdit.isHidden = true
        imageTick.isHidden = false
        imageCanel.isHidden = false
        
        let actionsheet = UIAlertController(title: "Action Needed", message: "Choose your best option to upload profile image", preferredStyle: .actionSheet)
        
        let libButton = UIAlertAction(title: "Select Image From Library", style: .default) { (libSelected) in
            
            let imagepicker = UIImagePickerController()
            imagepicker.delegate = self
            imagepicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            imagepicker.allowsEditing = true
            self.uiviewcontroller.present(imagepicker, animated: true, completion: nil)
        }
        
        let CameraButton = UIAlertAction(title: "Capture Image From Camera", style: .default) { (camSelected) in
            
           if( UIImagePickerController.isSourceTypeAvailable(.camera)){
                
                let imagepicker = UIImagePickerController()
                imagepicker.delegate = self
                imagepicker.sourceType = UIImagePickerController.SourceType.camera
                imagepicker.allowsEditing = true
                self.uiviewcontroller.present(imagepicker, animated: true, completion: nil)
           }else{
            let alert = UIAlertController(title: "UnExpected Behaviour", message: "Camera Not Avaliable", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok",
                                          style: UIAlertAction.Style.default,
                                          handler: { [self](_: UIAlertAction!) in
                                                    //canel out action
                                                    self.self.profileImage.image = orignalImage
                                                    self.imageEdit.isHidden = false
                                                    self.imageTick.isHidden = true
                                                    self.imageCanel.isHidden = true
                                                    
                    }))
            self.uiviewcontroller.present(alert, animated: true, completion: nil)
           }
            
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel) { [self] (cancelSeleted) in
            
            self.self.profileImage.image = orignalImage
            self.imageEdit.isHidden = false
            self.imageTick.isHidden = true
            self.imageCanel.isHidden = true
            print("cancel Selected")
        }
        actionsheet.addAction(libButton)
        actionsheet.addAction(CameraButton)
        actionsheet.addAction(cancelButton)
        uiviewcontroller.present(actionsheet, animated: true, completion: nil)
        
    }
    
    @IBAction func imageTick(_ sender: Any) {
        
        MainImageSelectedDelegate.updateImage(iamge: ImageFormGalleryorCamera)
        imageEdit.isHidden = false
        imageTick.isHidden = true
        imageCanel.isHidden = true

    }
    
    @IBAction func ImageCross(_ sender: Any) {
        
        profileImage.image = orignalImage
        imageEdit.isHidden = false
        imageTick.isHidden = true
        imageCanel.isHidden = true
    }
}
extension TableViewCell : UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if let image_data = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as? UIImage {
            let imageData:Data = image_data.jpegData(compressionQuality: 1)!
            self.uiviewcontroller.dismiss(animated: true, completion: nil)
            if let finalImage = UIImage(data: imageData){
                profileImage.image = finalImage
                profileImage.contentMode = .scaleAspectFit
                ImageFormGalleryorCamera = finalImage
            }
        }
    }
}
