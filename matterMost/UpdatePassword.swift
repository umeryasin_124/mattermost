//
//  ViewController.swift
//  matterMost
//
//  Created by Umer yasin on 27/10/2020.
//

import UIKit
import IHKeyboardAvoiding

class UpdatePassword: UIViewController,UITextFieldDelegate {
    
    var newpassbool = true
    var conpassbool = true
    var email: String = "" //Recieving from OTP Screen
    var pass: String!
    
    var dataDictionary = [String: String]() // Intialising Dictonary
    var apiManger: APIManager!
    
    let visibleImage = UIImage(named: "eye") // ImageView For showing password
    let invisibleImage = UIImage(named: "eyeGone")
    
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var confirmpasseye: UIButton!
    @IBOutlet weak var newpasseye: UIButton!
    @IBOutlet weak var newPasswordError: UILabel!
    @IBOutlet weak var confirmPassError: UILabel!
    @IBOutlet weak var changePassBtn: UIButton!
    @IBOutlet weak var confirmPass: UITextField!
    @IBOutlet weak var newPass: UITextField!
    @IBOutlet weak var activityLoaderr: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title:"SignIn", style: UIBarButtonItem.Style.plain, target: self, action: #selector(UpdatePassword.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        activityLoaderr.isHidden = true
        self.hideKeyboardWhenTappedAround()
        self.navigationController?.navigationBar.isHidden = true
        updateIconColor()
        apiManger = APIManager()
        
        KeyboardAvoiding.avoidingView = self.subView
        confirmPass.addLine(position: .LINE_POSITION_BOTTOM, color: .gray, width: 1) //Adding line below Confirm Pass Field
        newPass.addLine(position: .LINE_POSITION_BOTTOM, color: .gray, width: 1) //Adding line below new Pass Field
        
        setDelegates() //Setting All delegates
    }
 //Setting showPass Image on Appear
    func updateIconColor() {
        
        let tintedImage = visibleImage?.withRenderingMode(.alwaysTemplate)
        newpasseye.setImage(tintedImage, for: .normal)
        newpasseye.tintColor = .darkGray
        
        let tintedImage1 = visibleImage?.withRenderingMode(.alwaysTemplate)
        confirmpasseye.setImage(tintedImage1, for: .normal)
        confirmpasseye.tintColor = .darkGray
    }
    
    @objc func back(sender: UIBarButtonItem) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //Password changes on server
    @IBAction func changePassword(_ sender: Any) {
        if(!confirmPass.text!.isEmpty && !newPass.text!.isEmpty){
            if(Validator.isValidPassword(newPass.text!)){
                if(newPass.text == confirmPass.text){
                    newPasswordError.isHidden = true
                    confirmPassError.isHidden = true
                    
                    if ConnectivityService().isConnectedToNetwork() {
                        activityLoaderr.isHidden = false
                        activityLoaderr.startAnimating()
                        pass = newPass.text
                        setPassApiCall()
                    }else{
                        activityLoaderr.stopAnimating()
                        activityLoaderr.isHidden = true
                        showCustomAlert(title: "", message: "No Internet Connection")}
                    
                }else{
                    confirmPassError.text = "Password Not Matched"
                    confirmPassError.isHidden = false
                }
            }else{
                newPasswordError.isHidden = false
                newPasswordError.text = "6 Digits, Upper case, Lower case, Number, Special Character"
            }
        }else{
            if(confirmPass.text!.isEmpty){
                confirmPassError.isHidden = false
            }
            if(newPass.text!.isEmpty){
                newPasswordError.isHidden = false
            }
        }
    }
    
    //Toogle ShowPassowrd eye Image
    @IBAction func newpassEye(_ sender: Any) {
        if(newpassbool){
            newPass.isSecureTextEntry = false
            newpassbool = false
            newpasseye.setImage(invisibleImage, for: .normal)
            
            let tintedImage1 = invisibleImage?.withRenderingMode(.alwaysTemplate)
            newpasseye.setImage(tintedImage1, for: .normal)
            newpasseye.tintColor = .darkGray
            
        }else{
            newpassbool = true
            newPass.isSecureTextEntry = true
            
            let tintedImage1 = visibleImage?.withRenderingMode(.alwaysTemplate)
            newpasseye.setImage(tintedImage1, for: .normal)
            newpasseye.tintColor = .darkGray
        }
    }
    
    @IBAction func confirmpasseye(_ sender: Any) {
        if(conpassbool){
            confirmPass.isSecureTextEntry = false
            conpassbool = false
            let tintedImage1 = invisibleImage?.withRenderingMode(.alwaysTemplate)
            confirmpasseye.setImage(tintedImage1, for: .normal)
            confirmpasseye.tintColor = .darkGray
        }else{
            conpassbool = true
            confirmPass.isSecureTextEntry = true
            
            let tintedImage1 = visibleImage?.withRenderingMode(.alwaysTemplate)
            confirmpasseye.setImage(tintedImage1, for: .normal)
            confirmpasseye.tintColor = .darkGray
        }
    }
    //Delegate Function of textField
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if(textField.tag == 0){
            if(!textField.text!.isEmpty){
                newPasswordError.isHidden = true
            }else{
                newPasswordError.isHidden = false
            }
        }else{
            if(!textField.text!.isEmpty){
                confirmPassError.isHidden = true
            }else{
                confirmPassError.isHidden = false
            }
        }
    }
    //Setting TextField Delegates
    func setDelegates(){
        newPass.delegate = self
        confirmPass.delegate = self
    }
    
    //Api Method to setPassword
    func setPassApiCall(){
        dataDictionary = ["email": email, "password": pass] //Adding Data to Dictonary
        
        let url = ENDPOINTS.newPassword
        apiManger.apiCaller(url: url, sendBodyData: dataDictionary, httpMethod: "POST"){[self] (result) in
            //Switch Check of success and failure
            switch result{
            case .success(let response):
                DispatchQueue.main.async {
                    activityLoaderr.stopAnimating()
                    activityLoaderr.isHidden = true
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let signinController = storyboard.instantiateViewController(withIdentifier: "SignIn") //Naviagting to SignIn Screen
                    signinController.modalPresentationStyle = .fullScreen
                    self.navigationController?.navigationBar.tintColor = .white
                    self.navigationController?.pushViewController(signinController, animated: true)
                    showCustomAlert(title: "Password Updated", message: response["status_message"] as! String)
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    activityLoaderr.stopAnimating()
                    activityLoaderr.isHidden = true
                    showCustomAlert(title: "Error", message: error.title!)
                }
            }
        }
    }
    
    //Custom Alert Dialog Box
    func showCustomAlert(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}
