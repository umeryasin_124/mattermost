//
//  UserCell.swift
//  ChatApp
//
//  Created by abdullah Javed on 02/11/2020.
//

import UIKit

class UserCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var joinButton: UIButton!
    var parentViewController:UIViewController!
    var isPublic = true
    var isJoined = false
    var channels: [PublicChannel]!
    var userID,channelID,teamID,companyID: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func joinChannel(_ sender: Any) {
        let alertController = UIAlertController(title: "Join Channel", message: "Do you want to join the channel?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Join", style: .default, handler: { [self] (action) in
            
            var dict = [String:Any]()
            dict["user_id"] = [userID]
            dict["channel_id"] = channelID!
            dict["team_id"] = teamID!
            dict["company_id"] = companyID!
            let token = UserDefaults.standard.string(forKey: "token")
            APIManager().apiCaller(url: ENDPOINTS.insertUserInChannel, sendBodyData: dict, httpMethod: "POST",token: token!) { (result) in
                
                switch (result){
                    case .success(let response):
                        DispatchQueue.main.async {
                            showCustomAlert(title: "Channel Joined", message: response["status_message"] as! String)
                            joinButton.isHidden  = true
                        }
                    break
                    case .failure(let error):
                        DispatchQueue.main.async {
                            showCustomAlert(title: "Unable To Join", message: error.title!)
                        }
                        break
                }}
            
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        parentViewController.present(alertController, animated: true, completion: nil)
    }
    func showCustomAlert(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        parentViewController.present(alertController, animated: true, completion: nil)
    }
}
