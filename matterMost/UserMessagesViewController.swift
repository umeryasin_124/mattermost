//
//  UserMessagesViewController.swift
//  matterMost
//
//  Created by Muhammad Huzaifa on 12/11/2020.
//

import UIKit

class UserMessagesViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var userList = [String]()
    var searchCountry = [String]()
    var searching = false
    var selectedCompanyId:Int!
    var selectedTeamID:Int!
    var userCompleteInfo:UserCompleteInfo!
    var teamID:Int!
    var ids = [Int]()
    var users = [Users]()
    var originalIds = [Int]()
    var filteredUsers = [Users]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setTableView()
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.delegate = self
        tableView.dataSource = self
        users = getTeamUsers()
        
        for i in 0 ..< users.count{
            if(!(users[i].username == nil)){
                ids.append(users[i].id)
                userList.append(users[i].username!)
            }
        }
        originalIds = ids
        tableView.reloadData()
    }
    private func setTableView(){
        let nib = UINib(nibName: "UserMessagesController", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "cellIdentifierMessages")
    }
    func getTeamUsers() -> [Users]{
        for i in 0 ..< userCompleteInfo.data.companies.count{
            if userCompleteInfo.data.companies[i].id == selectedCompanyId!{
                if(userCompleteInfo.data.companies[i].teams.count > 0){
                    teamID = userCompleteInfo.data.companies[i].teams[selectedTeamID].id
                    return userCompleteInfo.data.companies[i].teams[selectedTeamID].users
                }
            }
        }
        return [Users]()
    }
}
extension UserMessagesViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching{
            return searchCountry.count
        }
        else{
            return userList.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifierMessages", for: indexPath) as! UserMessagesController
        if (searching)
        {
            cell.userLabel.text = searchCountry[indexPath.row]
        }
        else{
            cell.userLabel.text = userList[indexPath.row]
        }
        return cell
    }
}
extension UserMessagesViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.frame.height / 13
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let settingsController = storyboard.instantiateViewController(withIdentifier: "ChatScreen") as! ChatScreen
        settingsController.modalPresentationStyle = .fullScreen
        if(searching){
            settingsController.userName = searchCountry[indexPath.row]
        }else{
            settingsController.userName = userList[indexPath.row]
        }
        settingsController.idT = ids[indexPath.row]
        settingsController.company_id = selectedCompanyId
        settingsController.idType = "direct"
        settingsController.teamID = teamID
        settingsController.completeinfo = userCompleteInfo
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.pushViewController(settingsController, animated: true)
        print("Did select Row At\(indexPath)")
        
    }
}

extension UserMessagesViewController : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if(!searchText.isEmpty){
            filteredUsers.removeAll()
            ids.removeAll()
            print(users)
            filteredUsers = getFilteredUsers(search: searchText)
            searchCountry = userList.filter({$0.lowercased().contains(searchText.lowercased())})
            print(searchCountry.count)
            for i in 0 ..< filteredUsers.count{
                ids.append(filteredUsers[i].id)
            }
            searching = true
            tableView.reloadData()
        }else{
            searching = false
            tableView.reloadData()
        }
    }
    func getFilteredUsers(search:String) -> [Users]{
        for i in 0 ..< users.count{
            if(!(users[i].username == nil)){
                if users[i].username!.lowercased().contains(search.lowercased()) {
                    filteredUsers.append(users[i])
                }
            }
        }
        return filteredUsers
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        ids = originalIds
        searching = false
        searchBar.text = ""
        tableView.reloadData()
        view.endEditing(true)
    }
}
