//
//  VideoMessageCell.swift
//  matterMost
//
//  Created by ethisham bader on 08/11/2020.
//

import UIKit
import AVKit
import AVFoundation

class VideoMessageCell: UITableViewCell {
    
    @IBOutlet weak var uname: UILabel!
    var parentViewController:UIViewController!
    var videoURL: String?
    @IBOutlet weak var timeStampVideo: UILabel!
    
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var imageThumbnail: UIImageView!
    @IBOutlet weak var videoPreviewView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//      /  videoView.layer.backgroundColor = UIColor(red: 5/255, green: 49/255, blue: 106/255, alpha: 1.0).cgColor
        timeStampVideo.text = DateFormatter.localizedString(from: Date(), dateStyle: .none, timeStyle: .short)


    }
    @IBAction func videoOpenButton(_ sender: Any) {
        videoLibrary()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func getDirectory() -> URL {
        
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = path[0]
        print(documentDirectory)
        return documentDirectory
    }
    func videoLibrary()
    {
        let filename = getDirectory().appendingPathComponent("output.mp4")
        let path = Data(base64Encoded: videoURL!)
        try? path!.write(to: filename, options: .atomicWrite)
        
        let player = AVPlayer(url: filename)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        
        parentViewController.present(playerViewController, animated: true){
            playerViewController.player!.play()
        }
        
        //        do {
        //            let asset = AVURLAsset(url: videoURL! as URL , options: nil)
        //                let imgGenerator = AVAssetImageGenerator(asset: asset)
        //                imgGenerator.appliesPreferredTrackTransform = true
        //            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
        //                let thumbnail = UIImage(cgImage: cgImage)
        //                imageThumbnail.image = thumbnail
        //            } catch let error {
        //                print("*** Error generating thumbnail: \(error.localizedDescription)")
        //            }
    }
    
}
