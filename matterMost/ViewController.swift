//
//  ViewController.swift
//  matterMost
//
//  Created by Umer yasin on 27/10/2020.
//

import UIKit
import TPKeyboardAvoiding

enum LINE_POSITION {
    case LINE_POSITION_TOP
    case LINE_POSITION_BOTTOM
}

@available(iOS 13.0, *)
class ViewController: UIViewController {

    var newpassbool = false
    var conpassbool = false
    
    @IBOutlet weak var newpasseye: UIButton!
    
    @IBOutlet weak var confirmPass: UITextField!
    @IBOutlet weak var newPass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        confirmPass.addLine(position: .LINE_POSITION_BOTTOM, color: .gray, width: 1)
        newPass.addLine(position: .LINE_POSITION_BOTTOM, color: .gray, width: 1)

        // Do any additional setup after loading the view.
    }

    @IBAction func newpassEye(_ sender: Any) {
        if(newpassbool){
            newPass.isSecureTextEntry = false
            newpassbool = false
        }else{
            newpassbool = true
            newPass.isSecureTextEntry = true

        }
    }
    
    @IBAction func confirmpasseye(_ sender: Any) {
        if(conpassbool){
            confirmPass.isSecureTextEntry = false
            conpassbool = false
        }else{
            conpassbool = true
            confirmPass.isSecureTextEntry = true

        }
    }
}
extension UIView {
    func addLine(position : LINE_POSITION, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        self.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .LINE_POSITION_TOP:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        }
    }
}
