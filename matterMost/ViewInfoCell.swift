//
//  ViewInfoCell.swift
//  ForgetPassword
//
//  Created by Aamna Mohsin on 02/11/2020.
//

import UIKit

protocol ViewInfoCellDelegate: AnyObject {
   // func didtap(with memberID: Int, name: String)
}

class ViewInfoCell: UITableViewCell {

    var memberInfo: Member!
    weak var delegate: ViewInfoCellDelegate?
    @IBOutlet weak var memberName: UILabel!
    @IBOutlet weak var memberIcon: UIImageView!
    @IBOutlet weak var removeMember: UIButton!
  /*
    @IBAction func removeMember(_ sender: Any) {
        delegate?.didtap(with: memberInfo.id, name:  memberName.text!)
    }
 */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        removeMember.isHidden = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
