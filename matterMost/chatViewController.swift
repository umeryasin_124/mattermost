//
//  chatViewController.swift
//  TestChat
//
//  Created by ethisham bader on 03/11/2020.
//

import UIKit
import ReplayKit

class chatViewController: UIViewController,UITextViewDelegate,UIActionSheetDelegate {
    let myPickerController = UIImagePickerController()
    let documentPickerController = UIDocumentPickerViewController(documentTypes: ["public.item"], in: .import)
    var messages: [UITextView]!
    let cellSpacingHeight: CGFloat = 5
    
    
    
    @IBOutlet weak var voiceBackView: UIView!
    @IBOutlet weak var timeStampView: UIView!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var voiceButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var chatInputText: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emojiButton: UIButton!
    @IBOutlet weak var sheetAction: UIButton!
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var videoPreview: UIView!
    
    @IBOutlet weak var stopRecordingButton: UIButton!
    
    @IBOutlet weak var imageShowView: UIView!
    var count = 0
    var index = 0
    @IBAction func sendMessage(_ sender: Any) {
        print("sended")
        count = count + 1
        tableView.insertRows(at: [IndexPath(row: index, section: 0)], with: .top)
        index = index + 1
        chatInputText.text = ""
        sendButton.isHidden = true
        voiceButton.isHidden = false
        cameraButton.isHidden = false
    }
    @IBAction func closePreview(_ sender: Any) {
        imagePreview.image = UIImage()
        imageShowView.isHidden = true
        self.view.backgroundColor = .white
    }
    @IBAction func sheetAction(_ sender: Any) {
        let alert = UIAlertController(title: "Select Any One", message: "", preferredStyle: .actionSheet)
        let action = UIAlertAction(title: "Search", style: .default) {
            UIAlertAction in
        }
        alert.addAction(action)
        let action2 = UIAlertAction(title: "Mute", style: .default) {
            UIAlertAction in
            // Write your code here
        }
        alert.addAction(action2)
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
            UIAlertAction in
            // It will dismiss action sheet
        }
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func emojiKeyboard(_ sender: Any) {
        chatInputText.becomeFirstResponder()
        
        
    }
    @IBAction func attachSheet(_ sender: Any) {
        let alert = UIAlertController(title: "Select Any One", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Document", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.documentLibrary()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        alert.addAction(UIAlertAction(title: "Screen Recording", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.screenRecording()
        }))
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
            UIAlertAction in
            // It will dismiss action sheet
        }
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func screenRecording(){
        print("hello from screen")
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        imageShowView.isHidden = true
        messages = [UITextView]()
        chatInputText.delegate = self
        
        self.chatInputText.delegate = self
        tableView.tableFooterView = UIView.init(frame: .zero)
        //setUpTableView()
        sendButton.isHidden = true
        voiceButton.isHidden = false
        
        tableView.delegate = self
        tableView.dataSource = self
        
        chatInputText.delegate = self
        // chatInputText.text = "Type your message"
        chatInputText.textColor = UIColor.lightGray
        
        timeStampView.clipsToBounds = true
        timeStampView.layer.cornerRadius = 40
        timeStampView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        voiceBackView.layer.cornerRadius = voiceBackView.frame.size.width/2
        voiceBackView.clipsToBounds = true
        
        voiceBackView.layer.borderColor = UIColor.white.cgColor
        voiceBackView.layer.borderWidth = 4.0
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if chatInputText.text == "Type your message" {
            chatInputText.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if chatInputText.text == "" {
            chatInputText.text = "Type your message"
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if !chatInputText.text.isEmpty {
            
            cameraButton.isHidden = true
            voiceButton.isHidden = true
            sendButton.isHidden = false
        }else{
            // chatInputText.text = "Type your message" //PLACEHOLDER
            cameraButton.isHidden = false
            voiceButton.isHidden = false
            sendButton.isHidden = true
            
        }
        
        if chatInputText.textColor == UIColor.lightGray {
            chatInputText.text = nil
            chatInputText.textColor = UIColor.black
        }
        
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            let currentText:String = chatInputText.text
            let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)
            if updatedText.isEmpty && updatedText.count > 0 {
                
                chatInputText.textColor = UIColor.lightGray
                voiceButton.isHidden = false
                cameraButton.isHidden = false
                
                sendButton.isHidden = true
                
                
                chatInputText.selectedTextRange = chatInputText.textRange(from: chatInputText.beginningOfDocument, to: chatInputText.beginningOfDocument)
            } else if chatInputText.textColor == UIColor.lightGray && !text.isEmpty {
                chatInputText.textColor = UIColor.black
                chatInputText.text = "Type your message"
            }else {
                return true
            }
            return false
        }
    }
    
    
}
extension chatViewController: UITextFieldDelegate{
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
}
extension chatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIDocumentPickerDelegate{
    func documentLibrary()
    {
        
        documentPickerController.delegate = self
        self.present(documentPickerController, animated: true, completion: nil)
        
    }
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        print("import result : \(myURL)")
    }

    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    func photoLibrary()
    {
        
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        myPickerController.allowsEditing = true
        self.present(myPickerController, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imageShowView.isHidden = false
        self.view.backgroundColor = .black
        let imageP = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as! UIImage
        imagePreview.image = imageP
        myPickerController.dismiss(animated: true, completion: nil)
    }
    
}
extension chatViewController: UITableViewDelegate,UITableViewDataSource{
    private func setUpTableView(){
        let nib = UINib(nibName: "MessageViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "chatnib")
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatcell",for: indexPath) as! MessageCell
        cell.sendMessageLabel.text = chatInputText.text
        cell.sendMessageLabel?.backgroundColor = .lightGray
        
        cell.sendMessageLabel.layer.cornerRadius = 10
        cell.sendMessageLabel.layer.borderColor = .none
        
        cell.sendMessageLabel.layer.borderWidth = 0
        //cell.sendMessageLabel.layer.clipsToBounds = true
        cell.sendMessageLabel.clipsToBounds = true
        cell.sendMessageLabel?.numberOfLines = 0
        
        return cell
    }

}

