//
//  firsttimeloginViewController.swift
//  matterMost
//
//  Created by syed muhammad bin saadat on 03/11/2020.
//
// It is Account Setup.

import UIKit
import IHKeyboardAvoiding

class firsttimeloginViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var activrtyLoaderfds: UIActivityIndicatorView!
    @IBOutlet weak var EnterUsername: UITextField!
    @IBOutlet weak var NewPass: UITextField!
    @IBOutlet weak var ConfirmNewPass: UITextField!
    @IBOutlet weak var UpdatePassButton: UIButton!
    
    @IBOutlet weak var confirmPassEyeButton: UIButton!
    @IBOutlet weak var passEyeButton: UIButton!
    //Error Labels
    @IBOutlet weak var usernameError: UILabel!
    @IBOutlet weak var passwordError: UILabel!
    @IBOutlet weak var confirmError: UILabel!
    var token:String!
    
    @IBOutlet weak var subView: UIView!
    //Secure Text
    var securePassEntry: Bool = false
    var secureConfirmPassEntry: Bool = false
    
    let visibleImage = UIImage(named: "eye")
    let invisibleImage = UIImage(named: "eyeGone")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        activrtyLoaderfds.isHidden = true
        KeyboardAvoiding.avoidingView = self.subView
        self.hideKeyboardWhenTappedAround()
        NewPass.delegate = self
        ConfirmNewPass.delegate = self
        EnterUsername.delegate = self
    }
    override func viewDidAppear(_ animated: Bool) {
        
        let tintedImage = visibleImage?.withRenderingMode(.alwaysTemplate)
        passEyeButton.setImage(tintedImage, for: .normal)
        passEyeButton.tintColor = .darkGray
        
        let tintedImage1 = visibleImage?.withRenderingMode(.alwaysTemplate)
        passEyeButton.setImage(tintedImage1, for: .normal)
        passEyeButton.tintColor = .darkGray
    }
    @IBAction func confirmPassTxt(_ sender: Any) {
        
    }
    
    @IBAction func setupAccount(_ sender: Any) {
        if(!ConfirmNewPass.text!.isEmpty && !NewPass.text!.isEmpty && !EnterUsername.text!.isEmpty){
            if(Validator.isValidPassword(NewPass.text!)){
                if(NewPass.text == ConfirmNewPass.text){
                    passwordError.isHidden = true
                    confirmError.isHidden = true
                    UpdatePassButton.isUserInteractionEnabled = false
                    activrtyLoaderfds.isHidden = false
                    activrtyLoaderfds.startAnimating()
                    
                    let apiins = APIManager()
                    var dict = [String:Any]()
                    
                    dict["username"] = EnterUsername.text
                    dict["new_password"] = NewPass.text
                    dict["confirm_password"] = ConfirmNewPass.text
                    UserDefaults.standard.set(EnterUsername.text, forKey: "userName")
                    apiins.apiCaller(url: ENDPOINTS.firstLogin, sendBodyData: dict, httpMethod: "POST",token: token) { [self] (result) in
                        
                        switch result{
                        case .success(let response):
                            DispatchQueue.main.async {
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "SelectCompany") as! SelectCompany
                                vc.apiResponse = response
                                saveUserDataInCoreData(apiResponse: response) {
                                }
                                self.UpdatePassButton.isUserInteractionEnabled = true
                                self.activrtyLoaderfds.isHidden = true
                                self.activrtyLoaderfds.stopAnimating()
                                self.present(vc, animated: true, completion: nil)
                            }
                            break
                        case .failure(let error):
                            DispatchQueue.main.async {
                                self.showCustomAlert(title: "Login Failed", message: error.title!)
                            }
                        }
                    }
                }else{
                    confirmError.text = "Password Not Matched"
                    confirmError.isHidden = false
                }
            }
            else{
                passwordError.isHidden = false
                passwordError.text = "6 Digits, Upper case, Lower case, Number, Special Character"
            }
        }else{
            if(ConfirmNewPass.text!.isEmpty){
                confirmError.isHidden = false
            }
            if(NewPass.text!.isEmpty){
                passwordError.isHidden = false
            }
            if(EnterUsername.text!.isEmpty){
                usernameError.isHidden = false
            }
        }
    }
    func saveUserDataInCoreData(apiResponse:[String:Any],completion: @escaping () -> Void){
        do{
            let data = try JSONSerialization.data(withJSONObject: apiResponse, options: [])
            let jsonDecoder = JSONDecoder()
            let userData = try jsonDecoder.decode(UserCompleteInfo.self, from: data)
            print(userData)
            completion()
        }catch let error{
            print(error)
            completion()
        }
    }
    func showCustomAlert(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func securePassEntry(_ sender: Any) {
        if(securePassEntry){
            NewPass.isSecureTextEntry = false
            securePassEntry = false
            passEyeButton.setImage(invisibleImage, for: .normal)
        }else{
            securePassEntry = true
            NewPass.isSecureTextEntry = true
            passEyeButton.setImage(visibleImage, for: .normal)
        }
    }
    @IBAction func secureConfirmPassEntry(_ sender: Any) {
        if(secureConfirmPassEntry){
            ConfirmNewPass.isSecureTextEntry = false
            secureConfirmPassEntry = false
            confirmPassEyeButton.setImage(invisibleImage, for: .normal)
        }else{
            secureConfirmPassEntry = true
            ConfirmNewPass.isSecureTextEntry = true
            confirmPassEyeButton.setImage(visibleImage, for: .normal)
        }
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if(!EnterUsername.text!.isEmpty){
            usernameError.isHidden = true
        }
        if(!NewPass.text!.isEmpty){
            passwordError.isHidden = true
        }
        if(!ConfirmNewPass.text!.isEmpty){
            confirmError.isHidden = true
        }
    }
}
