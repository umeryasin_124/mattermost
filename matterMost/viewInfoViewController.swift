//
//  viewInfoViewController.swift
//  matterMost
//
//  Created by Umer yasin on 09/11/2020.
//

import Foundation
import UIKit
struct Member{
    var id:Int
    var username:String
}

let editImage = UIImage(named: "edit")

class viewInfoViewController : UIViewController{
    
    @IBOutlet weak var groupName: UITextField!
    var data = [String: Any]()
    var updateChannelDic = [String: Any]()
    var completeInfo: UserCompleteInfo!
    var apiManager: APIManager!
    var teamID:Int!
    var companyID:Int!
    var channelID:Int!
    var checkGroupName: String? = ""
    
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var doneBtn: UIBarButtonItem!

    var channelName: String!
    var members = [Member]()
    
    @IBOutlet weak var memberList: UITableView!
   
    @IBOutlet weak var listCount: UILabel!
    
    override func viewDidLoad() {
        apiManager = APIManager()
        memberList.delegate = self
        memberList.dataSource = self
        setUpTableView()
        listCount.text = String(members.count)
        doneBtn.isEnabled = false
        groupName.isUserInteractionEnabled = false
        groupName.addLine(position: .LINE_POSITION_BOTTOM, color: .darkGray, width: 0.5)
        
        let tintedImage = editImage?.withRenderingMode(.alwaysTemplate)
        editBtn.setImage(tintedImage, for: .normal)
        editBtn.tintColor = .darkGray
    }
    
    @IBAction func doneBtn(_ sender: Any) {
        
        groupName.isUserInteractionEnabled = false
        doneBtn.isEnabled = false
        if(checkGroupName == groupName.text){
            
            showCustomAlert(title: "Group Name not changed", message: "Previous Group Name is not changed Please Check")
            doneBtn.isEnabled = true
        }
        else{
            if(groupName.text!.isEmpty){
                showCustomAlert(title: "Error", message: "Group Name field is Empty")
                doneBtn.isEnabled = true
            } else{
                if ConnectivityService().isConnectedToNetwork(){
                    //getiing token
                    let userToken = UserDefaults.standard.string(forKey: "token")
                    
                    //addding to grouo name to dictonary
                    updateChannelDic["name"] = groupName.text
                    updateChannelDic["channel_id"] = channelID
                    updateChannelDic["team_id"] = teamID
                    updateChannelDic["company_id"] = companyID
                    // calling api
                    apiManager.apiCaller(url: ENDPOINTS.updateChannel, sendBodyData: updateChannelDic, httpMethod: "POST", token: userToken!) { [self] (result) in
                        switch result {
                        case .success( _):
                            DispatchQueue.main.async {
                                showCustomAlert(title: "Done", message: "Succusfully Group Name Changed")
                            }
                            break
                        case .failure(let error):
                            DispatchQueue.main.async {
                                showCustomAlert(title: "Group Name Change Failed", message: error.title!)
                                doneBtn.isEnabled = true
                                groupName.isUserInteractionEnabled = true
                            }
                        }
                    }
                } else{
                    showCustomAlert(title: "Internet Connection Error", message: "Internet Not Connected Please Connect")
                }
            }
        }
    }
    @IBAction func editGroupName(_ sender: Any) {
        
        groupName.isUserInteractionEnabled = true
        doneBtn.isEnabled = true

    }
    override func viewDidAppear(_ animated: Bool) {
        if ConnectivityService().isConnectedToNetwork() {
            apiCall()
            checkGroupName = channelName
            //print(checkGroupName!)
        }else{
            showCustomAlert(title: "", message: "No Internet Connection")}
    }

    func showCustomAlert(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func apiCall(){
    
        let token = UserDefaults.standard.string(forKey: "token")
        
        data["channel_id"] = channelID
        data["team_id"] = teamID
        data["company_id"] = companyID
        
        apiManager.apiCaller(url: ENDPOINTS.userInChannels, sendBodyData: data, httpMethod: "POST", token: token!) { [self] (result) in
            switch result{
            case .success(let response):
                DispatchQueue.main.async { [self] in
                      let dataU =  response["data"] as! [[String:Any]]
                      channelName = dataU[0]["name"] as? String
                      print(channelName!)
                    let users = dataU[0]["users"] as? [[String:Any]]
                    for i in 0 ..< users!.count{
                        if(!(users![i]["username"] == nil)){
                           members.append(Member(id: users![i]["id"] as! Int, username: users![i]["username"] as! String))
                       }
                   }
                   memberList.reloadData()
                   listCount.text = String(members.count)
                   groupName.text = channelName
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    print("Error \(error)")
                    self.showCustomAlert(title:"Error", message: error.title!)
                    
                }
            }
        }
    }
}

extension viewInfoViewController: UITableViewDelegate,UITableViewDataSource, ViewInfoCellDelegate{
  
    private func setUpTableView(){
        let nib = UINib(nibName: "ViewInfoCell", bundle: nil)
        memberList.register(nib, forCellReuseIdentifier: "cellNib")
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return members.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = memberList.dequeueReusableCell(withIdentifier: "cellNib",for: indexPath) as! ViewInfoCell
        
        cell.memberName.text = members[indexPath.row].username
        cell.memberInfo = members[indexPath.row]
        cell.delegate = self
        return cell
        
    }
}
